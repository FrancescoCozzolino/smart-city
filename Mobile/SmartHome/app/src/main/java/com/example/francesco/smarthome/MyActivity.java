package com.example.francesco.smarthome;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.data.MediaType;

import java.io.IOException;
import java.util.concurrent.Semaphore;

/**
 * Created by Francesco on 18/07/2015.
 */
public class MyActivity extends AppCompatActivity implements FeatureFragment.EventListener{

    final static String FRAGMENT="fragment";
    final static String EVENT="event";
    final static String FROM_ALARM="fromAlarm";
    final static String DETAIL = "detail";
    final static String EVENTLIST = "eventList";
    private final static String START="start";
    private final static int SCREEN_SIZE= 7;

    protected boolean landscape=false;
    protected FeatureFragment eventFragment=null;
    protected Intent application_intent=null;
    private boolean orientationIsChanged=false,goOn=false;
    private static ClientResource resource;
    //private static Semaphore s=new Semaphore(0);
    private static boolean firstTime=true,tablet=false;

   /* @Override
    public boolean onTouchEvent(MotionEvent event){

        int action = MotionEventCompat.getActionMasked(event);

        switch(action) {
            case (MotionEvent.ACTION_DOWN) :
                System.out.println("Punto di inizio: "+event.getX()+" "+event.getY());
                return true;
            case (MotionEvent.ACTION_MOVE) :
                System.out.println("In movimento: "+event.getX()+" "+event.getY());
                return true;
            case (MotionEvent.ACTION_UP) :
                System.out.println("Fine: "+event.getX()+" "+event.getY());
                return true;
            case (MotionEvent.ACTION_CANCEL) :
                return true;
            case (MotionEvent.ACTION_OUTSIDE) :
                return true;
            default :
                return super.onTouchEvent(event);
        }
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(firstTime){
            firstTime=false;

            SharedPreferences preferences = getSharedPreferences("Agenda", MODE_PRIVATE);

            resource=new ClientResource("http://192.168.1.103:8182/home/devices");
            refreshData();

            Fragment currentFragment=getFragmentManager().findFragmentByTag(DETAIL);

            if(currentFragment!=null){
                ((ManageFragment) currentFragment).setEnabled(false);
            }

            /*try {
                s.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/

            //controllo se � presente la dimensione dello schermo
            float inches = preferences.getFloat("sizeScreen", -1);
            if (inches == -1) {
                //calcolo le dimensioni dello schermo
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                double width = (double) displayMetrics.widthPixels / (double) displayMetrics.densityDpi;
                double height = (double) displayMetrics.heightPixels / (double) displayMetrics.densityDpi;
                inches = (float) Math.sqrt(Math.pow(width, 2) + Math.pow(height, 2));
                SharedPreferences.Editor editor = preferences.edit();
                editor.putFloat("sizeScreen", inches);
                editor.commit();
            }

            if (inches >= SCREEN_SIZE) {
                tablet = true;
            }
        }
    }


    //metodo invocato quando si preme il tasto back o invocato in seguito alla pressione di determinate icone
    @Override
    public void onBackPressed() {

        //if(animationIsEnded && ((Toolbar) findViewById(R.id.toolbar)).getMenu().hasVisibleItems()) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        //ottengo il numero di fragment presenti nello stack
        int count = getFragmentManager().getBackStackEntryCount();

        //se è maggiore di 0, l'applicazione non deve essere chiusa
        if (count > 0) {
            if(!landscape){
                setTitle("SmartHome");
            }

            if(!tablet || (tablet && !landscape)){
                getFragmentManager().popBackStack();
            }else if(tablet && landscape){
                for(int i=count;i>=0;i--){
                    getFragmentManager().popBackStack();
                }

                if(eventFragment!=null){
                    eventFragment.clear();
                }
            }

        } else {
            //se count == 0, se il dispositivo è un tablet, bisogna considerare il caso di un eventuale
            //cambio portrait-landscape

            //sono in portrait
            if (orientationIsChanged && !landscape) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                if (eventFragment == null) {
                    eventFragment = new FeatureFragment();
                }

                setTitle("SmartHome");
                //al momento dell'invocazione del metodo stavo visualizzando il dettaglio di un evento
                if (getFragmentManager().findFragmentByTag(DETAIL) != null && getFragmentManager().findFragmentByTag(DETAIL).isVisible()) {

                    //sostituzione fragment
                    transaction.setCustomAnimations(R.animator.pop_enter_event_list, R.animator.pop_exit_detail);
                    transaction.replace(R.id.container, eventFragment, EVENTLIST);
                    transaction.commit();
                }  else {
                    super.onBackPressed();
                }


            } else if (tablet && orientationIsChanged && landscape) {
                // sono in landscape
                if (getFragmentManager().findFragmentByTag(DETAIL) != null) {
                    getFragmentManager().popBackStack();
                } else {
                    super.onBackPressed();
                }
            } else {
                super.onBackPressed();
            }
        }
    }


    //metodo invocato quando viene selezionato un evento dalla listview
    @Override
    public void onClickEventListener(Feature feature) {
        Bundle bundle=new Bundle();
        bundle.putSerializable("feature",feature);

        ManageFragment fragment = new ManageFragment();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // se mi trovo in portrait eseguo delle determinate azioni relative alle animazioni dei fragment
        if(!tablet || !landscape) {
            setTitle(feature.getName());
            transaction.setCustomAnimations(R.animator.enter_detail, R.animator.exit_event_list, R.animator.pop_enter_event_list, R.animator.pop_exit_detail);
        }

        transaction.replace(R.id.container, fragment, DETAIL);
        transaction.addToBackStack(null);
        transaction.commit();
        getFragmentManager().executePendingTransactions();
    }

    public boolean isTablet() {
        return tablet;
    }

    //quando cambio modalità da portrait-landscape o viceversa, prima dell'invocazione del metodo
    // onDestroy(), viene invocato il seguente metodo, utilizzato per salvare i dati necessari
    // per mantenere lo stato dei fragment
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ManageFragment fragment;
        if(getFragmentManager().findFragmentByTag(DETAIL)!=null && getFragmentManager().findFragmentByTag(DETAIL).isVisible()){
            fragment = (ManageFragment)getFragmentManager().findFragmentByTag(DETAIL);

            outState.putString(FRAGMENT, DETAIL);
            outState.putSerializable(EVENT, fragment.getFeature());
        }

        outState.putBoolean("tablet", tablet);

    }

    //controllo l'intent. questo metodo viene invocato dall'activity creata
    protected void checkIntent(){
        goOn=true;
        if(application_intent!=null && application_intent.getStringExtra(FRAGMENT)!=null){

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            orientationIsChanged=true;


            Bundle bundle=new Bundle();
            bundle.putSerializable("feature",((Feature)application_intent.getSerializableExtra(EVENT)));
            ManageFragment event = new ManageFragment();
            event.setArguments(bundle);

            if (application_intent.getStringExtra(FRAGMENT).equals(DETAIL)) {
                if(!application_intent.getBooleanExtra(FROM_ALARM,false)){
                    transaction.add(R.id.container, event, DETAIL);
                }
            }

            if(landscape) {
                transaction.addToBackStack(null);
            }else{
                setTitle(((Feature)application_intent.getSerializableExtra(EVENT)).getName());
            }
            transaction.commit();
            getFragmentManager().executePendingTransactions();
        }else{
            orientationIsChanged=false;
        }
    }

    // metodo invocato quando si passa da portrait-landscape o viceversa, ed è necessario mantenere
    // lo stato del fragment
    protected void putExtraIntent(Bundle savedInstanceState,Intent intent){
        if(savedInstanceState!=null && savedInstanceState.getString(FRAGMENT)!=null){
            intent.putExtra(FRAGMENT,savedInstanceState.getString(FRAGMENT));
            if(savedInstanceState.getString(FRAGMENT).equals(DETAIL)){
                intent.putExtra(EVENT,savedInstanceState.getSerializable(EVENT));
            }else{
                intent.putExtra(EVENT,savedInstanceState.getSerializable(EVENT));
            }
        }
    }


    // metodo invocato dopo che l'activity è stata creata. Si impostano i vari valori a seconda del
    // fragment visualizzato
    @Override
    protected void onStart() {
        super.onStart();

        if(application_intent!=null && application_intent.getStringExtra(FRAGMENT)!=null && goOn){

            ManageFragment fragment;

            if(application_intent.getStringExtra(FRAGMENT).equals(DETAIL)){
                if((application_intent.getBooleanExtra(FROM_ALARM,false)&&application_intent.getBooleanExtra(START,false))||!(application_intent.getBooleanExtra(FROM_ALARM,false))) {
                    fragment = (ManageFragment) getFragmentManager().findFragmentByTag(DETAIL);
                    fragment.setFeature((Feature) application_intent.getSerializableExtra(EVENT));
                    if (tablet && landscape) {
                        Feature feature =((Feature) application_intent.getSerializableExtra(EVENT));
                        eventFragment.selectItem(feature.getName());
                    }
                }
            }

        }
        goOn=false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        Fragment currentFragment=getFragmentManager().findFragmentByTag(DETAIL);

        if(currentFragment!=null) {
            ManageFragment fragment=(ManageFragment)currentFragment;

            fragment.setEnabled(false);

            /*Feature feature=fragment.getFeature();

            if(feature instanceof LightFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getLightFeature());
                }
                fragment.refreshLight();
            }else if(feature instanceof AlarmFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getAlarmFeature());
                }
                fragment.refreshAlarm();
            }else if(feature instanceof GarageFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getGarageFeature());
                }
                fragment.refreshGarage();
            }else if(feature instanceof IrrigationFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getIrrigationFeature());
                }
                fragment.refreshIrrigation();
            }else if(feature instanceof OvenFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getOvenFeature());
                }
                fragment.refreshOven();
            }else if(feature instanceof TemperatureFeature){
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getTemperatureFeature());
                }
                fragment.refreshTemperature();
            }else if(feature instanceof WasherFeature) {
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getWasherFeature());
                }
                fragment.refreshWasher();
            }else if(feature instanceof EnergyFeature) {
                if(orientationIsChanged){
                    fragment.setFeature(FeatureFactory.getEnergyFeature());
                }
                fragment.refreshEnergy();
            }*/
        }

        if(id==R.id.action_send){
            Toast.makeText(this,"Richiesta inviata",Toast.LENGTH_SHORT).show();
            sendRequest();
            /*try {
                s.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }else{
            Toast.makeText(this,"Refresh",Toast.LENGTH_SHORT).show();
            refreshData();
        }

        //refreshData();

        /*try {
            s.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        return super.onOptionsItemSelected(item);
    }

    private void refreshData(){
        new SendRequestToServer().execute("GET");
    }

    private void sendRequest(){
        new SendRequestToServer().execute("PUT");
    }

    private class SendRequestToServer extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... params) {
            if(params[0].compareTo("PUT")==0){
                JSONObject json=new JSONObject();
                try {
                    setJSONObject(FeatureFactory.getAlarmFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getGarageFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getIrrigationFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getLightFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getOvenFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getTemperatureFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getWasherFeature().getParam(),json);
                    setJSONObject(FeatureFactory.getEnergyFeature().getParam(),json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(json.length()!=0) {
                    resource.put(new JsonRepresentation(json), MediaType.APPLICATION_JSON);
                }

            }else if(params[0].compareTo("GET")==0){
                try {
                    setParam(new JSONObject(resource.get().getText()));
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
            //s.release();

            /*Fragment currentFragment=getFragmentManager().findFragmentByTag(DETAIL);

            if(currentFragment!=null){
                ((ManageFragment) currentFragment).setEnabled(true);

            }*/

           return params[0];
        }

        @Override
        protected void onPostExecute(String string)
        {
            if(string.compareTo("PUT")==0){
                new SendRequestToServer().execute("GET");
            }else if(string.compareTo("GET")==0){
                Fragment currentFragment=getFragmentManager().findFragmentByTag(DETAIL);

                if(currentFragment!=null) {
                    ManageFragment fragment=(ManageFragment)currentFragment;
                    fragment.setEnabled(true);

                    Feature feature=fragment.getFeature();

                    if(feature instanceof LightFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getLightFeature());
                        }
                        fragment.refreshLight();
                    }else if(feature instanceof AlarmFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getAlarmFeature());
                        }
                        fragment.refreshAlarm();
                    }else if(feature instanceof GarageFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getGarageFeature());
                        }
                        fragment.refreshGarage();
                    }else if(feature instanceof IrrigationFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getIrrigationFeature());
                        }
                        fragment.refreshIrrigation();
                    }else if(feature instanceof OvenFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getOvenFeature());
                        }
                        fragment.refreshOven();
                    }else if(feature instanceof TemperatureFeature){
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getTemperatureFeature());
                        }
                        fragment.refreshTemperature();
                    }else if(feature instanceof WasherFeature) {
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getWasherFeature());
                        }
                        fragment.refreshWasher();
                    }else if(feature instanceof EnergyFeature) {
                        if(orientationIsChanged){
                            fragment.setFeature(FeatureFactory.getEnergyFeature());
                        }
                        fragment.refreshEnergy();
                    }
                }
            }
        }

        private void setJSONObject(JSONArray array, JSONObject json){
            for(int i=0;i<array.length();i++){
                try {
                    JSONObject object=array.getJSONObject(i);
                    String key=object.keys().next();
                    json.put(key,object.getJSONObject(key));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private void setParam(JSONObject json){
            try {
                FeatureFactory.getAlarmFeature().setParam(json.getJSONObject("alarm"));
                FeatureFactory.getGarageFeature().setParam(json.getJSONObject("garage"));
                FeatureFactory.getIrrigationFeature().setParam(json.getJSONObject("irrigation"));
                FeatureFactory.getLightFeature().setParam(json.getJSONObject("light"));
                FeatureFactory.getOvenFeature().setParam(json.getJSONObject("oven"));
                FeatureFactory.getTemperatureFeature().setParam(json.getJSONObject("temperature"));
                FeatureFactory.getWasherFeature().setParam(json.getJSONObject("washer"));
                FeatureFactory.getEnergyFeature().setParam(json.getJSONObject("main"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }
}
