package com.example.francesco.smarthome;

/**
 * Created by Francesco on 04/01/2017.
 */

public class FeatureFactory {

    private static AlarmFeature alarmFeature;
    private static IrrigationFeature irrigationFeature;
    private static LightFeature lightFeature;
    private static OvenFeature ovenFeature;
    private static TemperatureFeature temperatureFeature;
    private static WasherFeature washerFeature;
    private static GarageFeature garageFeature;
    private static EnergyFeature energyFeature;

    private FeatureFactory(){
    }

    public static AlarmFeature getAlarmFeature(){
        if(alarmFeature==null){
            alarmFeature=new AlarmFeature();
        }
        return alarmFeature;
    }

    public static IrrigationFeature getIrrigationFeature() {
        if(irrigationFeature==null){
            irrigationFeature=new IrrigationFeature();
        }
        return irrigationFeature;
    }

    public static LightFeature getLightFeature() {
        if(lightFeature==null){
            lightFeature=new LightFeature();
        }
        return lightFeature;
    }

    public static OvenFeature getOvenFeature() {
        if(ovenFeature==null){
            ovenFeature=new OvenFeature();
        }
        return ovenFeature;
    }

    public static TemperatureFeature getTemperatureFeature() {
        if(temperatureFeature==null){
            temperatureFeature=new TemperatureFeature();
        }
        return temperatureFeature;
    }

    public static WasherFeature getWasherFeature() {
        if(washerFeature==null){
            washerFeature=new WasherFeature();
        }
        return washerFeature;
    }

    public static GarageFeature getGarageFeature() {
        if(garageFeature==null){
            garageFeature=new GarageFeature();
        }
        return garageFeature;
    }

    public static EnergyFeature getEnergyFeature() {
        if(energyFeature==null){
            energyFeature=new EnergyFeature();
        }
        return energyFeature;
    }
}
