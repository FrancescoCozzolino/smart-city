package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Francesco on 25/01/2017.
 */
public class EnergyFeature extends Feature{
    
    private int currentConsumption,profile,currentProfile;

    public EnergyFeature() {
        super("Energia");

        currentConsumption=0;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            currentConsumption=json.getInt("val2");
            profile=json.getInt("val1");
            currentProfile=profile;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException {
        JSONArray array=new JSONArray();

        if(currentProfile!=profile){
            array.put(new JSONObject().put("energy",new JSONObject().put("profile",profile)));
        }

        return array;
    }

    public int getProfile() {
        return profile;
    }

    public int getLimitForSelectedProfile() {
        switch(profile){
            case 0:return 1000;
            case 1:return 2000;
            case 2:return 3000;
        }

        return 0;
    }

    public int getCurrentConsumption() {
        return currentConsumption;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }
}
