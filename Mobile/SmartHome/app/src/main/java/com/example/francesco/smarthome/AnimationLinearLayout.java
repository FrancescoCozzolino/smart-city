package com.example.francesco.smarthome;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.io.Serializable;

/**
 * Created by Francesco on 20/04/2015.
 */
// classe necessaria per eseguire delle transazioni animate
public class AnimationLinearLayout extends LinearLayout{

    public AnimationLinearLayout(Context context) {
        super(context);
    }

    public AnimationLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimationLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public float getXFraction() {
        final int width = getWidth();
        if (width != 0) return getX() / getWidth();
        else return getX();
    }

    public void setXFraction(float xFraction) {
        final int width = getWidth();
        setX((width > 0) ? (xFraction * width) : -9999);
    }

    public float getYFraction() {
        final int height = getHeight();
        if (height != 0) return getY() / getHeight();
        else return getY();
    }

    public void setYFraction(float yFraction) {
        final int height = getHeight();
        setY((height > 0) ? (yFraction * height) : -9999);
    }
}
