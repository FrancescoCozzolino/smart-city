package com.example.francesco.smarthome;

import java.io.Serializable;

/**
 * Created by Francesco on 19/01/2017.
 */

public class TimeHeater implements Serializable {
    private String dayOfWeek,timeStart,timeEnd;
    private int dayOfWeekNumber;
    private long duration;

    public TimeHeater(int dayOfWeek,String timeStart,String timeEnd){
        this.dayOfWeekNumber=dayOfWeek;
        this.timeStart=timeStart;
        this.timeEnd=timeEnd;

        switch (dayOfWeekNumber){
            case 0:this.dayOfWeek="Lunedì";break;
            case 1:this.dayOfWeek="Martedì";break;
            case 2:this.dayOfWeek="Mercoledì";break;
            case 3:this.dayOfWeek="Giovedì";break;
            case 4:this.dayOfWeek="Venerdì";break;
            case 5:this.dayOfWeek="Sabato";break;
            case 6:this.dayOfWeek="Domenica";break;
            default:this.dayOfWeek="null";break;
        }



        String[] start=timeStart.split(":");
        String[] end=timeEnd.split(":");

        int startInMinute=Integer.parseInt(start[0])*60+Integer.parseInt(start[1]);
        int endInMinute=Integer.parseInt(end[0])*60+Integer.parseInt(end[1]);

        duration=(endInMinute-startInMinute)*60000;
    }

    public String getDayOfWeek(){
        return dayOfWeek;
    }

    public String getTimeStart(){
        return timeStart;
    }

    public String getTimeEnd(){
        return timeEnd;
    }

    public long getDuration(){
        return duration;
    }

    public int getDayOfWeekNumber(){
        return dayOfWeekNumber;
    }

    @Override
    public String toString() {
        return dayOfWeek+" dalle: "+timeStart+" alle: "+timeEnd;
    }
}
