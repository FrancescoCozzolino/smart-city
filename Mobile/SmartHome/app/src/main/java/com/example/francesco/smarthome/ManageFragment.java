package com.example.francesco.smarthome;

import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.Calendar;


public class ManageFragment extends Fragment {

    private View view;
    private Feature feature =null;
    private ToggleButton toggleGas,toggleSmoke,toggleCamera,toggleGarage,toggleIrrigation,toggleLight,toggleExternalLight,toggleOven,toggleHeater,toggleWasher;
    private TextView txtLimitCurrent,txtCurrentConsumption,txtCamera,txtGarage,txtSoil,txtTank,txtExternalLight,txtTemperatureOven,txtRemainingTimeOven,txtTemperatureHome,txtMinTemperatureHome,txtMaxTemperatureHome,txtWaterTemperatureHeater,txtRemainingTimeWasher;
    private SeekBar seekbarExternalLight,seekbarTemperatureOven,seekbarMinTemperatureHeater,seekbarMaxTemperatureHeater,seekbarTemperatureWaterHeater;
    private EditText edittextModifyTimeOven,startTime,endTime;
    private Spinner spinnerWasher,spinnerDay,spinnerProfile;
    private ListView listView;
    private TimeAdapter adapter;
    private boolean flagStart=false,flagEnd=false,enabled=true;
    private Object lock=new Object();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        String layout="";
        if(getArguments()!=null) {
            feature=((Feature)getArguments().getSerializable("feature"));
            layout = feature.getName();
        }

        synchronized (lock) {
            if (layout.compareTo("Luci") == 0) {
                view = inflater.inflate(R.layout.fragment_light, container, false);
                initializeLight();
            } else if (layout.compareTo("Allarmi") == 0) {
                view = inflater.inflate(R.layout.fragment_alarm, container, false);
                initializeAlarm();
            } else if (layout.compareTo("Lavatrice") == 0) {
                view = inflater.inflate(R.layout.fragment_washer, container, false);
                initializeWasher();
            } else if (layout.compareTo("Irrigazione") == 0) {
                view = inflater.inflate(R.layout.fragment_irrigation, container, false);
                initializeIrrigation();
            } else if (layout.compareTo("Garage") == 0) {
                view = inflater.inflate(R.layout.fragment_garage, container, false);
                initializeGarage();
            } else if (layout.compareTo("Forno") == 0) {
                view = inflater.inflate(R.layout.fragment_oven, container, false);
                initializeOven();
            } else if (layout.compareTo("Termostato") == 0) {
                view = inflater.inflate(R.layout.fragment_temperature, container, false);
                initializeTemperature();
            } else if (layout.compareTo("Energia") == 0) {
                view = inflater.inflate(R.layout.fragment_energy, container, false);
                initializeEnergy();
            }

            view.setBackgroundColor(Color.WHITE);

            if(!enabled){
                enableDisableViewGroup((ViewGroup)view);
            }
        }

        return view;
    }

    public void setFeature(Feature feature){
        this.feature=feature;
    }

    public Feature getFeature() {
        return feature;
    }

    private void initializeAlarm(){
        txtCamera=((TextView) view.findViewById(R.id.Camera));
        txtCamera.setText("Camera: "+((AlarmFeature)feature).getStateCamera());

        toggleCamera=((ToggleButton)view.findViewById(R.id.stateCamera));

        if(((AlarmFeature) feature).isActiveCamera()){
            toggleCamera.setChecked(true);
        }else{
            toggleCamera.setChecked(false);
        }

        toggleCamera.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((AlarmFeature) feature).setActiveCamera(isChecked);
            }
        });

        toggleGas=((ToggleButton)view.findViewById(R.id.stateGas));

        if(((AlarmFeature) feature).isActiveGas()){
            toggleGas.setChecked(true);
        }else{
            toggleGas.setChecked(false);
        }

        toggleGas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((AlarmFeature) feature).setActiveGas(isChecked);
            }
        });

        toggleSmoke=((ToggleButton)view.findViewById(R.id.stateSmoke));

        if(((AlarmFeature) feature).isActiveSmoke()){
            toggleSmoke.setChecked(true);
        }else{
            toggleSmoke.setChecked(false);
        }

        toggleSmoke.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((AlarmFeature) feature).setActiveSmoke(isChecked);
            }
        });
    }

    private void initializeGarage(){
        toggleGarage=((ToggleButton)view.findViewById(R.id.stateGarage));

        if(((GarageFeature) feature).isOpen()){
            toggleGarage.setChecked(true);
        }else{
            toggleGarage.setChecked(false);
        }

        toggleGarage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((GarageFeature) feature).setOpen(isChecked);
            }
        });

        txtGarage=((TextView) view.findViewById(R.id.StateGarage));
        txtGarage.setText("Stato: "+((GarageFeature) feature).getState());
    }

    private void initializeIrrigation(){
        toggleIrrigation=((ToggleButton)view.findViewById(R.id.stateIrrigation));

        if(((IrrigationFeature) feature).isActive()){
            toggleIrrigation.setChecked(true);
        }else{
            toggleIrrigation.setChecked(false);
        }

        toggleIrrigation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((IrrigationFeature) feature).setActive(isChecked);
            }
        });

        txtSoil=((TextView) view.findViewById(R.id.SoilHumidity));
        txtSoil.setText("Terreno: "+((IrrigationFeature) feature).getSoil());
        txtTank=((TextView) view.findViewById(R.id.TankState));
        txtTank.setText("Serbatoio: "+((IrrigationFeature) feature).getTank());
    }

    private void initializeLight(){
        toggleLight=((ToggleButton)view.findViewById(R.id.stateLight));

        if(((LightFeature) feature).getState()){
            toggleLight.setChecked(true);
        }else{
            toggleLight.setChecked(false);
        }

        toggleLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((LightFeature) feature).setState(isChecked);
            }
        });

        toggleExternalLight=((ToggleButton)view.findViewById(R.id.stateSensorLight));

        if(((LightFeature) feature).getStateSensorLight()){
            toggleExternalLight.setChecked(true);
        }else{
            toggleExternalLight.setChecked(false);
        }

        toggleExternalLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((LightFeature) feature).setStateSensorLight(isChecked);
            }
        });


        txtExternalLight=((TextView) view.findViewById(R.id.externalLight));
        txtExternalLight.setText("Luce esterna: "+((LightFeature) feature).getBrightness());
        seekbarExternalLight=((SeekBar) view.findViewById(R.id.brightness));
        seekbarExternalLight.setProgress(((LightFeature) feature).getBrightness());
        seekbarExternalLight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtExternalLight.setText("Luce esterna: "+progress);
                ((LightFeature) feature).setBrightness(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initializeOven(){
        toggleOven=((ToggleButton)view.findViewById(R.id.stateOven));

        if(((OvenFeature) feature).isActive()){
            toggleOven.setChecked(true);
        }else{
            toggleOven.setChecked(false);
        }

        toggleOven.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((OvenFeature) feature).setActive(isChecked);
            }
        });

        edittextModifyTimeOven=((EditText) view.findViewById(R.id.ModifyTime));
        edittextModifyTimeOven.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int time = Integer.parseInt(s.toString());
                    ((OvenFeature)feature).setModifyTime(time);
                }catch(NumberFormatException e){
                    ((OvenFeature)feature).setModifyTime(0);
                }
            }
        });
        txtTemperatureOven=((TextView) view.findViewById(R.id.TemperatureOven));
        txtTemperatureOven.setText("Temperatura: "+((OvenFeature) feature).getTemperature());
        txtRemainingTimeOven=((TextView) view.findViewById(R.id.RemainingTimeOven));
        txtRemainingTimeOven.setText("Tempo rimanente: "+((OvenFeature) feature).getRemainingTime());
        edittextModifyTimeOven=((EditText)view.findViewById(R.id.ModifyTime));
        seekbarTemperatureOven=((SeekBar) view.findViewById(R.id.SeekBarTemperatureOven));
        seekbarTemperatureOven.setProgress(((OvenFeature) feature).getTemperature()-90);
        seekbarTemperatureOven.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress+=90;
                txtTemperatureOven.setText("Temperatura: "+progress);
                ((OvenFeature) feature).setTemperature(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initializeTemperature(){

        final Calendar calendar=Calendar.getInstance();
        listView=(ListView) view.findViewById(R.id.timeList);
        flagStart=false;
        flagEnd=false;

        adapter=new TimeAdapter(getActivity(),((TemperatureFeature)feature));
        listView.setAdapter(adapter);

        spinnerDay=((Spinner) view.findViewById(R.id.spnDay));
        startTime=((EditText) view.findViewById(R.id.startTime));
        endTime=((EditText)view.findViewById(R.id.endTime));

        final TimePickerDialog.OnTimeSetListener timeFrom = new TimePickerDialog.OnTimeSetListener() {

            int count=0;

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if(count==1) {
                    String time=hourOfDay<10?"0"+hourOfDay:Integer.toString(hourOfDay);
                    if(minute<10){
                        time+=":0"+minute;
                    }else{
                        time+=":"+minute;
                    }

                    flagStart = true;

                    if (flagEnd) {

                        int timeStart=hourOfDay*60+minute;
                        String arrayHour[]=endTime.getText().toString().split(":");
                        int timeEnd=Integer.parseInt(arrayHour[0])*60+Integer.parseInt(arrayHour[1]);

                        if(timeStart<timeEnd) {
                            ((TemperatureFeature) feature).addTime(spinnerDay.getSelectedItemPosition(), time, endTime.getText().toString());
                            adapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getActivity(),"Campi errati",Toast.LENGTH_SHORT).show();
                        }

                        endTime.setText("");
                        flagStart = false;
                        flagEnd = false;
                    } else {
                        startTime.setText(time);
                    }
                    count=-1;
                }
                count++;
            }
        };


        final TimePickerDialog.OnTimeSetListener timeTo = new TimePickerDialog.OnTimeSetListener() {

            int count=0;

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                if(count==1) {
                    String time=hourOfDay<10?"0"+hourOfDay:Integer.toString(hourOfDay);
                    if(minute<10){
                        time+=":0"+minute;
                    }else{
                        time+=":"+minute;
                    }
                    flagEnd = true;

                    if (flagStart) {

                        int timeEnd=hourOfDay*60+minute;
                        String arrayHour[]=startTime.getText().toString().split(":");
                        int timeStart=Integer.parseInt(arrayHour[0])*60+Integer.parseInt(arrayHour[1]);

                        if(timeEnd>timeStart) {
                            ((TemperatureFeature) feature).addTime(spinnerDay.getSelectedItemPosition(), startTime.getText().toString(), time);
                            adapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getActivity(),"Campi errati",Toast.LENGTH_SHORT).show();
                        }

                        startTime.setText("");
                        flagStart = false;
                        flagEnd = false;
                    } else {
                        endTime.setText(time);
                    }

                    count=-1;
                }

                count++;
            }


        };

        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), timeFrom, calendar.get((Calendar.HOUR_OF_DAY)), calendar.get(Calendar.MINUTE), true).show();
            }
        });

        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(getActivity(), timeTo, calendar.get((Calendar.HOUR_OF_DAY)), calendar.get(Calendar.MINUTE), true).show();
            }
        });

        ArrayAdapter<String> spinnerArrayAdapter=new ArrayAdapter<String>(getActivity(),
                R.layout.layout_spinner,new String []{"Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato","Domenica"});
        spinnerArrayAdapter.setDropDownViewResource(R.layout.layout_spinner);
        spinnerDay.setAdapter(spinnerArrayAdapter);
        spinnerDay.setSelection(0);

        toggleHeater=((ToggleButton)view.findViewById(R.id.stateHeater));

        if(((TemperatureFeature) feature).isActive()){
            toggleHeater.setChecked(true);
        }else{
            toggleHeater.setChecked(false);
        }

        toggleHeater.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((TemperatureFeature) feature).setActive(isChecked);
            }
        });

        txtTemperatureHome=((TextView) view.findViewById(R.id.TemperatureHeater));
        txtTemperatureHome.setText("Temperatura: "+((TemperatureFeature) feature).getTemperature());
        txtMinTemperatureHome=((TextView) view.findViewById(R.id.minTemperature));
        txtMinTemperatureHome.setText("Min. Temp: "+((TemperatureFeature) feature).getMinTemperature());
        txtMaxTemperatureHome=((TextView) view.findViewById(R.id.maxTemperature));
        txtMaxTemperatureHome.setText("Max. Temp: "+((TemperatureFeature) feature).getMaxTemperature());
        txtWaterTemperatureHeater=((TextView) view.findViewById(R.id.TemperatureWaterHeater));
        txtWaterTemperatureHeater.setText("Temp. Acqua: "+((TemperatureFeature) feature).getWaterTemperature());

        seekbarMinTemperatureHeater=((SeekBar) view.findViewById(R.id.SeekBarMinTemperature));
        seekbarMinTemperatureHeater.setProgress(((TemperatureFeature) feature).getMinTemperature()-10);
        seekbarMinTemperatureHeater.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress+=10;
                txtMinTemperatureHome.setText("Min. Temp: "+progress);
                ((TemperatureFeature) feature).setMinTemperature(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbarMaxTemperatureHeater=((SeekBar) view.findViewById(R.id.SeekBarMaxTemperature));
        seekbarMaxTemperatureHeater.setProgress(((TemperatureFeature) feature).getMaxTemperature()-10);
        seekbarMaxTemperatureHeater.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress+=10;
                txtMaxTemperatureHome.setText("Max. Temp: "+progress);
                ((TemperatureFeature) feature).setMaxTemperature(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbarTemperatureWaterHeater=((SeekBar) view.findViewById(R.id.SeekBarTemperatureHeater));
        seekbarTemperatureWaterHeater.setProgress(((TemperatureFeature) feature).getWaterTemperature()-20);
        seekbarTemperatureWaterHeater.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress+=20;
                txtWaterTemperatureHeater.setText("Temp. Acqua: "+progress);
                ((TemperatureFeature) feature).setWaterTemperature(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initializeWasher(){
        toggleWasher=((ToggleButton)view.findViewById(R.id.stateWasher));

        if(((WasherFeature) feature).isActive()){
            toggleWasher.setChecked(true);
        }else{
            toggleWasher.setChecked(false);
        }

        toggleWasher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ((WasherFeature) feature).setActive(isChecked);
            }
        });

        txtRemainingTimeWasher=((TextView) view.findViewById(R.id.RemainingTimeWasher));
        txtRemainingTimeWasher.setText("Tempo rimanente: "+((WasherFeature)feature).getRemainingTime());

        spinnerWasher=((Spinner) view.findViewById(R.id.spnProgram));
        ArrayAdapter<String> spinnerArrayAdapter=new ArrayAdapter<String>(getActivity(),
                R.layout.layout_spinner,new String []{"Programma 1","Programma 2","Programma 3","Programma 4","Programma 5","Programma 6","Programma 7","Programma 8","Programma 9","Programma 10"});
        spinnerArrayAdapter.setDropDownViewResource(R.layout.layout_spinner);
        spinnerWasher.setAdapter(spinnerArrayAdapter);
        spinnerWasher.setSelection(/*((WasherFeature)feature).getProgram()*/0);
        spinnerWasher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((WasherFeature)feature).setProgram(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initializeEnergy(){
        spinnerProfile=((Spinner) view.findViewById(R.id.spnProfile));
        txtLimitCurrent=((TextView) view.findViewById(R.id.currentLimit));
        txtLimitCurrent.setText("Limite: "+((EnergyFeature)feature).getLimitForSelectedProfile());
        txtCurrentConsumption=((TextView) view.findViewById(R.id.currentConsumption));
        txtCurrentConsumption.setText("Consumo corrente: "+((EnergyFeature)feature).getCurrentConsumption());
        ArrayAdapter<String> spinnerArrayAdapter=new ArrayAdapter<String>(getActivity(),
                R.layout.layout_spinner,new String []{"Basso","Bilanciato","Elevato"});
        spinnerArrayAdapter.setDropDownViewResource(R.layout.layout_spinner);
        spinnerProfile.setAdapter(spinnerArrayAdapter);
        spinnerProfile.setSelection(((EnergyFeature)feature).getProfile());

        spinnerProfile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((EnergyFeature)feature).setProfile(position);
                txtLimitCurrent.setText("Limite: "+((EnergyFeature)feature).getLimitForSelectedProfile());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void refreshAlarm(){
        txtCamera.setText("Camera: "+((AlarmFeature)feature).getStateCamera());

        if(((AlarmFeature) feature).isActiveCamera()){
            toggleCamera.setChecked(true);
        }else{
            toggleCamera.setChecked(false);
        }
        if(((AlarmFeature) feature).isActiveGas()){
            toggleGas.setChecked(true);
        }else{
            toggleGas.setChecked(false);
        }

        if(((AlarmFeature) feature).isActiveSmoke()){
            toggleSmoke.setChecked(true);
        }else{
            toggleSmoke.setChecked(false);
        }
    }

    public void refreshGarage(){
        if(((GarageFeature) feature).isOpen()){
            toggleGarage.setChecked(true);
        }else{
            toggleGarage.setChecked(false);
        }

       txtGarage.setText("Stato: "+((GarageFeature) feature).getState());
    }

    public void refreshIrrigation(){
        if(((IrrigationFeature) feature).isActive()){
            toggleIrrigation.setChecked(true);
        }else{
            toggleIrrigation.setChecked(false);
        }
        txtSoil.setText("Terreno: "+((IrrigationFeature) feature).getSoil());
        txtTank.setText("Serbatoio: "+((IrrigationFeature) feature).getTank());
    }

    public void refreshLight(){
        if(((LightFeature) feature).getState()){
            toggleLight.setChecked(true);
        }else{
            toggleLight.setChecked(false);
        }

        if(((LightFeature) feature).getStateSensorLight()){
            toggleExternalLight.setChecked(true);
        }else{
            toggleExternalLight.setChecked(false);
        }

        txtExternalLight.setText("Luce esterna: "+((LightFeature) feature).getBrightness());
        seekbarExternalLight.setProgress(((LightFeature) feature).getBrightness());
    }

    public void refreshOven(){
        if(((OvenFeature) feature).isActive()){
            toggleOven.setChecked(true);
        }else{
            toggleOven.setChecked(false);
        }

        edittextModifyTimeOven.setText("");

        txtTemperatureOven.setText("Temperatura: "+((OvenFeature) feature).getTemperature());
        txtRemainingTimeOven.setText("Tempo rimanente: "+((OvenFeature) feature).getRemainingTime());
        seekbarTemperatureOven.setProgress(((OvenFeature) feature).getTemperature()-90);
    }

    public void refreshTemperature(){
        if(((TemperatureFeature) feature).isActive()){
            toggleHeater.setChecked(true);
        }else{
            toggleHeater.setChecked(false);
        }

        txtTemperatureHome.setText("Temperatura: "+((TemperatureFeature) feature).getTemperature());
        txtMinTemperatureHome.setText("Min. Temp: "+((TemperatureFeature) feature).getMinTemperature());
        txtMaxTemperatureHome.setText("Max. Temp: "+((TemperatureFeature) feature).getMaxTemperature());
        txtWaterTemperatureHeater.setText("Temp. Acqua: "+((TemperatureFeature) feature).getWaterTemperature());

        seekbarMinTemperatureHeater.setProgress(((TemperatureFeature) feature).getMinTemperature()-10);
        seekbarMaxTemperatureHeater.setProgress(((TemperatureFeature) feature).getMaxTemperature()-10);
        seekbarTemperatureWaterHeater.setProgress(((TemperatureFeature) feature).getWaterTemperature()-20);

        adapter.notifyDataSetChanged();

    }

    public void refreshWasher(){
        if(((WasherFeature) feature).isActive()){
            toggleWasher.setChecked(true);
        }else{
            toggleWasher.setChecked(false);
        }

        txtRemainingTimeWasher.setText("Tempo rimanente: "+((WasherFeature)feature).getRemainingTime());

        spinnerWasher.setSelection(((WasherFeature)feature).getProgram());
    }

    public void refreshEnergy(){
        spinnerProfile.setSelection(((EnergyFeature)feature).getProfile());
        txtLimitCurrent.setText("Limite: "+((EnergyFeature)feature).getLimitForSelectedProfile());
        txtCurrentConsumption.setText("Consumo corrente: "+((EnergyFeature)feature).getCurrentConsumption());
    }

    public void setEnabled(boolean enabled){
        synchronized (lock){
            this.enabled=enabled;
            if(view!=null){
                enableDisableViewGroup((ViewGroup)view);
            }
        }
    }

    private void enableDisableViewGroup(ViewGroup viewGroup){

        for(int i=0;i<viewGroup.getChildCount();i++){
            View view=viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if(view instanceof ViewGroup){
                enableDisableViewGroup((ViewGroup)view);
            }
        }
    }
}
