package com.example.francesco.smarthome;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;


public class MainActivity extends MyActivity {

    static boolean running = false;
    private boolean flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        landscape = false;
        application_intent = getIntent();
        //se l'applicazione non è stata aperta dai task recenti

        if (application_intent != null &&  application_intent.getFlags()!= 269500416 &&  application_intent.getFlags()!= 873480192 &&  application_intent.getFlags()!= 877674496) {
           checkIntent();
        }else{
            application_intent=null;
        }

        //se il dispositivo è in modalità landscape e ha uno schermo >= 7 pollici
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE && isTablet()) {

            /* se l'applicazione non è aperta si prosegue, altrimenti si forza la chiusura dell'activity.
             Questo caso si verifica quando l'applicazione è aperta e ci si trova in landscape e si vuole
             visualizzare una notifica, in quanto al momento della visualizzazione viene invocata questa activity,
             è necessario controllare per i dispositivi con schermo >= 7 pollici se l'applicazione è aperta e
             se si trova in landscape
             */
            if(!LandScapeActivity.running && !running) {
                running=false;
                Intent intent = new Intent(getApplicationContext(), LandScapeActivity.class);

                putExtraIntent(savedInstanceState, intent);

                startActivity(intent);
                finish();
            }else{
                finish();
            }

        } else {
            if(!running && !LandScapeActivity.running) {
                running=true;

                if (getFragmentManager().findFragmentByTag(EVENTLIST) == null && getFragmentManager().findFragmentByTag(DETAIL) == null) {
                    eventFragment = new FeatureFragment();

                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.add(R.id.container, eventFragment, EVENTLIST);
                    transaction.commit();
                }
            }else{
                flag=true;
                finish();
            }
        }

    }

    //quando si passa dalla modalit� portrait-landscape o viceversa, le animazioni impostate per i vari
    //fragment vanno persi, e per mantenere l'animazione � stato necessario impostare nel manifest il
    //seguente parametro android:configChanges="orientation|screenSize"
    //il problema di questo parametro � che quando si passa in portrait/landscape , l'activity non viene ricreata.
    //Senza l'invocazione del metodo onCreate non � possibile verificare se il dispositivo � un tablet, e se si
    //trova in modalit� landscape, lanciare una nuova activity.Per questo motivo � stato necessario eseguire l'override
    //di questo metodo e forzare la creazione dell'activity.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
       if(isTablet()){
           recreate();
       }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(!flag) {
            running = false;
        }else {
            flag=false;
        }
    }

}
