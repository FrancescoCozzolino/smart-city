package com.example.francesco.smarthome;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.restlet.resource.ClientResource;
import org.restlet.ext.json.JsonRepresentation;
import org.json.JSONObject;
import org.restlet.data.MediaType;
import android.util.Log;


/**
 * Created by Francesco on 24/12/2016.
 */

public class FirebaseToken extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // TODO: Implement this method to send any registration to your app's servers.

        ClientResource resource = new ClientResource("http://192.168.1.103:8182/home/token");

        try {
            JsonRepresentation entity = new JsonRepresentation(new JSONObject().put("token",refreshedToken));
            resource.post(entity, MediaType.APPLICATION_JSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
