package com.example.francesco.smarthome;

import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Francesco on 19/04/2015.
 */
public abstract class Feature implements Serializable {

    private String name;

    public Feature(String name) {
        this.name=name;
    }

    public String getName(){
        return name;
    }

    public abstract void setParam(JSONObject json);

    public abstract JSONArray getParam() throws JSONException;

}
