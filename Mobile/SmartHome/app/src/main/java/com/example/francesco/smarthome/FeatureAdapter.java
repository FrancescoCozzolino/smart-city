package com.example.francesco.smarthome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Francesco on 19/04/2015.
 */
public class FeatureAdapter  extends ListAdapter{


    public FeatureAdapter(Context context, List<Feature> features){
        super(context,features);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_event,parent,false);
        }

        ((TextView)convertView.findViewById(R.id.nameEvent)).setText(((Feature)getItem(position)).getName());

        if (position == selectedItemPosition) {
            convertView.setBackgroundColor(context.getResources().getColor(R.color.light_purple2));
        }
        else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        return convertView;
    }
}
