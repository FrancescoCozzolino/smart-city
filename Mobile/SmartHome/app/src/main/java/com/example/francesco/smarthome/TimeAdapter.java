package com.example.francesco.smarthome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Francesco on 18/01/2017.
 */

public class TimeAdapter extends ListAdapter{

    private TemperatureFeature temperatureFeature;

    public TimeAdapter(Context context, TemperatureFeature temperatureFeature){
        super(context,temperatureFeature.getTimes());
        this.temperatureFeature=temperatureFeature;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = LayoutInflater.from(context).inflate(R.layout.list_time,parent,false);
        }

        ((TextView)convertView.findViewById(R.id.nameEvent)).setText(((TimeHeater)getItem(position)).toString());

        ((ImageView)convertView.findViewById(R.id.imageView)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temperatureFeature.removeTime(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
