package com.example.francesco.smarthome;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

/**
 * Created by Francesco on 18/07/2015.
 */
public class LandScapeActivity extends MyActivity {

    /* variabile utilizzata quando l'applicazione � aperta e si trova in landscape e si vuole
    visualizzare una notifica
     */
    static boolean running=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landscape);

        running=true;
        landscape=true;

        application_intent=getIntent();
        checkIntent();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            running=false;
            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
            putExtraIntent(savedInstanceState,intent);
            startActivity(intent);
            finish();
        }else {
            eventFragment = new FeatureFragment();
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.fragmentEventList, eventFragment,EVENTLIST);
            transaction.commit();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        running=false;
    }
}
