package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Francesco on 02/01/2017.
 */

public class WasherFeature extends Feature {

    private int program,remainingTime;
    private boolean isActive,currentState;

    public WasherFeature(){
        super("Lavatrice");

        isActive=false;
        program=0;
        remainingTime=0;
    }

    public int getProgram() {
        return program;
    }

    public void setProgram(int program) {
        this.program = program;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getRemainingTime() {
        return remainingTime;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            isActive=json.getString("val3").compareTo("on")==0?true:false;
            currentState=isActive;
            program=json.getInt("val2");
            remainingTime=json.getInt("val1");
            if(remainingTime<0){
                remainingTime=0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException{
        JSONArray array=new JSONArray();

        if(currentState!=isActive){
            JSONObject object=new JSONObject();
            object.put("val2",isActive?1:0);
            object.put("val1",program);
            array.put(new JSONObject().put("washer",object));
        }

        return array;
    }
}
