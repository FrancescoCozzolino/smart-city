package com.example.francesco.smarthome;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francesco on 19/04/2015.
 */
public class FeatureFragment extends Fragment implements AdapterView.OnItemClickListener{

    private ListView listView;
    private List<Feature> features =null;
    private EventListener listener;
    private FeatureAdapter adapter=null;
    private int position=-1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        view.setBackgroundColor(Color.WHITE);

        features=new ArrayList<>();
        features.add(FeatureFactory.getAlarmFeature());
        features.add(FeatureFactory.getEnergyFeature());
        features.add(FeatureFactory.getOvenFeature());
        features.add(FeatureFactory.getGarageFeature());
        features.add(FeatureFactory.getIrrigationFeature());
        features.add(FeatureFactory.getWasherFeature());
        features.add(FeatureFactory.getLightFeature());
        features.add(FeatureFactory.getTemperatureFeature());
        listView=(ListView) view.findViewById(R.id.eventList);

        adapter=new FeatureAdapter(getActivity(), features);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(activity instanceof EventListener){
            listener=(EventListener)activity;
        }

    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener=null;
    }

    public void clear(){
        if(adapter!=null){
           ((FeatureAdapter) listView.getAdapter()).selectItem(-1);

            listView.clearChoices();
            adapter.notifyDataSetChanged();
        }
    }

    public void selectItem(String name){
        if(features !=null && adapter!=null){
            for(int i = 0; i< features.size(); i++){
                if(features.get(i).getName().equals(name)){
                    ((FeatureAdapter) listView.getAdapter()).selectItem(i);
                    listView.clearChoices();
                    adapter.notifyDataSetChanged();
                    i= features.size();
                }
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listener.onClickEventListener(features.get(position));
        ((FeatureAdapter) listView.getAdapter()).selectItem(position);
        ((ListView) parent).invalidateViews();
        this.position=position;
    }

    public interface EventListener{
        void onClickEventListener(Feature feature);
    }

}
