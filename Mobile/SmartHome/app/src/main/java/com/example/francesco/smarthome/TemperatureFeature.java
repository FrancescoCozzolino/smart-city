package com.example.francesco.smarthome;

import android.os.Build;
import android.support.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Francesco on 02/01/2017.
 */

public class TemperatureFeature extends Feature {
    private boolean isActive,currentState;
    private int minTemperature,maxTemperature,waterTemperature,currentMinTemp,currentMaxTemp,currentWaterTemp;
    private double temperature;
    private List<TimeHeater> time,timeAdd,timeRemove;

    public TemperatureFeature(){
        super("Termostato");

        time=new ArrayList<>();
        timeAdd=new ArrayList<>();
        timeRemove=new ArrayList<>();
        minTemperature=18;
        maxTemperature=24;
        waterTemperature=50;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(int minTemperature) {
        this.minTemperature = minTemperature;
    }

    public int getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(int maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public int getWaterTemperature() {
        return waterTemperature;
    }

    public void setWaterTemperature(int waterTemperature) {
        this.waterTemperature = waterTemperature;
    }

    public double getTemperature() {
        return temperature;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            time.clear();
            timeAdd.clear();
            timeRemove.clear();
            temperature=json.getDouble("val6");
            minTemperature=json.getInt("val5");
            currentMinTemp=minTemperature;
            maxTemperature=json.getInt("val4");
            currentMaxTemp=maxTemperature;
            isActive=json.getBoolean("val3");
            currentState=isActive;
            waterTemperature=json.getInt("val2");
            currentWaterTemp=waterTemperature;
            buildTimeHeater(json.getString("val1"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void buildTimeHeater(String string){

        if(!string.equals("")) {
            String[] arrayTime = string.split("\n");

            for(String tmp:arrayTime){
                String[] tmp2=tmp.split(" ");
                int endHourMinute=Integer.parseInt(tmp2[1])*60+Integer.parseInt(tmp2[2])+((int)(Long.parseLong(tmp2[3])/60000));
                int hour=endHourMinute/60;
                int minute=endHourMinute%60;
                String endTime=hour<10?"0"+hour:Integer.toString(hour);
                if(minute<10){
                    endTime+=":0"+minute;
                }else{
                    endTime+=":"+minute;
                }

                time.add(new TimeHeater(Integer.parseInt(tmp2[0]),tmp2[1]+":"+tmp2[2],endTime));
            }

            Collections.sort(time, new Comparator<TimeHeater>() {
                @Override
                public int compare(TimeHeater o1, TimeHeater o2) {
                    int day1=o1.getDayOfWeekNumber();
                    int day2=o2.getDayOfWeekNumber();

                    if(day1==day2){
                        String[] start1=o1.getTimeStart().split(":");
                        String[] start2=o2.getTimeStart().split(":");
                        int timeMinute1=((Integer.parseInt(start1[0])*60)+Integer.parseInt(start1[1]));
                        int timeMinute2=((Integer.parseInt(start2[0])*60)+Integer.parseInt(start2[1]));

                        return timeMinute1>timeMinute2?1:timeMinute1==timeMinute2?0:-1;
                    }else{
                        return day1>day2?1:day1==day2?0:-1;
                    }
                }
            });
        }
    }

    @Override
    public JSONArray getParam() throws JSONException{
        JSONArray array=new JSONArray();

        if(currentMinTemp!=minTemperature){
            array.put(new JSONObject().put("setMinTemp",new JSONObject().put("value",minTemperature)));
        }

        if(currentMaxTemp!=maxTemperature) {
            array.put(new JSONObject().put("setMaxTemp",new JSONObject().put("value",maxTemperature)));
        }

        if(currentWaterTemp!=waterTemperature){
            array.put(new JSONObject().put("setWaterTemp",new JSONObject().put("value",waterTemperature)));
        };

        if(currentState!=isActive){
            array.put(new JSONObject().put("setHeater",new JSONObject().put("value",isActive?1:0)));
        }

        if(timeAdd.size()!=0){
            array.put(new JSONObject().put("addTime",new JSONObject().put("value",buildString(timeAdd))));
        }

        if(timeRemove.size()!=0){
            array.put(new JSONObject().put("removeTime",new JSONObject().put("value",buildString(timeRemove))));
        }

        return array;
    }

    private String buildString(List<TimeHeater> list){
        String string="\"";

        for(int i=0;i<list.size()-1;i++){
            TimeHeater tmp=list.get(i);
            string+=tmp.getDayOfWeekNumber()+" "+tmp.getTimeStart().replace(":"," ")+" "+tmp.getDuration()+"-";
        }

        TimeHeater tmp=list.get(list.size()-1);
        string+=tmp.getDayOfWeekNumber()+" "+tmp.getTimeStart().replace(":"," ")+" "+tmp.getDuration()+"\"";

        return string;
    }

    public void addTime(int dayOfWeek, final String startTime, String endTime){

        TimeHeater tmp=new TimeHeater(dayOfWeek,startTime,endTime);

        time.add(tmp);
        timeAdd.add(tmp);

        Collections.sort(time, new Comparator<TimeHeater>() {
            @Override
            public int compare(TimeHeater o1, TimeHeater o2) {
                int day1=o1.getDayOfWeekNumber();
                int day2=o2.getDayOfWeekNumber();

                if(day1==day2){
                    String[] start1=o1.getTimeStart().split(":");
                    String[] start2=o2.getTimeStart().split(":");
                    int timeMinute1=((Integer.parseInt(start1[0])*60)+Integer.parseInt(start1[1]));
                    int timeMinute2=((Integer.parseInt(start2[0])*60)+Integer.parseInt(start2[1]));

                    return timeMinute1>timeMinute2?1:timeMinute1==timeMinute2?0:-1;
                }else{
                    return day1>day2?1:day1==day2?0:-1;
                }
            }
        });
    }

    public void removeTime(int index){
        TimeHeater tmp=time.remove(index);
        timeAdd.remove(tmp);
        timeRemove.add(tmp);
    }

    public List<TimeHeater> getTimes(){
        return time;
    }
}
