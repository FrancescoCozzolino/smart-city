package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

/**
 * Created by Francesco on 02/01/2017.
 */

public class AlarmFeature extends Feature {

    private boolean isActiveGas,isActiveSmoke,isActiveCamera,currentStateGas,currentStateSmoke,currentStateCamera;
    private String stateCamera;

    public AlarmFeature(){
        super("Allarmi");
        isActiveGas=false;
        isActiveSmoke=false;
        stateCamera="off";
    }

    public boolean isActiveGas() {
        return isActiveGas;
    }

    public void setActiveGas(boolean activeGas) {
        isActiveGas = activeGas;
    }

    public boolean isActiveSmoke() {
        return isActiveSmoke;
    }

    public void setActiveSmoke(boolean activeSmoke) {
        isActiveSmoke = activeSmoke;
    }

    public String getStateCamera() {
        return stateCamera;
    }

    public boolean isActiveCamera() {
        return isActiveCamera;
    }

    public void setActiveCamera(boolean activeCamera) {
        isActiveCamera = activeCamera;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            isActiveSmoke=json.getInt("val3")==0?false:true;
            currentStateSmoke=isActiveSmoke;
            isActiveGas=json.getInt("val2")==0?false:true;
            currentStateGas=isActiveGas;
            stateCamera=json.getString("val1");
            isActiveCamera=stateCamera.compareTo("off")==0?false:true;
            currentStateCamera=isActiveCamera;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException {
        JSONArray array=new JSONArray();

        if(currentStateSmoke!=isActiveSmoke){
            array.put(new JSONObject().put("alarmSmoke",new JSONObject().put("state",isActiveSmoke?1:0)));
        }

        if(currentStateGas!=isActiveGas){
            array.put(new JSONObject().put("alarmGas",new JSONObject().put("state",isActiveGas?1:0)));
        }

        if(currentStateCamera!=isActiveCamera){
            array.put(new JSONObject().put("alarmCamera",new JSONObject().put("state",isActiveCamera?1:0)));
        }

        return array;
    }
}
