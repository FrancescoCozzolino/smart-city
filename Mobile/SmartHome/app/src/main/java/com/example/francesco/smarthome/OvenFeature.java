package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Francesco on 02/01/2017.
 */

public class OvenFeature extends Feature {
    private boolean isActive,currentState;
    private int temperature,currentTemperature,remainingTime,modifyTime;

    public OvenFeature(){
        super("Forno");
        isActive=false;
        temperature=90;
        remainingTime=0;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getRemainingTime() {
        return remainingTime;
    }

    public void setModifyTime(int modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public void setParam(JSONObject json){
        try {
            isActive=json.getBoolean("val3");
            currentState=isActive;
            int tmp=json.getInt("val2");
            if(tmp!=0){
                temperature=tmp;
                currentTemperature=temperature;
            }else{
                temperature=currentTemperature;
            }
            remainingTime=json.getInt("val1");
            if(remainingTime<0){
                remainingTime=0;
            }

            modifyTime=0;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException{
        JSONArray array=new JSONArray();

        if((isActive && !currentState)||(!isActive && currentState)){
            if(isActive){
                if(modifyTime!=0){
                    JSONObject object=new JSONObject();
                    object.put("val2",temperature);
                    object.put("val1",modifyTime);

                    array.put(new JSONObject().put("turnOnOven",object));
                }

            }else{
                array.put(new JSONObject().put("turnOffOven",new JSONObject().put("state",isActive?1:0)));
            }
        }else{
            if(isActive&&currentState){
                if(modifyTime!=0){
                    array.put(new JSONObject().put("changeTime",new JSONObject().put("time",modifyTime)));
                }

                if(temperature!=currentTemperature){
                    array.put(new JSONObject().put("changeDegrees",new JSONObject().put("temperature",temperature)));
                }
            }
        }

        return array;
    }
}
