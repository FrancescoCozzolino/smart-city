package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Francesco on 02/01/2017.
 */

public class LightFeature extends Feature{

    private int brightness,currentBrightness;
    private boolean state, stateSensorLight,currentState,currentStateSensorLight;

    public LightFeature(){
        super("Luci");
    }

    public int getBrightness() {
        return brightness;
    }

    public void setBrightness(int brightness) {
        this.brightness = brightness;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public boolean getStateSensorLight() {
        return stateSensorLight;
    }

    public void setStateSensorLight(boolean stateSensorLight) {
        this.stateSensorLight = stateSensorLight;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            state=json.getInt("val3")==0?false:true;
            currentState=state;
            stateSensorLight=json.getInt("val2")==0?false:true;
            currentStateSensorLight=stateSensorLight;
            brightness=json.getInt("val1");
            currentBrightness=brightness;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException{
        JSONArray array=new JSONArray();

        if(currentState!=state){
            array.put(new JSONObject().put("light",new JSONObject().put("state",state?1:0)));
        }

        if((currentStateSensorLight!=stateSensorLight) || (!stateSensorLight && currentBrightness!=brightness)){
            JSONObject object=new JSONObject();
            object.put("val2",stateSensorLight?1:0);
            object.put("val1",brightness);
            array.put(new JSONObject().put("externalLight",object));
        }
        return array;
    }
}
