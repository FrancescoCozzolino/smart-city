package com.example.francesco.smarthome;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Francesco on 18/01/2017.
 */

public abstract class ListAdapter extends BaseAdapter {
    private List<? extends Object> list =null;
    protected Context context;
    protected int selectedItemPosition=-1;

    public ListAdapter(Context context, List<? extends Object> list){
        this.list = list;
        this.context = context;
    }

    public void selectItem(int i) {
        selectedItemPosition = i;
    }

    @Override
    public int getCount() {
        return list !=null? list.size():0;
    }

    @Override
    public Object getItem(int position) {
        return (list !=null && position< list.size())? list.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

}
