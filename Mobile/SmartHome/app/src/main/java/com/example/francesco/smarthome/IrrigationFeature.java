package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Francesco on 02/01/2017.
 */

public class IrrigationFeature extends Feature {

    private String soil,tank;
    private boolean isActive,currentState;

    public IrrigationFeature(){
        super("Irrigazione");

        tank="full";
        soil="dry";
        isActive=false;
    }

    public String getSoil() {
        return soil;
    }

    public String getTank() {
        return tank;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            soil=json.getString("val2");
            isActive=soil.isEmpty()?false:true;
            currentState=isActive;
            tank=json.getBoolean("val1")?"vuoto":"pieno";
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException{
        JSONArray array=new JSONArray();

        if(currentState!=isActive){
            array.put(new JSONObject().put("moistureSensor",new JSONObject().put("state",isActive?1:0)));
        }

        return array;
    }
}
