package com.example.francesco.smarthome;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Francesco on 02/01/2017.
 */

public class GarageFeature extends Feature {

    private String state;
    private boolean isOpen,currentOpen;

    public GarageFeature(){
        super("Garage");

        isOpen=false;
        state="close";
    }

    public String getState() {
        return state;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    @Override
    public void setParam(JSONObject json) {
        try {
            state=json.getString("val1");

            if(state.compareTo("close")==0 || state.compareTo("opening")==0){
                isOpen=false;
            }else{
                isOpen=true;
            }

            currentOpen=isOpen;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public JSONArray getParam() throws JSONException {
        JSONArray array=new JSONArray();

        if(currentOpen!=isOpen | state.compareTo("stop")==0){
            array.put(new JSONObject().put("garage",new JSONObject().put("state",isOpen?1:0)));
        }

        return array;
    }
}
