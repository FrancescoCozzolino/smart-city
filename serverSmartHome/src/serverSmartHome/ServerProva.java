package serverSmartHome;

import java.io.IOException;
import java.util.ArrayList;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class ServerProva extends Agent {

	 private AID[] agents;
	 private String value="1";
	// Put agent initializations here
	  protected void setup() {
	      // Printout a welcome message
	      System.out.println("Hello! Server-agent "+getAID().getName()+" is ready.");
	      
	   // Add a TickerBehaviour that schedules a request to seller agents every minute
	        addBehaviour(new TickerBehaviour(this, 10000) {
	          protected void onTick() {
	            // Update the list of seller agents
	            DFAgentDescription template = new DFAgentDescription();
	            ServiceDescription sd = new ServiceDescription();
	            sd.setType("externalLight");
	            template.addServices(sd);
	            //cerco l'agente con servizio luce
	            try {
	              DFAgentDescription[] result = DFService.search(myAgent, template); 
	              System.out.println("Found the following agents:");
	              agents = new AID[result.length];
	              for (int i = 0; i < result.length; ++i) {
	                agents[i] = result[i].getName();
	                System.out.println(agents[i].getName());
	              }
	              
	              if(value.compareTo("1")==0){
	            	  value="0";
	              }else{
	            	  value="1";
	              }
	              
	              myAgent.addBehaviour(new RequestPerformer());
	              
	            } catch (FIPAException fe) {
	              fe.printStackTrace();
	            }
	          
	          }
	        } );

	  }

	  // Put agent clean-up operations here
	  protected void takeDown() {
	    // Printout a dismissal message
	    System.out.println("Server-agent "+getAID().getName()+" terminating.");
	  }

	  private class RequestPerformer extends Behaviour {
		    private MessageTemplate mt;
		    private boolean flag=false;
		    
		    
		    public void action() {
		         // Send the cfp to all sellers
		          ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
		          for (int i = 0; i < agents.length; ++i) {
		            cfp.addReceiver(agents[i]);
		          } 
		          ArrayList<Object> list=new ArrayList<>();
		          list.add(value);
		          list.add("ciao");
		          try {
					cfp.setContentObject(list);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		          //cfp.setContent(value);
		          cfp.setConversationId("book-trade");//forse non serve
		          cfp.setReplyWith("cfp"+System.currentTimeMillis()); // Unique value
		          myAgent.send(cfp);
		          // Prepare the template to get proposals
		          mt = MessageTemplate.and(MessageTemplate.MatchConversationId("book-trade"),
		                                   MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
		          
		          try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		          flag=true;
		        
		    }

			@Override
			public boolean done() {
				// TODO Auto-generated method stub
				return flag;
			}
		    
	  }  // End of inner class RequestPerformer
}
