package serverSmartHome;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Semaphore;

import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Method;

public class Token extends Restlet{

	private Semaphore s;
	
	public Token(Semaphore s){
		this.s=s;
	}
	
	@Override
	public void handle(Request request, Response response) {
		if(request.getMethod()==Method.POST){
			try{
				JSONObject json=new JSONObject(request.getEntityAsText());
				if(json.has("token")){
					s.acquire();
					PrintWriter writer = new PrintWriter("token.txt", "UTF-8");
			    	writer.println(json.getString("token"));
			    	writer.close();
			    	s.release();
				}
			} catch (IOException | InterruptedException e) {
			   e.printStackTrace();
			}
		}
	}
}
