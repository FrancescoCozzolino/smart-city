package serverSmartHome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

public class ProvaNotifiche {

	public final static String AUTH_KEY_FCM = "AIzaSyB7yS4PxC75s9vtUuFVGyqrhN7so_6Mars";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	
	public static void main(String[] args) {

		try{
			if(new File("token.txt").exists()){
				BufferedReader br = new BufferedReader(new FileReader("token.txt"));
				String userDeviceIdKey = br.readLine();
				br.close();
				System.out.println(userDeviceIdKey);
				System.out.println("Ho letto il token");
				System.out.println("Invio la richiesta");
				
				URL url = new URL(API_URL_FCM);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();

				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				conn.setRequestMethod("POST");
				conn.setRequestProperty("Authorization","key="+AUTH_KEY_FCM);
				conn.setRequestProperty("Content-Type","application/json");

				JSONObject json = new JSONObject();
				json.put("to",userDeviceIdKey.trim());
				JSONObject info = new JSONObject();
				info.put("title", "Ciao");   // Notification title
				info.put("body", "Ciao a tutti"); // Notification body
				json.put("notification", info);

				OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
				wr.write(json.toString());
				wr.flush();
				wr.close();
				conn.getInputStream();
				
				System.out.println("ciao");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
