package serverSmartHome;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

import org.json.JSONObject;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.lang.acl.ACLMessage;

/**
 * Class that represents two parralel behaviours. One behaviour create a server. 
 * The second one, handle the message between the server and the jason agents that 
 * represent the functionalities of a home
 * @author Francesco
 *
 */
public class HandleClientAgent extends ParallelBehaviour{
	private static final long serialVersionUID = 1L;
	private DeviceResource resource;
	private Semaphore s,s2;
	private int agents;
	private JSONObject json;
	
	public HandleClientAgent(ServerAgent agent){
		super(agent,ParallelBehaviour.WHEN_ALL);
		
		s=new Semaphore(0);
		s2=new Semaphore(1);
		resource=new DeviceResource(agent,s);
		json=new JSONObject();
		agents=agent.getAgents();
		
		addSubBehaviour(new Behaviour(agent){

			private static final long serialVersionUID = 1L;
			private boolean flag=false;
			
			@Override
			public void action() {
				Application myApp = new Application() {
			        @Override
			        public org.restlet.Restlet createInboundRoot() {
			            Router router = new Router(getContext());
					    router.attach("/devices", resource);
					    router.attach("/token",new TokenResource(s2));
					    return router;
			        };
			    };


			    Component component = new Component();
			    component.getDefaultHost().attach("/home", myApp);
			
			    try {
					new Server(Protocol.HTTP, 8182, component).start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				synchronized(this){
					flag=true;
				}
			}

			@Override
			public synchronized boolean done() {
				return flag;
			}

			
		});
		
		
		addSubBehaviour(new CyclicBehaviour(agent){

			private final static String AUTH_KEY_FCM = "AIzaSyB7yS4PxC75s9vtUuFVGyqrhN7so_6Mars";
			private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
			private final static String GAS_MESSAGE="Livello di gas superiore alla norma";
			private final static String SMOKE_MESSAGE="Rilevata presenza di fumo";
			private final static String CAMERA_MESSAGE="Rilevata presenza di persone";
			private final static String CLOUD_MESSAGE="Video disponibile sul cloud";
			private static final long serialVersionUID = 1L;
			private int count=0;
			private int countResponse=0;
			private long timeStampGas=0,timeStampSmoke=0,timeStampCamera=0;
			
			@Override
			public void action() {
				ACLMessage message=myAgent.blockingReceive();
				String tmp=message.getContent();
				
				if(ACLMessage.getPerformative(message.getPerformative()).compareTo("INFORM")==0){
					if(tmp.startsWith("[")){
						String[] array=tmp.replaceAll("\\[","").replaceAll("\\]","").replaceAll("\"", "").split(",");
						JSONObject obj=new JSONObject();
						
						int i=array.length;
						for(String value:array){
							obj.put("val"+(i--),value);
						}
						json.put(message.getSender().getLocalName(), obj);	
						
						if(++count==agents){
							resource.setEntity(json.toString());
							s.release();
							count=0;
							json=new JSONObject();
						}
					}else{
						if(++countResponse==((ServerAgent)myAgent).getNumberOfResponse()){
							s.release();
							countResponse=0;
						}
					}
				}else if(ACLMessage.getPerformative(message.getPerformative()).compareTo("REQUEST")==0){
					String[] array=tmp.replaceAll("\\[","").replaceAll("\\]","").replaceAll("\"", "").split(",");
							
					if(message.getSender().getLocalName().compareTo("alarm")==0){
						if(array[0].compareTo("alarmGas")==0){
							long time=Long.parseLong(array[1]);
							
							if(time-timeStampGas>60000){
								timeStampGas=time;
								sendNotification("Allarme Gas",GAS_MESSAGE);		
							}
							
							writeFile(millisToDate(time)+"\t "+GAS_MESSAGE+System.lineSeparator());
						}else if(array[0].compareTo("alarmSmoke")==0){
							long time=Long.parseLong(array[1]);
							
							if(time-timeStampSmoke>60000){
								timeStampSmoke=time;
								sendNotification("Allarme Fumo",SMOKE_MESSAGE);
							}
							
							writeFile(millisToDate(time)+"\t "+SMOKE_MESSAGE+System.lineSeparator());
						}else if(array[0].compareTo("alarmCamera")==0){
							long time=Long.parseLong(array[1]);
							
							if(time-timeStampCamera>60000){
								timeStampCamera=time;
								sendNotification("Videosorveglianza",CAMERA_MESSAGE);
							}
							
							writeFile(millisToDate(time)+"\t "+CAMERA_MESSAGE+System.lineSeparator());
						}else if(array[0].startsWith("video")){
							sendNotification("Videosorveglianza",CLOUD_MESSAGE);
						}
					}
				}
	
			}
			
			private String millisToDate(long value){
				return new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss").format(new Date(value));
			}
			
			private void writeFile(String text){
				File file=new File("log.txt");
				FileWriter fileWriter=null;
				BufferedWriter writer=null;
				
				try {
					if (!file.exists()) {
						file.createNewFile();
					}

					fileWriter = new FileWriter(file.getAbsoluteFile(), true);
					writer = new BufferedWriter(fileWriter);

					writer.write(text);
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (writer != null){
							writer.close();
						}

						if (fileWriter != null){
							fileWriter.close();
						}
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
			
			private void sendNotification(String title,String body){
			
				try{
					if(new File("token.txt").exists()){
						s2.acquire();
						BufferedReader br = new BufferedReader(new FileReader("token.txt"));
						String userDeviceIdKey = br.readLine();
						br.close();
						s2.release();
			
						URL url = new URL(API_URL_FCM);
						HttpURLConnection conn = (HttpURLConnection) url.openConnection();

						conn.setUseCaches(false);
						conn.setDoInput(true);
						conn.setDoOutput(true);

						conn.setRequestMethod("POST");
						conn.setRequestProperty("Authorization","key="+AUTH_KEY_FCM);
						conn.setRequestProperty("Content-Type","application/json");

						JSONObject json = new JSONObject();
						json.put("to",userDeviceIdKey.trim());
						JSONObject info = new JSONObject();
						info.put("title", title);
						info.put("body", body);
						json.put("notification", info);

						OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
						wr.write(json.toString());
						wr.flush();
						wr.close();
						conn.getInputStream();
					}
				}catch(Exception e){
					e.printStackTrace();
				}

			}
			
		});
	}
}
