package serverSmartHome;

import jade.core.Agent;

/**
 * JADE agent that has a two parallel behaviours. One behaviour create a server. 
 * The second one handle the messages between the server and the jason agents that 
 * represent the functionalities of a home
 * @author Francesco Cozzolino
 *
 */
public class ServerAgent extends Agent{
	
	private static final long serialVersionUID = 1L;
	private int agents=8;
	private int numberOfResponse;
	
	@Override
	protected void setup() {
		 addBehaviour(new HandleClientAgent(this));
	}
	
	/**
	 * 
	 * @return The number of jason agents that handle the home
	 */
	public int getAgents(){
		return agents;
	}
	
	/**
	 * Set the number of responses that the server waits
	 * @param n Number of waited responses
	 */
	public void setNumberOfResponse(int n){
		numberOfResponse=n;
	}
	
	/**
	 * 
	 * @return The number of waited responses
	 */
	public int getNumberOfResponse(){
		return numberOfResponse;
	}
}
