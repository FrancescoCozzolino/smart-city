package serverSmartHome;

import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import jade.core.AID;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.io.IOException;

/**
 *  Resource that modifies the state of devices in a home. Also, it possible get
 *  the state of the devices
 * @author Francesco Cozzolino
 *
 */
public class DeviceResource extends Restlet{
 
	private ServerAgent agent;
	private String entity;
	private Semaphore s;
	
	public DeviceResource(ServerAgent agent,Semaphore s){
		super();
		this.agent=agent;
		this.s=s;
	}
	
	@Override
	public void handle(Request request, Response response) {
		Method method=request.getMethod();
		
		if(method==Method.GET){
			sendACLMessage("","getStatus",ACLMessage.REQUEST,"getStatus","get"+System.currentTimeMillis());
			
			try {
				s.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			response.setEntity(entity,MediaType.APPLICATION_JSON);
		}else if(method==Method.PUT){
			JSONObject json=new JSONObject(request.getEntityAsText());
			Set<String> set=json.keySet();
			agent.setNumberOfResponse(set.size());
			
			for(String key:set){
				JSONObject object=json.getJSONObject(key);
				ArrayList<Object> list=new ArrayList<>();
				Set<String> innerSet = object.keySet();
				
				list.add(key);
				for(String tmp: innerSet){
					list.add(object.get(tmp));
				}
		
				sendACLMessage(key,list,ACLMessage.REQUEST,"setStatus","set"+System.currentTimeMillis());
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			try {
				s.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
	}

	private void sendACLMessage(String type,Serializable content,int perf,String conversationId,String reply){
		DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        
        // cerco gli agenti che mi offrono un tipo di servizio, la stringa type="" cerca tutti gli agenti 
        if(type!=null && type!=""){
        	sd.setType(type);
        }
        
        template.addServices(sd);
        
        try {
			DFAgentDescription[] result = DFService.search(agent, template);
			//colleziono gli agenti compatibili con la ricerca
			AID [] agents = new AID[result.length];
			
		     for (int i = 0; i < result.length; ++i) {
              agents[i] = result[i].getName();
            }

			//creo il messaggio da inviare con il tipo di messaggio (proposal, inform, etc)
			//in base al parametro passato
            ACLMessage message = new ACLMessage(perf);
            
            //aggiungo i destinatari
	        for (int i = 0; i < agents.length; ++i) {
	          message.addReceiver(agents[i]);
	        } 
	       
	        //setto il contenuto del messaggio
	       	if(content!=null){
	       		message.setContentObject(content);
	       	}else{
	       		message.setContent("message");
	       	}

	       	//setto l'id del messaggio
	       	if(conversationId!=null && conversationId!=""){
	       		message.setConversationId(conversationId);//forse non serve
	       	}else{
	       		message.setConversationId("default");
	       	}
	       	
	       	//setto l'id dell'eventuale risposta
	       	if(reply!=null && reply!=""){
	       		message.setReplyWith(reply); // Unique value
	       	}else{
	       		message.setReplyWith("default");
	       	}
	       	
	       	//invio il messaggio
	        agent.send(message);
	        
		} catch (FIPAException | IOException e) {
			e.printStackTrace();
		} 

	}
	
	public void setEntity(String entity){
		this.entity=entity;
	}
	
}
