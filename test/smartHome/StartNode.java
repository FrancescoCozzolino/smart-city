package smartHome;

import cartago.CartagoException;
import cartago.CartagoService;

public class StartNode {

	private StartNode(){
		
	}
	
	public static synchronized void startCartagoNode(){
		if(!CartagoService.isNodeActive()){
			try {
				CartagoService.startNode();
			} catch (CartagoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
