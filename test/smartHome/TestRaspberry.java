package smartHome;

import static org.junit.Assert.assertTrue;

import org.firmata4j.Pin.Mode;
import org.junit.Before;
import org.junit.Test;

import cartago.CartagoException;
import cartago.CartagoService;
import smartHome.MyAgent.Operation;

public class TestRaspberry {
	private static MyAgent raspberry=null;
	private static boolean firstTime=true;
	
	@Before
	public void establishConnectionRaspberry() {
		
		if(firstTime){
			firstTime=false;
		
			try {
				//CartagoService.startNode();
				StartNode.startCartagoNode();
				while (!CartagoService.isNodeActive()) {
					System.out.println("Waiting ...");
					Thread.sleep(500);
				}
				raspberry=new MyAgent("raspberry");
				raspberry.setOperation(Operation.INITIALIZE);
				raspberry.run();
				
			} catch (CartagoException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Test
	public void pinModeRaspberry(){
		
		raspberry.setPin(21);
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.UNSUPPORTED);
		
		raspberry.setPin(21);
		raspberry.setMode(Mode.INPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.INPUT);
		
		raspberry.setPin(22);
		raspberry.setMode(Mode.OUTPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.OUTPUT);
		
		raspberry.setPin(23);
		raspberry.setMode(Mode.PWM);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.PWM);
		
		raspberry.setPin(2);
		raspberry.setMode(Mode.SERVO);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.SERVO);
		
		raspberry.setPin(50);
		raspberry.setMode(Mode.ANALOG);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.GET_PIN_MODE);
		raspberry.run();
		assertTrue(raspberry.getMode()==Mode.ANALOG);
	}
	
	@Test
	public void readWriteRaspberry(){
		
		raspberry.setPin(50);
		raspberry.setMode(Mode.ANALOG);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()>=0 && raspberry.getValue()<=1023);
		
		raspberry.setPin(24);
		raspberry.setMode(Mode.OUTPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);

		
		raspberry.setValue(1);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==1);
		
		raspberry.setValue(0);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
		
		raspberry.setPin(3);
		raspberry.setMode(Mode.SERVO);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setValue(120);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==120);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		raspberry.setValue(0);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
		
		
		raspberry.setPin(25);
		raspberry.setMode(Mode.PWM);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setValue(50);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==50);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		raspberry.setValue(0);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
		
		
		raspberry.setPin(26);
		raspberry.setMode(Mode.INPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
		
		raspberry.setPin(27);
		raspberry.setMode(Mode.OUTPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
	}
	
	
	@Test
	public void i2cRaspberry(){
		
		raspberry.setAddress((byte)0x18);
		raspberry.setRegister((byte)0x05);
		raspberry.setResponseLength((byte)0x02);
		raspberry.setOperation(Operation.ADDI2CDEVICE);
		raspberry.run();
		raspberry.setOperation(Operation.READI2C);
		raspberry.run();
		byte [] data= raspberry.getDataI2C();
		
		int t = (data[0] << 8) + (data[1]&0xFF);
		double temp = (t & 0x0FFF)/16.0;
		
		if ((t & 0x1000)==0x1000){
			temp -= 256;
		}
		
		assertTrue(temp>=18&&temp<=24);
	}
	
	
	@Test
	public void startStopTrackPinRaspberry(){
		
		raspberry.setPin(28);
		raspberry.setMode(Mode.OUTPUT);
		raspberry.setOperation(Operation.SET_PIN_MODE);
		raspberry.run();
		
		raspberry.setOperation(Operation.START_TRACK_PIN);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
		assertTrue(raspberry.pinIsTracked());
				
		raspberry.setValue(1);
		raspberry.setOperation(Operation.WRITE_TRACKED_PIN);
		raspberry.run();
		
		assertTrue(raspberry.getValue()==1);
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		raspberry.setOperation(Operation.STOP_TRACK_PIN);
		raspberry.run();
		assertTrue(!raspberry.pinIsTracked());
		
		raspberry.setValue(0);
		raspberry.setOperation(Operation.WRITE);
		raspberry.run();
		
		assertTrue(raspberry.getValue()==-1);
		
		raspberry.setOperation(Operation.READ);
		raspberry.run();
		assertTrue(raspberry.getValue()==0);
	}

}
