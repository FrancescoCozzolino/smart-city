package smartHome;

import static org.junit.Assert.assertTrue;

import org.firmata4j.Pin.Mode;
import org.junit.Before;
import org.junit.Test;

import cartago.CartagoException;
import cartago.CartagoService;
import smartHome.MyAgent.Operation;


public class TestArduino {
	
	private static MyAgent arduino=null;
	private static boolean firstTime=true;
	
	@Before
	public void establishConnectionArduino() {
		
		if(firstTime){
			firstTime=false;
		
			try {
				//CartagoService.startNode();
				StartNode.startCartagoNode();
				while (!CartagoService.isNodeActive()) {
					System.out.println("Waiting ...");
					Thread.sleep(500);
				}
				
				arduino=new MyAgent("arduino",/*"/dev/ttyACM0"*/"COM4");
				arduino.setOperation(Operation.INITIALIZE);
				arduino.run();
			} catch (CartagoException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Test
	public void pinModeArduino(){
		
		arduino.setPin(14);
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.ANALOG);
		
		arduino.setPin(12);
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.OUTPUT);
		
		arduino.setMode(Mode.INPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.INPUT);
		
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.OUTPUT);
		
		arduino.setPin(3);
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.OUTPUT);
		
		arduino.setMode(Mode.PWM);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.PWM);
		
		arduino.setPin(11);
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.OUTPUT);
		
		arduino.setMode(Mode.SERVO);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.GET_PIN_MODE);
		arduino.run();
		assertTrue(arduino.getMode()==Mode.SERVO);
	}
	
	@Test
	public void readWriteArduino(){
		
		arduino.setPin(14);
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()>=0 && arduino.getValue()<=1023);
		
		arduino.setPin(12);
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);

		
		arduino.setValue(1);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==1);
		
		arduino.setValue(0);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
		
		arduino.setPin(11);
		arduino.setMode(Mode.SERVO);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setValue(120);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==120);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		arduino.setValue(0);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		
		arduino.setPin(3);
		arduino.setMode(Mode.PWM);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setValue(50);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==50);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		arduino.setValue(0);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		
		arduino.setPin(12);
		arduino.setMode(Mode.INPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
		
		arduino.setPin(12);
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
	}
	/*
	@Test
	public void i2cArduino(){
		
		arduino.setAddress((byte)0x18);
		arduino.setRegister((byte)0x05);
		arduino.setResponseLength((byte)0x02);
		arduino.setOperation(Operation.ADDI2CDEVICE);
		arduino.run();
		arduino.setOperation(Operation.READI2C);
		arduino.run();
		byte [] data= arduino.getDataI2C();
		
		int t = (data[0] << 8) + (data[1]&0xFF);
		double temp = (t & 0x0FFF)/16.0;
		
		if ((t & 0x1000)==0x1000){
			temp -= 256;
		}
		assertTrue(temp>=18&&temp<=24);
	}*/
	
	@Test
	public void startStopTrackPinArduino(){
		
		arduino.setPin(13);
		arduino.setMode(Mode.OUTPUT);
		arduino.setOperation(Operation.SET_PIN_MODE);
		arduino.run();
		
		
		arduino.setOperation(Operation.START_TRACK_PIN);
		arduino.run();
		assertTrue(arduino.pinIsTracked());
		assertTrue(arduino.getValue()==0);
				
		arduino.setValue(1);
		arduino.setOperation(Operation.WRITE_TRACKED_PIN);
		arduino.run();
		
		assertTrue(arduino.getValue()==1);
		
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		arduino.setOperation(Operation.STOP_TRACK_PIN);
		arduino.run();
		assertTrue(!arduino.pinIsTracked());
		
		arduino.setValue(0);
		arduino.setOperation(Operation.WRITE);
		arduino.run();
		
		assertTrue(arduino.getValue()==-1);
		
		arduino.setOperation(Operation.READ);
		arduino.run();
		assertTrue(arduino.getValue()==0);
	}
	

}
