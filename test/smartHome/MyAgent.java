package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.ArtifactId;
import cartago.ArtifactObsProperty;
import cartago.CartagoException;
import cartago.IEventFilter;
import cartago.Op;
import cartago.events.ArtifactObsEvent;
import cartago.util.agent.Agent;
import cartago.util.agent.Percept;

public class MyAgent extends Agent{

	public static enum Operation{
		SET_PIN_MODE,
		GET_PIN_MODE,
		READ,
		WRITE,
		READI2C,
	    WRITEI2C,
		READI2CDIRECT,
		WRITEI2CDIRECT,
		ADDI2CDEVICE,
		START_TRACK_PIN,
		STOP_TRACK_PIN,
		INITIALIZE,
		WRITE_TRACKED_PIN,
	}
	
	private String port;
	private Operation operation=Operation.INITIALIZE;
	private ArtifactId device,operationArtifact;
	private int pin;
	private long value,valueDevice;
	private Mode mode,modeDevice;
	private IEventFilter filter;
	private byte address,register,responseLength;
	private byte [] data,dataRead;
	private boolean trackPin;
	
	public MyAgent(String agentName,String port) throws CartagoException{
		super(agentName);
		this.port=port;
		filter=new IEventFilter() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public boolean select(ArtifactObsEvent arg0) {	
				return true;
			}
		};
	}
	
	public MyAgent(String agentName) throws CartagoException{
		super(agentName);
		this.port="";
		filter=new IEventFilter() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public boolean select(ArtifactObsEvent arg0) {	
				return true;
			}
		};
	}
	
	public void setPin(int pin){
		this.pin=pin;
	}
	
	public int getPin(){
		return pin;
	}
	
	public Mode getMode(){
		return modeDevice;
	}
	
	public void setMode(Mode mode){
		this.mode=mode;
		modeDevice=null;
	}
	
	public long getValue(){
		return valueDevice;
	}
	
	public byte[] getDataI2C(){
		return dataRead;
	}
	
	public void setValue(int value){
		this.value=value;
		valueDevice=-1;
	}
	
	public void setOperation(Operation operation){
		this.operation=operation;
	}
	
	public void setAddress(byte address){
		this.address=address;
	}
	
	public void setRegister(byte register){
		this.register=register;
	}
	
	public void setResponseLength(byte responseLength){
		this.responseLength=responseLength;
	}
	
	public void setData(byte [] data){
		this.data=data;
	}
	
	
	public boolean pinIsTracked(){
		return trackPin;
	}
	
	@Override
	public void run() {
		try {
			
			switch(operation){
				case INITIALIZE:{
					if(port.equals("")){
						device = makeArtifact("raspberry","smartHome.Raspberry");
					}else{
						device = makeArtifact("arduino","smartHome.Arduino",new Object[]{port});
					}
					
					operationArtifact=makeArtifact("operation"+device.getName(),"smartHome.OperationArtifact",new Object[]{device});
					focus(operationArtifact);
				};break;
				case GET_PIN_MODE:{
					doAction(new Op("changePin", pin));
					doAction(new Op("getPinMode"));
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					modeDevice=(Mode)property[0].getValue();
				};break;
				
				case SET_PIN_MODE:{
					doAction(new Op("changePin", pin));
					doAction(new Op("changePinMode",mode));
					doAction(new Op("setPinMode"));
				};break;
				
				case READ:{
					doAction(new Op("changePin", pin));
					doAction(new Op("readValue"));
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					valueDevice=(long)property[0].getValue();
				};break;
				
				case WRITE:{
					doAction(new Op("changePin", pin));
					doAction(new Op("changeValueToWrite",value));
					doAction(new Op("writeValue"));
				};break;
				
				case ADDI2CDEVICE:{
					doAction(new Op("changeAddress",address));
					doAction(new Op("addI2CDevice"));
				};break;
				
				case WRITEI2C:{
					doAction(new Op("writeI2C",register,data));
				};break;
				
				case WRITEI2CDIRECT:{
					doAction(new Op("writeI2CDirect",data));
				};break;
				
				case READI2C:{
					doAction(new Op("readI2C",register,responseLength));
					
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					dataRead=(byte[])property[0].getValue();
				};break;
				
				case READI2CDIRECT:{
					doAction(new Op("readI2CDirect",responseLength));
					
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					dataRead=(byte[])property[0].getValue();
				};break;
				
				case START_TRACK_PIN:{
					doAction(new Op("changePin", pin));
					doAction(new Op("startTrackPin"));
					
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					for(int i=0;i<property.length;i++){
						if(property[i].getName().compareTo("trackPin")==0){
							trackPin=(boolean)property[i].getValue();
						}
					}
				};break;
				
				case STOP_TRACK_PIN:{
					doAction(new Op("changePin", pin));
					doAction(new Op("stopTrackPin"));
					
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					for(int i=0;i<property.length;i++){
						if(property[i].getName().compareTo("trackPin")==0){
							trackPin=(boolean)property[i].getValue();
						}
					}
				};break;
				
				case WRITE_TRACKED_PIN:{
					doAction(new Op("changePin", pin));
					doAction(new Op("changeValueToWrite",value));
					doAction(new Op("writeValue"));
					Percept p  = waitForPercept(filter);
					
					ArtifactObsProperty[] property=p.getPropChanged();
					valueDevice=(long)property[0].getValue();
				};break;
				
			}
			
		} catch (Exception ex){
			ex.printStackTrace();
		}
	}

}
