package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.*;

public class OperationArtifact extends Artifact implements ArtifactTransducer,ArtifactI2C{

	private final static String MODE="mode";
	private final static String VALUE="value";
	private final static String DATAI2C="dataI2C";
	private final static String TRACK_PIN="trackPin";
	private ArtifactId id;
	private int pin;
	private long value;
	private Mode mode;
	private byte address;
	
	void init(ArtifactId id){
		this.id=id;
		defineObsProperty(MODE,"");
		defineObsProperty(VALUE,"");
		defineObsProperty(DATAI2C,new byte[]{});
		defineObsProperty(TRACK_PIN,false);
		value=0;
	}

	@OPERATION
	void changePinMode(Mode mode){
		this.mode=mode;
	}
	
	@OPERATION
	void changePin(int pin){
		this.pin=pin;
	}
	
	@OPERATION
	void changeValueToWrite(long value){
		this.value=value;
	}
	
	@OPERATION
	void changeAddress(byte address){
		this.address=address;
	}
	
	@OPERATION
	public void getPinMode() {
		OpFeedbackParam<Mode> mode=new OpFeedbackParam<>();
		try {
			execLinkedOp(id,NameGpioOperation.GET_PIN_MODE,pin,mode);
			getObsProperty(MODE).updateValue(mode.get());
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void setPinMode() {
		try {
			execLinkedOp(id,NameGpioOperation.SET_PIN_MODE,pin,mode);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void readValue() {
		try {
			OpFeedbackParam<Long> value=new OpFeedbackParam<>();
			execLinkedOp(id,NameGpioOperation.READ,pin,value);
			getObsProperty(VALUE).updateValue(value.get());
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void writeValue() {
		try {
			execLinkedOp(id,NameGpioOperation.WRITE,pin,(int)value);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	public void startTrackPin() {
		try {
			execLinkedOp(id,NameGpioOperation.START_TRACK_PIN,pin,this);
			getObsProperty(TRACK_PIN).updateValue(true);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@OPERATION
	public void stopTrackPin() {
		try {
			execLinkedOp(id,NameGpioOperation.STOP_TRACK_PIN,pin,this);
			getObsProperty(TRACK_PIN).updateValue(false);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@LINK
	public void changeValue(long value) {
		getObsProperty(VALUE).updateValue(value);
	}

	@LINK
	@Override
	public ArtifactId getArtifactId() {
		return getId();
	}
	
	@LINK
	@Override
	public ArtifactId getArtifactIdI2C() {
		return getId();
	}

	@OPERATION
	@Override
	public void addI2CDevice() {
		try {
			execLinkedOp(id,NameGpioOperation.ADDI2CDEVICE,address);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	@Override
	public void writeI2C(byte register, byte... data) {
		try {
			execLinkedOp(id,NameGpioOperation.WRITEI2C,address,register,data);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	@Override
	public void writeI2CDirect(byte... data) {
		try {
			execLinkedOp(id,NameGpioOperation.WRITEI2C,address,data);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	@Override
	public void readI2C(byte register, byte responseLength) {
		try {
			execLinkedOp(id,NameGpioOperation.READI2C,this,address,register,responseLength);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@OPERATION
	@Override
	public void readI2CDirect(byte responseLength) {
		try {
			execLinkedOp(id,NameGpioOperation.READI2CDIRECT,this,address,responseLength);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}

	@LINK
	@Override
	public void changeValueI2C(byte[] value) {
		getObsProperty(DATAI2C).updateValue(value);
		
	}

	
}
