package utils;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTerm;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.StringTerm;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;

public class split extends DefaultInternalAction {
    
	private static final long serialVersionUID = 1L;

	/*
     * arg[0]=stringa da splittare
     * arg[1]=carattere per lo split
     * arg[2]=lista di destinazione
     * */
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	
    	String string=((StringTerm)args[0]).getString();
    	String regex=((StringTerm)args[1]).getString();
    	String[] array=string.split(regex);
    	ListTerm list=new ListTermImpl();
    	
    	for(String s:array){
    		list.add(new StringTermImpl(s));
    	}
    	
    	return un.unifies(args[2],list);
    }
    
    
  
}
