package utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import jason.asSemantics.*;
import jason.asSyntax.*;


public class dayHourMinuteOfWeek extends DefaultInternalAction {
    
	private static final long serialVersionUID = 1L;
	
	/*args[0] giorno
	 *args[1] ora
	 *args[2] minuti
	 * */
	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	String day;
    	Calendar calendar = Calendar.getInstance();
    	String [] time=new SimpleDateFormat("HH:mm").format(calendar.getTime()).split(":");
    	
    	switch(calendar.get(Calendar.DAY_OF_WEEK)){
			case Calendar.MONDAY:day="0";break;
			case Calendar.TUESDAY:day="1";break;
			case Calendar.WEDNESDAY:day="2";break;
			case Calendar.THURSDAY:day="3";break;
			case Calendar.FRIDAY:day="4";break;
			case Calendar.SATURDAY:day="5";break;
			case Calendar.SUNDAY:day="6";break;
			default:day="-1";break;
    	}
    	
    	un.unifies(args[0], new StringTermImpl(day));
    	un.unifies(args[1], new StringTermImpl(time[0]));
    	un.unifies(args[2], new StringTermImpl(time[1]));
    	
        return true;
    }
    
    
  
}
