package utils;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTerm;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;

public class replace extends DefaultInternalAction {
    
	private static final long serialVersionUID = 1L;

	/*
     * arg[0]=stringa da sostituire
     * arg[1]=carattere da sostituire
     * arg[2]=carattere
     * arg[3]=risultato
     * */
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	return un.unifies(args[3],new StringTermImpl(((StringTerm)args[0]).getString().replace(((StringTerm)args[1]).getString(), ((StringTerm)args[2]).getString())));
    }

}
