package utils;

import jason.asSemantics.*;
import jason.asSyntax.*;


public class stringToNumber extends DefaultInternalAction {
    
	private static final long serialVersionUID = 1L;
	
	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	
		Integer number;
		
		try{
			number=Integer.parseInt(((StringTerm)args[0]).getString());
		}catch(NumberFormatException e){
			e.printStackTrace();
			number=-1;
		}
		return un.unifies(args[1], new NumberTermImpl(number));
	}
}
