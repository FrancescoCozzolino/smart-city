// Internal action code for project smartHome

package jadedf;

import java.util.logging.Logger;

import jade.domain.DFService;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Term;
import jason.infra.jade.*;

public class deregister extends DefaultInternalAction {

	private static final long serialVersionUID = 1L;

	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	
    	Logger logger = Logger.getLogger("JadeDF.mas2j."+register.class.getName());
    	
    	try {
            if (ts.getUserAgArch().getArchInfraTier() instanceof JasonBridgeArch) {
                // get a reference to the jade agent that represents this Jason agent
                JadeAgArch infra = ((JasonBridgeArch)ts.getUserAgArch().getArchInfraTier()).getJadeAg();

                DFService.deregister(infra);
                return true;
            } else {
                logger.warning("jadefd.register can be used only with JADE infrastructure. Current arch is "+ts.getUserAgArch().getArchInfraTier().getClass().getName());
            }
        } catch (Exception e) {
            logger.warning("Error in internal action 'jadedf.register'! "+e);
        }
        return false;

    }
}
