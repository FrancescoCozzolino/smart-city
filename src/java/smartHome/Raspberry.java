package smartHome;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.firmata4j.Pin.Mode;

import cartago.*;

import com.pi4j.component.servo.ServoProvider;
import com.pi4j.component.servo.impl.RPIServoBlasterProvider;
import com.pi4j.gpio.extension.base.AdcGpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008GpioProvider;
import com.pi4j.gpio.extension.mcp.MCP3008Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;
import com.pi4j.io.i2c.I2CFactory.UnsupportedBusNumberException;
import com.pi4j.io.spi.SpiChannel;

public class Raspberry extends Artifact implements GpioOperation{

	private GpioController gpio;
	private HashMap<Integer,Pin> pinRasp;
	private HashMap<Integer,Mode> pinMode;
	private HashMap<Integer,GpioPinDigitalInput> digitalInputPin;
	private HashMap<Integer,GpioPinDigitalOutput> digitalOutputPin;
	private HashMap<Integer,GpioPinPwmOutput> pwmOutputPin;
	private HashMap<Integer,GpioPinServoImpl> servoPin;
	private GpioPinAnalogInput analogInputPin[];
	private RaspEventListener listener;
	private HashMap<Integer,HashSet<ArtifactTransducer>> observer;
	private AdcGpioProvider provider;
	private ServoProvider servoProvider;
	private I2CBus bus;
	private HashMap<Byte,I2CDevice> i2cDevice;
	
	void init(){
		gpio=GpioFactory.getInstance();
		pinRasp=RaspberryPin.getPins();
		pinMode=new HashMap<>();
		digitalInputPin=new HashMap<>();
		digitalOutputPin=new HashMap<>();
		pwmOutputPin=new HashMap<>();
		servoPin=new HashMap<>();
		observer=new HashMap<>();
		i2cDevice=new HashMap<>();
		listener=new RaspEventListener();
		bus=null;

		try {
			servoProvider= new RPIServoBlasterProvider();
			provider=new MCP3008GpioProvider(SpiChannel.CS0);
		} catch (IOException e) {
			e.printStackTrace();
		}

		//TOGLIERE I PIN 10-12-13-14 DALLA MAP E CONTROLLARE IN INPUT DI SETPINMODE 
		//PER VIETARNE L'UTILIZZO
		pinMode.put(10,Mode.ANALOG);
		pinMode.put(12,Mode.ANALOG);
		pinMode.put(13,Mode.ANALOG);
		pinMode.put(14,Mode.ANALOG);
		
		analogInputPin=new GpioPinAnalogInput[8];
		
		analogInputPin[0]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH0, "InputPin0");
		analogInputPin[1]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH1, "InputPin1");
		analogInputPin[2]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH2, "InputPin2");
		analogInputPin[3]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH3, "InputPin3");
		analogInputPin[4]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH4, "InputPin4");
		analogInputPin[5]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH5, "InputPin5");
		analogInputPin[6]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH6, "InputPin6");
		analogInputPin[7]=gpio.provisionAnalogInputPin(provider, MCP3008Pin.CH7, "InputPin7");
				
		//mi avvisa se il cambio tra una lettura e l'altra e >=100
		provider.setEventThreshold(0, analogInputPin); // all inputs; alternatively you can set thresholds on each input discretely

        //intervallo ogni quanto controlla i canali
        provider.setMonitorInterval(100); // milliseconds
        
		execInternalOp("notifyObservers");
	}
	
	//PERMETTERE DI CAMBIARE MODALITA PIN, ma vietare l uso di quelli riservati all analogico
	//e controllare se un pin viene impostato su uno di quelli riservati all'i2c(8 e 9) 
	//se si vuole settare una modalita su 8 e/o 9, non deve essere presente l'i2c
	@LINK
	@Override
	public void setPinMode(int pin, Mode mode) {
		if(pinRasp.containsKey(pin)){
			Pin gpioPin=pinRasp.get(pin);
			
			switch(mode){
				case INPUT:{
					if(gpioPin.getSupportedPinModes().contains(PinMode.DIGITAL_INPUT) && !pinMode.containsKey(pin)){	
						digitalInputPin.put(pin,gpio.provisionDigitalInputPin(gpioPin));
						pinMode.put(pin,mode);
					}
				}break;
				case OUTPUT:{
					if(gpioPin.getSupportedPinModes().contains(PinMode.DIGITAL_OUTPUT) && !pinMode.containsKey(pin)){
						digitalOutputPin.put(pin,gpio.provisionDigitalOutputPin(gpioPin));
						pinMode.put(pin,mode);
					}
				}break;
				case PWM:{
					
					if((gpioPin.getSupportedPinModes().contains(PinMode.SOFT_PWM_OUTPUT) || gpioPin.getSupportedPinModes().contains(PinMode.PWM_OUTPUT)) && !pinMode.containsKey(pin)){
						
						if(gpioPin.getSupportedPinModes().contains(PinMode.SOFT_PWM_OUTPUT)){
							pwmOutputPin.put(pin,gpio.provisionSoftPwmOutputPin(gpioPin));
						}else{
							pwmOutputPin.put(pin,gpio.provisionPwmOutputPin(gpioPin));	
						}
						
						pinMode.put(pin,mode);
					}
				}break;
				case SERVO:{
					if((pin>=0 && pin<=7) && !pinMode.containsKey(pin)){
						try {
							servoPin.put(pin,new GpioPinServoImpl(servoProvider.getServoDriver(gpioPin)));
						} catch (IOException e) {
							e.printStackTrace();
						}
						pinMode.put(pin,mode);
					}
				}break;
			default:System.out.println("Error");break;
			}
		}else if(pin>=50 && pin<58 && mode==Mode.ANALOG){
			pinMode.put(pin,mode);
		}
	}

	//SISTEMARE IL CASO ANALOG PER I PIN 10-12-13-14
	//SISTEMARE IL CASO I2C PER I PIN 8 E 9
	@LINK
	@Override
	public void getPinMode(int pin, OpFeedbackParam<Mode> mode) {
		if(pinMode.containsKey(pin)){
			mode.set(pinMode.get(pin));
		}else{
			mode.set(Mode.UNSUPPORTED);
		}
	}

	//SISTEMARE IL CASO ANALOG PER I PIN 10-12-13-14
	//SISTEMARE IL CASO I2C PER I PIN 8-9
	@LINK
	@Override
	public void read(int pin, OpFeedbackParam<Long> value) {
		if(pinMode.containsKey(pin)){
			switch(pinMode.get(pin)){
				case ANALOG: if(pin>=50&&pin<58)value.set((long)analogInputPin[pin%50].getValue());break;
				case INPUT: value.set((long) digitalInputPin.get(pin).getState().getValue());break;
				case OUTPUT: value.set((long) digitalOutputPin.get(pin).getState().getValue());break;
				case PWM: value.set((long)pwmOutputPin.get(pin).getPwm());break;
				case SERVO: value.set((long) servoPin.get(pin).getAngle());break;
				default: value.set(null);break;
			}
		}else{
			value.set(null);
		}
	}

	//SISTEMARE IL CASO ANALOG PER I PIN 10-12-13-14
	//SISTEMARE IL CASO I2C PER I PIN 8-9
	@LINK
	@Override
	public void write(int pin, int value) {
		if(pinMode.containsKey(pin)){
			switch(pinMode.get(pin)){
				case OUTPUT:digitalOutputPin.get(pin).setState(value==1?PinState.HIGH:PinState.LOW);break;
				case PWM: {
						if(pwmOutputPin.get(pin).getPin().getSupportedPinModes().contains(PinMode.PWM_OUTPUT)){
							value=(int) ((value/100.0)*1024);
						}
						pwmOutputPin.get(pin).setPwm(value);
					}break;
				case SERVO: servoPin.get(pin).setAngle(value);break;
				default: System.out.println("Error");break;
			}
		}
	}

	//SISTEMARE IL CASO ANALOG PER I PIN 10-12-13-14
	//SISTEMARE IL CASO I2C PER I PIN 8-9
	@LINK
	@Override
	public void startTrackPin(int pin, ArtifactTransducer transducer) {
		if(!observer.containsKey(pin)){
			if(pinMode.containsKey(pin)){
				HashSet<ArtifactTransducer> artifact=new HashSet<>();
				artifact.add(transducer);
				observer.put(pin,artifact);
					
				switch(pinMode.get(pin)){
					case ANALOG: if(pin>=50&&pin<58)analogInputPin[pin%50].addListener(listener);break;
					case INPUT: digitalInputPin.get(pin).addListener(listener);break;
					case OUTPUT: digitalOutputPin.get(pin).addListener(listener);break;
					case PWM: pwmOutputPin.get(pin).addListener(listener);break;
					case SERVO: servoPin.get(pin).addListener(listener);break;
					default: System.out.println("Error");break;
				}
			}
		}else{
			observer.get(pin).add(transducer);
		}
	}

	//SISTEMARE IL CASO ANALOG PER I PIN 10-12-13-14
	//SISTEMARE IL CASO I2C PER I PIN 8-9
	@LINK
	@Override
	public void stopTrackPin(int pin, ArtifactTransducer transducer) {
		if(observer.containsKey(pin)){
			HashSet<ArtifactTransducer> list=observer.get(pin);
			list.remove(transducer);
			if(list.size()==0){
				observer.remove(pin);
					
				switch(pinMode.get(pin)){
					case ANALOG: if(pin>=50&&pin<58)analogInputPin[pin%50].removeListener(listener);break;
					case INPUT: digitalInputPin.get(pin).removeListener(listener);break;
					case OUTPUT: digitalOutputPin.get(pin).removeListener(listener);break;
					case PWM: pwmOutputPin.get(pin).removeListener(listener);break;
					case SERVO: servoPin.get(pin).removeListener(listener);break;
					default: System.out.println("Error");break;
				}
			}
		}
	}

	//CONTROLLARE CHE I PIN 8-9 SIANO DISPONIBILI. SE UNO DEI DUE NON E LIBERO
	//LANCIARE ERRORE. SE ENTRAMBI SONO LIBERI, IMPOSTARE LA MODALITA I2C
	@LINK
	@Override
	public void addI2CDevice(byte address) {
		if((!pinMode.containsKey(8)&&!pinMode.containsKey(9))||pinMode.get(8)==Mode.I2C){
			if(bus==null){
				try {
					bus = I2CFactory.getInstance(I2CBus.BUS_1);
					pinMode.put(8,Mode.I2C);
					pinMode.put(9,Mode.I2C);
				} catch (UnsupportedBusNumberException | IOException e) {
					e.printStackTrace();
				}
			}
			
			if(!i2cDevice.containsKey(address)){
				try {
					i2cDevice.put(address,bus.getDevice(address));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else{
			System.out.println("Non e' possibile utilizzare l'i2c. Pin gia' utilizzati");
		}
	}

	@LINK
	@Override
	public void writeI2C(byte address, byte register, byte... data) {
		if(i2cDevice.containsKey(address)){
			try {
				i2cDevice.get(address).write(register,data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@LINK
	@Override
	public void writeI2CDirect(byte address, byte... data) {
		if(i2cDevice.containsKey(address)){
			try {
				i2cDevice.get(address).write(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@LINK
	@Override
	public void readI2C(ArtifactI2C transducer, byte address, byte register, byte responseLength) {
		if(i2cDevice.containsKey(address)){
			try {
				byte[] value=new byte[responseLength];
				i2cDevice.get(address).read(register,value,0,responseLength);
				execLinkedOp(transducer.getArtifactIdI2C(),ArtifactI2C.METHOD,value);
			} catch (IOException | OperationException e) {
				e.printStackTrace();
			}
		}
	}

	@LINK
	@Override
	public void readI2CDirect(ArtifactI2C transducer, byte address, byte responseLength) {
		if(i2cDevice.containsKey(address)){
			try {
				byte[] value=new byte[responseLength];
				i2cDevice.get(address).read(value, 0,responseLength);
				execLinkedOp(transducer.getArtifactIdI2C(),ArtifactI2C.METHOD,value);
			} catch (IOException | OperationException e) {
				e.printStackTrace();
			}
		}
	}
	
	//CONTROLLARE PWM
	@INTERNAL_OPERATION
	@Override
	public void notifyObservers() {
		while(true){
			await(listener);
			GpioPinEvent event=listener.getEvent();
			ArrayList<ArtifactTransducer> artifacts=null;
			int pin=event.getPin().getPin().getAddress();

			if(event instanceof GpioPinAnalogValueChangeEvent){
				pin+=50;
			}

			if(observer.containsKey(pin)){
				artifacts = new ArrayList<>(observer.get(pin));
			}
						
			if(artifacts!=null){
				long value=0;
				
				if(event instanceof GpioPinDigitalStateChangeEvent){
					value=((GpioPinDigitalStateChangeEvent)event).getState().getValue();
				}else if(event instanceof GpioPinAnalogValueChangeEvent){
					value=(long)((GpioPinAnalogValueChangeEvent)event).getValue();
				}else if(event instanceof GpioPinServoAngleChangeEvent){
					value=(long)((GpioPinServoAngleChangeEvent)event).getAngle();
				}
				
				for(ArtifactTransducer tmp:artifacts){
					try {
						execLinkedOp(tmp.getArtifactId(),ArtifactTransducer.METHOD,value);
					} catch (OperationException e) {
						e.printStackTrace();
					}
				}
			}	
		}
	}
}
