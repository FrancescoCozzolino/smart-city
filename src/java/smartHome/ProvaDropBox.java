package smartHome;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import com.dropbox.core.DbxApiException;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.NetworkIOException;
import com.dropbox.core.RetryException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.UploadSessionFinishErrorException;
import com.dropbox.core.v2.files.UploadSessionLookupErrorException;
import com.dropbox.core.v2.files.WriteMode;

public class ProvaDropBox {

	private static final long CHUNKED_UPLOAD_CHUNK_SIZE = 8L << 20; // 8MiB
    private static final int CHUNKED_UPLOAD_MAX_ATTEMPTS = 5;
	
	public static void main(String[] args) throws DbxApiException, DbxException, FileNotFoundException, IOException {
		
		
		
		try {
			File file=new File("JADE-all-4.4.0.zip");
			DbxClientV2 client = new DbxClientV2(new DbxRequestConfig("smartCityHome"), "YCJ57IH015AAAAAAAAAFwrOqSs39QV4PEV_dJbR3Vn7WWTcoQ0oGel0MFq5gg9R5");
			
			if(file.length() <= (2 * CHUNKED_UPLOAD_CHUNK_SIZE)){
				client.files().uploadBuilder("/video/"+file.getName()).uploadAndFinish(new FileInputStream(file));
			}else{
				long size = file.length();

		        // assert our file is at least the chunk upload size. We make this assumption in the code
		        // below to simplify the logic.
		        /*if (size < CHUNKED_UPLOAD_CHUNK_SIZE) {
		            System.err.println("File too small, use upload() instead.");
		            System.exit(1);
		            return;
		        }*/

		        long uploaded = 0L;
		        DbxException thrown = null;

		        // Chunked uploads have 3 phases, each of which can accept uploaded bytes:
		        //
		        //    (1)  Start: initiate the upload and get an upload session ID
		        //    (2) Append: upload chunks of the file to append to our session
		        //    (3) Finish: commit the upload and close the session
		        //
		        // We track how many bytes we uploaded to determine which phase we should be in.
		        String sessionId = null;
		        for (int i = 0; i < CHUNKED_UPLOAD_MAX_ATTEMPTS; ++i) {
		            if (i > 0) {
		                System.out.printf("Retrying chunked upload (%d / %d attempts)\n", i + 1, CHUNKED_UPLOAD_MAX_ATTEMPTS);
		            }

		            try (InputStream in = new FileInputStream(file)) {
		                // if this is a retry, make sure seek to the correct offset
		                in.skip(uploaded);

		                // (1) Start
		                if (sessionId == null) {
		                    sessionId = client.files().uploadSessionStart()
		                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE)
		                        .getSessionId();
		                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
		                    printProgress(uploaded, size);
		                }

		                UploadSessionCursor cursor = new UploadSessionCursor(sessionId, uploaded);

		                // (2) Append
		                while ((size - uploaded) > CHUNKED_UPLOAD_CHUNK_SIZE) {
		                    client.files().uploadSessionAppendV2(cursor)
		                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE);
		                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
		                    printProgress(uploaded, size);
		                    cursor = new UploadSessionCursor(sessionId, uploaded);
		                }

		                // (3) Finish
		                long remaining = size - uploaded;
		                CommitInfo commitInfo = CommitInfo.newBuilder("/video/"+file.getName())
		                    .withMode(WriteMode.ADD)
		                    .withClientModified(new Date(file.lastModified()))
		                    .build();
		                FileMetadata metadata = client.files().uploadSessionFinish(cursor, commitInfo)
		                    .uploadAndFinish(in, remaining);

		                System.out.println(metadata.toStringMultiline());
		                return;
		            } catch (RetryException ex) {
		                thrown = ex;
		                // RetryExceptions are never automatically retried by the client for uploads. Must
		                // catch this exception even if DbxRequestConfig.getMaxRetries() > 0.
		                sleepQuietly(ex.getBackoffMillis());
		                continue;
		            } catch (NetworkIOException ex) {
		                thrown = ex;
		                // network issue with Dropbox (maybe a timeout?) try again
		                continue;
		            } catch (UploadSessionLookupErrorException ex) {
		                if (ex.errorValue.isIncorrectOffset()) {
		                    thrown = ex;
		                    // server offset into the stream doesn't match our offset (uploaded). Seek to
		                    // the expected offset according to the server and try again.
		                    uploaded = ex.errorValue
		                        .getIncorrectOffsetValue()
		                        .getCorrectOffset();
		                    continue;
		                } else {
		                    // Some other error occurred, give up.
		                    System.err.println("Error uploading to Dropbox: " + ex.getMessage());
		                    System.exit(1);
		                    return;
		                }
		            } catch (UploadSessionFinishErrorException ex) {
		                if (ex.errorValue.isLookupFailed() && ex.errorValue.getLookupFailedValue().isIncorrectOffset()) {
		                    thrown = ex;
		                    // server offset into the stream doesn't match our offset (uploaded). Seek to
		                    // the expected offset according to the server and try again.
		                    uploaded = ex.errorValue
		                        .getLookupFailedValue()
		                        .getIncorrectOffsetValue()
		                        .getCorrectOffset();
		                    continue;
		                } else {
		                    // some other error occurred, give up.
		                    System.err.println("Error uploading to Dropbox: " + ex.getMessage());
		                    System.exit(1);
		                    return;
		                }
		            } catch (DbxException ex) {
		                System.err.println("Error uploading to Dropbox: " + ex.getMessage());
		                System.exit(1);
		                return;
		            } catch (IOException ex) {
		                System.err.println("Error reading from file \"" + file + "\": " + ex.getMessage());
		                System.exit(1);
		                return;
		            }
		        }

			}
		} catch (DbxException | IOException e) {
			e.printStackTrace();
		}
	
	}
	
	private static void printProgress(long uploaded, long size) {
        System.out.printf("Uploaded %12d / %12d bytes (%5.2f%%)\n", uploaded, size, 100 * (uploaded / (double) size));
    }
	
	private static void sleepQuietly(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            // just exit
            System.err.println("Error uploading to Dropbox: interrupted during backoff.");
            System.exit(1);
        }
    }
}
