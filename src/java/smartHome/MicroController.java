package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.OpFeedbackParam;

public interface MicroController {

	void setPinMode(int pin,Mode mode);
	void getPinMode(int pin, OpFeedbackParam<Mode> mode);
	void read(int pin, OpFeedbackParam<Long> value);
	void write(int pin, int value);
	void startTrackPin(int pin,ArtifactTransducer transducer);
	void stopTrackPin(int pin,ArtifactTransducer transducer);
	void addI2CDevice(byte address);
	void writeI2C(byte address,byte register,byte... data);
	void writeI2CDirect(byte address,byte... data);
	void readI2C(ArtifactI2C transducer,byte address,byte register,byte responseLength);
	void readI2CDirect(ArtifactI2C transducer,byte address,byte responseLength);
	void notifyObservers();
}
