// CArtAgO artifact code for project smartHome

package smartHome;

import cartago.*;

/**
 * Artifact that model a light. This artifact extend Actuator's Artifact
 * 
 * @author Francesco Cozzolino
 * @see Actuator
 */
public class Light extends Actuator {
	
	private final static String PROPERTY="value";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean pwmMode;
	
	/**
	 * Initialize the light 
	 * 
	 * @param idDevice Artifact's identifier that control the device which light is attached.
	 * @param pin Pin of the device which the light is attached.
	 * @param pwmMode Set if the light work in pwm mode (true) or digital mode (false).
	 */
	void init(ArtifactId idDevice, int pin, boolean pwmMode) {
		super.init(idDevice, pin, pwmMode);
		this.pwmMode=pwmMode;
		defineObsProperty(PROPERTY, 0);
		
		if(pwmMode){
			defineObsProperty(POWER_CONSUMPTION, 0);
		}else{
			defineObsProperty(POWER_CONSUMPTION,100);
		}
	}
	
	/**
	 * Set the brightness of light
	 * 
	 * @param value Brightness of light. 0 for turn off the light. 
	 * If light work in digital mode, set value to 1 for turn on the light, otherwise
	 * set value between 1 and 100. 
	 */
	@OPERATION
	void setBrightness(int value){
		setValue(value);
	}
	
	@LINK
	public void changeValue(long value){
		getObsProperty(PROPERTY).updateValue(value);
		
		if(value!=0 && pwmMode){
			getObsProperty(POWER_CONSUMPTION).updateValue(value);
		}
		
	}
}

