package smartHome;


public interface ArtifactActuator{

	/**
	 * Set the value for the actuator.
	 * 
	 * @param value If actuator works in digital mode, the value must be 0 or 1.
	 * If actuator works in pwm mode, the value must be between 0 and 100. In case of
	 * pwm mode, the value indicates the percentage of duty cycle.
	 */
	void setValue(int value);
}
