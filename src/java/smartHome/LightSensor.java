package smartHome;

import cartago.*;

/**
 * Artifact that model the sensor light ga1a12s202.
 * This Artifact extend Sensor's Artifact
 * 
 * @author Francesco Cozzolino
 * @see Sensor
 */
public class LightSensor extends Sensor {

	private final static String PROPERTY="lux";
	private final static String STATE="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which light sensor is attached.
	 * @param pin Pin of the device which the light sensor is attached.
	 */
	void init(ArtifactId idDevice,int pin){
		super.init(idDevice,pin,true);
		defineObsProperty(STATE,0);
		defineObsProperty(POWER_CONSUMPTION,50);
	}
	
	@OPERATION
	@Override
	public void startTrackPin() {
		System.out.println("start");
		getObsProperty(STATE).updateValue(1);
		super.startTrackPin();
	}
	
	@OPERATION
	@Override
	public void stopTrackPin() {
		System.out.println("stop");
		getObsProperty(STATE).updateValue(0);
		super.stopTrackPin();
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		if((int)getObsProperty(STATE).getValue()!=0){
			signal(PROPERTY,calculateLux(value));
		}
	}
	
	private float calculateLux(long value){
		return (float) Math.pow(10,(value*5.0/1024));
	}

}
