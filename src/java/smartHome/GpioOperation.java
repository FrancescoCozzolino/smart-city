package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.OpFeedbackParam;

/**
 * This interface describe the possible operation on a device with general purpose I/O pins.
 * @author Francesco Cozzolino
 *
 */
public interface GpioOperation {

	/**
	 * Set working mode of pin
	 * @param pin Number of pin
	 * @param mode Working mode of pin
	 */
	void setPinMode(int pin,Mode mode);
	
	/**
	 * Get the working mode of pin
	 * @param pin Number of pin
	 * @param mode Variable to store the working mode of pin
	 */
	void getPinMode(int pin, OpFeedbackParam<Mode> mode);
	
	/**
	 * Read the value from a pin
	 * @param pin Number of pin
	 * @param value Variable to store the value read from the pin
	 */
	void read(int pin, OpFeedbackParam<Long> value);
	
	/**
	 * Write a value on a pin
	 * @param pin Number of pin
	 * @param value Value to write on a pin
	 */
	void write(int pin, int value);
	
	/**
	 * Track every change of a pin. At every single change, a linked
	 * operation of the caller is invoked
	 * @param pin Number of pin to track
	 * @param transducer Caller of operation
	 */
	void startTrackPin(int pin,ArtifactTransducer transducer);
	
	/**
	 * Stop track the changes of a pin
	 * 
	 * @param pin Number of pin
	 * @param transducer Caller of operation
	 */
	void stopTrackPin(int pin,ArtifactTransducer transducer);
	
	/**
	 * Add I2C device attached to the device
	 * 
	 * @param address Address of I2C device
	 */
	void addI2CDevice(byte address);
	
	/**
	 * 
	 * Sends some data to an I2C device on a specified register
	 * 
	 * @param address Address of I2C device
	 * @param register Register of I2C device
	 * @param data Data to send
	 */
	void writeI2C(byte address,byte register,byte... data);
	
	/**
	 * Sends some data to an I2C device
	 * 
	 * @param address Address of I2C device
	 * @param data Data to send
	 */
	void writeI2CDirect(byte address,byte... data);
	
	/**
	 * Read data from a registry of an I2C device
	 * 
	 * @param transducer Caller of operation
	 * @param address Address of I2C device
	 * @param register Register of I2C device
	 * @param responseLength length of data to read
	 */
	void readI2C(ArtifactI2C transducer,byte address,byte register,byte responseLength);
	
	/**
	 * Read data from an I2C device 
	 * 
	 * @param transducer Caller of operation
	 * @param address Address of I2C device
	 * @param responseLength length of data to read
	 */
	void readI2CDirect(ArtifactI2C transducer,byte address,byte responseLength);
	
	void notifyObservers();
}
