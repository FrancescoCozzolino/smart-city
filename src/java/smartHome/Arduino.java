// CArtAgO artifact code for project smartHome

package smartHome;
 

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.firmata4j.I2CDevice;
import org.firmata4j.I2CEvent;
import org.firmata4j.IODevice;
import org.firmata4j.IOEvent;
import org.firmata4j.firmata.FirmataDevice;
import org.firmata4j.Pin.Mode;

import cartago.*;

/**
 * Artifact that permit to control an Arduino board
 * 
 * @author Francesco Cozzolino
 */
public class Arduino extends Artifact implements GpioOperation{
	
	private IODevice arduino;
	private HashMap<Integer,HashSet<ArtifactTransducer>> observer;
	private HashMap<Byte,I2CDevice> i2cDevice;
	private HashMap<Byte,ArrayList<ArtifactI2C>> observerI2C;
	private EventListener listener;
	
	/**
	 * Establish a connection with an Arduino board on specified port.
	 * 
	 * @param port The port for establish a connection with Arduino board (e.g. "COM4")
	 */
	void init(String port) {
		arduino=new FirmataDevice(port);
		observer=new HashMap<>();
		i2cDevice=new HashMap<>();
		observerI2C=new HashMap<>();
		listener=new EventListener();
		
		try {
			arduino.start();
			arduino.ensureInitializationIsDone();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		execInternalOp("notifyObservers");
	}
	
	@LINK
	public void setPinMode(int pin,Mode mode){
		if(arduino!=null && pin>=2 && pin<=arduino.getPinsCount()){
			try{
				switch(mode){
					case ANALOG:arduino.getPin(pin).setMode(Mode.ANALOG);break;
					case INPUT:arduino.getPin(pin).setMode(Mode.INPUT);break;
					case OUTPUT:arduino.getPin(pin).setMode(Mode.OUTPUT);break;
					case PWM:arduino.getPin(pin).setMode(Mode.PWM);break;
					case SERVO:arduino.getPin(pin).setMode(Mode.SERVO);break;
					default:System.out.println("Unsupported mode: "+mode);break;
				}
			}catch(IllegalArgumentException | IOException e){
				e.printStackTrace();
			}
		}
	}
	
	@LINK
	public void getPinMode(int pin, OpFeedbackParam<Mode> mode){
		if(arduino!=null){
				mode.set(arduino.getPin(pin).getMode());
		}
	}
	
	@LINK
	public void read(int pin, OpFeedbackParam<Long> value){
		if(arduino!=null && pin>=2 && pin<arduino.getPinsCount()){
			if(arduino.getPin(pin).getMode()!=Mode.PWM){
				value.set(arduino.getPin(pin).getValue());
			}else{
				double val=(arduino.getPin(pin).getValue()/255.0)*100;
				value.set(Math.round(val));
			}
		}
	}
	
	@LINK
	public void write(int pin, int value){
		if(arduino!=null && pin>=2 && pin<arduino.getPinsCount()){
			try{
				int tmp=value;
				if(arduino.getPin(pin).getMode()==Mode.PWM){
					tmp=(int) ((value/100.0)*255);
				}
				arduino.getPin(pin).setValue(tmp);
			}catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@LINK
	public void startTrackPin(int pin,ArtifactTransducer transducer){

		if(arduino!=null && pin>=2 && pin<arduino.getPinsCount()){
			if(!observer.containsKey(pin)){
				HashSet<ArtifactTransducer> artifact=new HashSet<>();
				artifact.add(transducer);
				observer.put(pin,artifact);
				arduino.getPin(pin).addEventListener(listener);
			}else{
				observer.get(pin).add(transducer);
			}
			
		}
	}
	
	@LINK
	public void stopTrackPin(int pin,ArtifactTransducer transducer){
		if(arduino!=null && pin>=2 && pin<arduino.getPinsCount()){
			if(observer.containsKey(pin)){
				HashSet<ArtifactTransducer> list=observer.get(pin);
				list.remove(transducer);
				if(list.size()==0){
					observer.remove(pin);
					arduino.getPin(pin).removeEventListener(listener);
				}
			}
			
		}
	}
	
	
	@LINK
	public void addI2CDevice(byte address){
		if(arduino!=null){
			if(!i2cDevice.containsKey(address)){
				try {
					I2CDevice device=arduino.getI2CDevice(address);
					i2cDevice.put(address,device);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
		
	@LINK
	public void writeI2C(byte address,byte register,byte... data){
		if(arduino!=null){
			if(i2cDevice.containsKey(address)){
				try {
					i2cDevice.get(address).tell(register);
					i2cDevice.get(address).tell(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@LINK
	public void writeI2CDirect(byte address,byte... data){
		if(arduino!=null){
			if(i2cDevice.containsKey(address)){
				try {
					i2cDevice.get(address).tell(data);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@LINK
	public void readI2C(ArtifactI2C transducer,byte address,byte register,byte responseLength){
		if(arduino!=null){
			if(i2cDevice.containsKey(address)){
				
				if(!observerI2C.containsKey(address)){
					ArrayList<ArtifactI2C> artifacts=new ArrayList<>();
					artifacts.add(transducer);
					observerI2C.put(address, artifacts);
				}else{
					observerI2C.get(address).add(transducer);
				}
				
				
				try {
					i2cDevice.get(address).tell(register);
					i2cDevice.get(address).ask(responseLength,listener);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@LINK
	public void readI2CDirect(ArtifactI2C transducer,byte address,byte responseLength){
		if(arduino!=null){
			if(i2cDevice.containsKey(address)){
				
				if(!observerI2C.containsKey(address)){
					ArrayList<ArtifactI2C> artifacts=new ArrayList<>();
					artifacts.add(transducer);
					observerI2C.put(address, artifacts);
				}else{
					observerI2C.get(address).add(transducer);
				}
				
				try {
					i2cDevice.get(address).ask(responseLength,listener);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@INTERNAL_OPERATION
	public void notifyObservers(){
		while(true){
			await(listener);
			Object event=listener.getEvent();
			
			if(event instanceof IOEvent){
				ArrayList<ArtifactTransducer> artifacts=null;
				IOEvent ioEvent=(IOEvent)event;
				int pin=ioEvent.getPin().getIndex();
			
				if(observer.containsKey(pin)){
					artifacts = new ArrayList<>(observer.get(pin));
				}
			
				if(artifacts!=null){
					for(ArtifactTransducer tmp:artifacts){
						try {
							if(ioEvent.getPin().getMode()!=Mode.PWM){
								execLinkedOp(tmp.getArtifactId(),ArtifactTransducer.METHOD,ioEvent.getValue());
							}else{
								double val=(ioEvent.getValue()/255.0)*100;
								execLinkedOp(tmp.getArtifactId(),ArtifactTransducer.METHOD,Math.round(val));
							}
						} catch (OperationException e) {
							e.printStackTrace();
						}
					}
				}
			
			}else if(event instanceof I2CEvent){
				I2CEvent i2cEvent=(I2CEvent)event;
				ArtifactI2C transducer=observerI2C.get(i2cEvent.getDevice().getAddress()).remove(0);
				try {
					execLinkedOp(transducer.getArtifactIdI2C(),ArtifactI2C.METHOD,i2cEvent.getData());
				} catch (OperationException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
}

