package smartHome;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.firmata4j.I2CEvent;
import org.firmata4j.I2CListener;
import org.firmata4j.IOEvent;
import org.firmata4j.PinEventListener;

import cartago.IBlockingCmd;

/**
 * The listener class for receiving events from Arduino board.
 * @author Francesco Cozzolino
 *
 */
public class EventListener implements IBlockingCmd, PinEventListener, I2CListener{

	private BlockingQueue<Object> events;
	private Object event;
	
	public EventListener(){
		events=new ArrayBlockingQueue<>(100);
		event=null;
	}
	
	@Override
	public void onModeChange(IOEvent event) {
	}

	@Override
	public void onValueChange(IOEvent event) {
		try {
			events.put(event);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void exec() {
		try {
			event=events.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	public Object getEvent(){
		return event;
	}

	@Override
	public void onReceive(I2CEvent event) {
		try {
			events.put(event);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
