package smartHome;

import cartago.*;

public class Vibration extends Sensor{

	private final static String PROPERTY="notify";
	
	void init(ArtifactId idDevice, int pin) {
		super.init(idDevice, pin,false);
	}
	
	@OPERATION
	@Override
	public void startTrackPin(){
		defineObsProperty(PROPERTY,read().get()==1?true:false);
		super.startTrackPin();	
	}
	
	
	@LINK
	@Override
	public void changeValue(long value) {
		getObsProperty(PROPERTY).updateValue(value==1?true:false);
	}

}
