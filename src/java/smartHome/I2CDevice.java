package smartHome;

import cartago.*;

/**
 * Artifact that model an I2C device. This artifact implement the ArtifactI2C's interface.
 *  
 * @author Francesco Cozzolino
 * @see ArtifactI2C
 */
public abstract class I2CDevice extends Artifact implements ArtifactI2C{

	protected ArtifactId idDevice;
	protected byte address;
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which I2C device is attached
	 * @param address Address of I2C device
	 */
	void init(ArtifactId idDevice,byte address) {
		this.idDevice=idDevice;
		this.address=address;
	}
	
	/**
	 * Establish a connection between the device and I2C device
	 */
	@OPERATION
	public void addI2CDevice(){
		try {
			execLinkedOp(idDevice,NameGpioOperation.ADDI2CDEVICE,address);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	@INTERNAL_OPERATION
	public void writeI2C(byte register,byte...data){
		try {
			execLinkedOp(idDevice,NameGpioOperation.WRITEI2C,this,address,register,data);
		} catch (OperationException e) {
			e.printStackTrace();
		}	
	}
	
	@INTERNAL_OPERATION
	public void writeI2CDirect(byte...data){
		try {
			execLinkedOp(idDevice,NameGpioOperation.WRITEI2CDIRECT,this,address,data);
		} catch (OperationException e) {
			e.printStackTrace();
		}	
	}
	
	@INTERNAL_OPERATION
	public void readI2C(byte register,byte responseLength){
		try {
			execLinkedOp(idDevice,NameGpioOperation.READI2C,this,address,register,responseLength);
		} catch (OperationException e) {
			e.printStackTrace();
		}	
	}
	
	@INTERNAL_OPERATION
	public void readI2CDirect(byte responseLength){
		try {
			execLinkedOp(idDevice,NameGpioOperation.READI2CDIRECT,this,address,responseLength);
		} catch (OperationException e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Return the Id of artifact
	 */
	public ArtifactId getArtifactIdI2C(){
		return getId();
	}
	
	@LINK
	public abstract void changeValueI2C(byte[]value);
	
	
}
