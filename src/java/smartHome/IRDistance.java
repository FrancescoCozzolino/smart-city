package smartHome;

import cartago.*;

/**
 * Artifact that model the IR distance sensor Sharp GP2Y0A41SK0F.
 * This artifact extend Sensor's Artifact.
 * 
 * @author Francesco
 * @see Sensor
 */
public class IRDistance extends Sensor {

	private final static String PROPERTY="distance";
	private final static String POWER_CONSUMPTION="powerConsumption";
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which the IR distance sensor is attached
	 * @param pin Pin of the device which the IR distance sensor is attached
	 */
	void init(ArtifactId idDevice,int pin){
		super.init(idDevice, pin, true);
		defineObsProperty(POWER_CONSUMPTION, 50);
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		signal(PROPERTY,calculateDistance(value));
	}

	private int calculateDistance(long value){
		value-=11;
		if(value<=0){
			value=1;
		}
		return (int)(2076/value);
	}
}
