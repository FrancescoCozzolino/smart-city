package smartHome;

import cartago.*;

/**
 * Artifact that model a heater.
 * This Artifact extend Actuator's Artifact
 * 
 * @author Francesco Cozzolino
 * @see Actuator
 */
public class Heater extends Actuator {

	private final static String PROPERTY="on";
	private final static String TEMPERATURE="waterTemperature";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private final static double MIN=20.0;
	private final static double MAX=80.0;
	private int waterTemperature,value;
	private boolean isOn;
	
	/**
	 * Initialize the heater 
	 * 
	 * @param idDevice Artifact's identifier that control the device which heater is attached
	 * @param pin Pin of the device which the heater is attached
	 */
	void init(ArtifactId idDevice, int pin){
		super.init(idDevice, pin,true);
		defineObsProperty(PROPERTY, false);
		waterTemperature=50;
		defineObsProperty(TEMPERATURE,waterTemperature);
		defineObsProperty(POWER_CONSUMPTION,(int)(300+waterTemperature-MIN));
		value=50;
		isOn=false;
	}
	
	/**
	 * Set the temperature of water in the heater
	 * 
	 * @param waterTemperature Temperature of water in the heater. The value must be between 20 and 80. 
	 * If the value is over the maximum temperature or lower the minimum temperature, the water of heater
	 * is set to the minimum or maximum.
	 */
	@OPERATION
	void setWaterTemperature(int waterTemperature) {
		if(waterTemperature>=MIN && waterTemperature<=MAX){
			this.waterTemperature = waterTemperature;
		}else if(waterTemperature<MIN){
			this.waterTemperature = (int) MIN;
		}else{
			this.waterTemperature = (int) MAX;
		}
		
		getObsProperty(POWER_CONSUMPTION).updateValue((int)(300+waterTemperature-MIN));
		getObsProperty(TEMPERATURE).updateValue(this.waterTemperature);
		
		value=(int) ((((this.waterTemperature-MIN)/(MAX-MIN))*99)+1);
		
		if(isOn){
			setValue(value);
		}
	}
	
	/**
	 * Turn on the heater
	 * 
	 */
	@OPERATION
	void turnOn(){
		isOn=true;
		setValue(value);
	}
	
	
	/**
	 * Turn off the heater
	 * 
	 */
	@OPERATION
	void turnOff(){
		isOn=false;
		setValue(0);
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		System.out.println("Heater is on: "+(value!=0));
		
		if(!(boolean)getObsProperty(PROPERTY).getValue()){
			if(value!=0){
				getObsProperty(PROPERTY).updateValue(true);
			}
		}else if(value==0){
			getObsProperty(PROPERTY).updateValue(false);
		}
	}

}
