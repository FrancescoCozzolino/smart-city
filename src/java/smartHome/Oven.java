// CArtAgO artifact code for project smartHome

package smartHome;

import cartago.*;
/**
 * Artifact that model an oven.
 * This Artifact extend Actuator's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Actuator
 */
public class Oven extends Actuator {

	//PROVARE A TOGLIERE LA VARIABILE BOOLEAN E USARE LA PROPRIETA
	private static final String PROPERTY="on";
	private static final String PROPERTY2="temperature";
	private static final String END_TIME="endTime";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private final static double MIN=90.0,MAX=230.0;
	private long time;
	private int degrees;
	private boolean isOn=false,flagChangeTime=false;
	private FakeListener listener;
	private Wait wait;
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which oven is attached.
	 * @param pin Pin of the device which the oven is attached.
	 */
	void init(ArtifactId idDevice,int pin) {
		super.init(idDevice, pin, true);
		defineObsProperty(PROPERTY,false);
		defineObsProperty(PROPERTY2,0);
		defineObsProperty(END_TIME,System.currentTimeMillis());
		defineObsProperty(POWER_CONSUMPTION,400);
		listener=new FakeListener();
	}

	/**
	 * Turn on the oven to the desired degrees for the desired time
	 * @param degrees Celsius degrees for heater the oven. The value must be between 90 and 230. 
	 * If the value is over the maximum or lower the minimum,the degrees
	 * are set to the minimum or maximum.
	 * @param time Amount of time before the oven turn off. This value is expressed in milliseconds.
	 */
	@OPERATION
	void turnOn(int degrees,long time){
		if(!isOn && time>0){
			isOn=true;
			
			if(degrees<MIN){
				degrees=(int) MIN;
			}else if(degrees>MAX){
				degrees=(int) MAX;
			}
			
			this.degrees=degrees;
			this.time=time;
			setValue((int) ((((degrees-MIN)/(MAX-MIN))*99)+1));
		}
	}
	
	/**
	 * Change the degrees of oven
	 * @param degrees Celsius degrees for heater the oven. The value must be between 90 and 230. 
	 * If the value is over the maximum or lower the minimum,the degrees
	 * are set to the minimum or maximum.
	 */
	@OPERATION
	void changeDegrees(int degrees){
		if(isOn){
			System.out.println("Cambio la temperatura a: "+degrees);
			
			if(degrees<MIN){
				degrees=(int) MIN;
			}else if(degrees>MAX){
				degrees=(int) MAX;
			}
			
			this.degrees=degrees;
			
			setValue((int) ((((degrees-MIN)/(MAX-MIN))*99)+1));
		}
	}
	
	/**
	 * Change remaining time before the oven turn off.
	 * 
	 * @param time Amount of time to be add or subtract to the remaining time 
	 * before the oven turn off. If the value is greater than 0, add X milliseconds
	 * to the remaining time before the oven turn off. Otherwise, subtract X milliseconds
	 * to the remaining time.
	 * This value is expressed in milliseconds.
	 */
	@OPERATION
	void changeTime(long time){
		if(isOn){
			flagChangeTime=true;
			getObsProperty(END_TIME).updateValue(System.currentTimeMillis()+time);
			wait.interrupt();
			execInternalOp("waitTime", time);
		}
	}
	
	/**
	 * 
	 * Turn off the oven
	 *  
	 */
	@OPERATION
	void turnOff(){
		if(isOn){
			wait.interrupt();
		}
	}
	
	@INTERNAL_OPERATION
	void waitTime(long time){
		wait=new Wait(listener,time);
		wait.start();
		await(listener);
		
		if(isOn&&!flagChangeTime){
			System.out.println("Spengo il forno a: "+System.currentTimeMillis());
			isOn=false;
			setValue(0);
		}else if(flagChangeTime){
			flagChangeTime=false;
		}
	}
	
	@LINK
	public void changeValue(long value){
		if((boolean)getObsProperty(PROPERTY).getValue()){
			if(value==0){
				getObsProperty(PROPERTY).updateValue(false);
				getObsProperty(END_TIME).updateValue(System.currentTimeMillis());
				getObsProperty(PROPERTY2).updateValue(0);
			}else{
				getObsProperty(POWER_CONSUMPTION).updateValue((int)(400+(degrees-MIN)));
				getObsProperty(PROPERTY2).updateValue(degrees);	
			}
		}else if(value!=0){
			getObsProperty(POWER_CONSUMPTION).updateValue((int)(400+(degrees-MIN)));
			getObsProperty(PROPERTY2).updateValue(degrees);
			getObsProperty(PROPERTY).updateValue(true);	
			getObsProperty(END_TIME).updateValue(System.currentTimeMillis()+time);
			execInternalOp("waitTime", time);
		}
	}
}