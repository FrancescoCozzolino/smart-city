// CArtAgO artifact code for project smartHome

package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.*;
/**
 * Artifact that model an actuator.
 * The actuator must be digital or work in pwm mode.
 * This artifact extend Transducer's artifact.
 * 
 * @author Francesco Cozzolino
 * @see Transducer
 */
public abstract class Actuator extends Transducer implements ArtifactActuator {
	
	private boolean pwm;
	
	/**
	 * Initialize the actuator.
	 * 
	 * @param idDevice Artifact's id that control the device which the actuator is attached
	 * @param pin The pin of the device which the actuator is attached
	 * @param pwmMode Specify if the actuator works in digital or pwm mode. For I2C's device @see I2CDevice class
	 */
	void init(ArtifactId idDevice, int pin, boolean pwmMode) {
		super.init(idDevice,pin,pwmMode?Mode.PWM:Mode.OUTPUT);
		pwm=pwmMode;
	}
	
	/**
	 * Set the value for the actuator.
	 * 
	 * @param value If actuator works in digital mode, the value must be 0 or 1.
	 * If actuator works in pwm mode, the value must be between 0 and 100. In case of
	 * pwm mode, the value indicates the percentage of duty cycle.
	 */
	public void setValue(int value) {
		
		if((pwm && value>=0 && value<=100) || (!pwm && (value==0||value==1))){
			try {
				execLinkedOp(idDevice,NameGpioOperation.WRITE,pin, value);
			} catch (OperationException e) {
				e.printStackTrace();
			}	
		}
	}
	
	
}

