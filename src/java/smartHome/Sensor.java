package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.ArtifactId;
import cartago.OpFeedbackParam;
import cartago.OperationException;

/**
 * Artifact that model a sensor.
 * The sensor could be analog or digital.
 * This artifact extend Transducer's artifact.
 * 
 * @author Francesco Cozzolino
 * @see Transducer
 */
public abstract class Sensor extends Transducer implements ArtifactSensor{

	/**
	 * Initialize the artifact.
	 * 
	 * @param idDevice Artifact's id that control the device which the sensor is attached
	 * @param pin The pin of the device which the sensor is attached
	 * @param isAnalog Specify if the sensor works in analog or digital mode. For I2C's device @see I2CDevice class
	 */
	void init(ArtifactId idDevice, int pin, boolean isAnalog) {
		super.init(idDevice,pin,isAnalog?Mode.ANALOG:Mode.INPUT);
	}
	
	/**
	 * Read the value perceived from the sensor from the environment 
	 * @return The value perceived from the sensor from the environment
	 */
	public OpFeedbackParam<Long> read() {
		
		OpFeedbackParam<Long> result= new OpFeedbackParam<>();
		
		try {
			execLinkedOp(idDevice,NameGpioOperation.READ,pin, result);
		} catch (OperationException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
