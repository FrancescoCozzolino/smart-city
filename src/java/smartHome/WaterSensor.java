// CArtAgO artifact code for project smartHome

package smartHome;

import cartago.*;
/**
 * Artifact that model the a water sensor.
 * This Artifact extend Sensor's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Sensor
 */
public class WaterSensor extends Sensor {
	
	private final static String PROPERTY="empty";
	private final static String POWER_CONSUMPTION="powerConsumption";
	
	/**
	 * Initialize the artifact
	 * 
	  @param idDevice Artifact's identifier that control the device which water sensor is attached.
	 * @param pin Pin of the device which the water sensor is attached.
	 */
	void init(ArtifactId idDevice, int pin) {
		super.init(idDevice, pin, true);
		defineObsProperty(POWER_CONSUMPTION, 20);
		defineObsProperty(PROPERTY,true);
	}

	@LINK
	public void changeValue(long value){
		//0 contatto con acqua, 1 altrimenti
		boolean isEmpty;
		
		if(value>=200){
			isEmpty=true;
			
			if((boolean)getObsProperty(PROPERTY).getValue()!=isEmpty){
				getObsProperty(PROPERTY).updateValue(isEmpty);
			}
		}else if(value<=100){
			isEmpty=false;
			
			if((boolean)getObsProperty(PROPERTY).getValue()!=isEmpty){
				getObsProperty(PROPERTY).updateValue(isEmpty);
			}
		}
		
		
	}
}

