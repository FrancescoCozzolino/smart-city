package smartHome;

import cartago.ArtifactId;

/**
 * The ArtifactI2C interface defines the necessary methods for an artifact tha model an I2C device
 * 
 * @author Francesco Cozzolino
 *
 */
public interface ArtifactI2C {

	final static String METHOD="changeValueI2C";
	
	/**
	 * Return the Id of artifact
	 * @return Id of artifact
	 */
	ArtifactId getArtifactIdI2C();
	
	/**
	 *adds the i2c device in order to communicate with it
	 */
	void addI2CDevice();
	
	/**
	 * Sends data to the specified registry of I2C device
	 * @param register Register to send data
	 * @param data Data to send
	 */
	void writeI2C(byte register,byte...data);
	
	/**
	 * Sends data to the I2C device
	 * @param data Data to send
	 */
	void writeI2CDirect(byte...data);
	
	/**
	 * Read data from the specified registry of I2C device
	 * @param register Register to read data
	 * @param responseLength Length of response
	 */
	void readI2C(byte register,byte responseLength);
	
	/**
	 * Read data from I2C device
	 * @param responseLength Length of response
	 */
	void readI2CDirect(byte responseLength);
	
	/**
	 * Invoke when the I2C device responds to the device
	 * @param value Response of I2C device
	 */
	void changeValueI2C(byte[]value);
}
