// CArtAgO artifact code for project smartHome

package smartHome;

import java.util.HashMap;
import java.util.Set;

import cartago.*;

/**
 * Artifact that control the power consumption of a set of devices.
 * 
 * @author Francesco Cozzolino
 *
 */
public class ElectricMeter extends Artifact {
	
	private static enum Profile{LOW,BALANCE,HIGH};
	
	private final static String PROFILE="profile";
	private final static String CURRENT_CONSUMPTION="currentConsumption";
	private final static int LOW_THRESHOLD=1000;
	private final static int BALANCE_THRESHOLD=2000;
	private final static int HIGH_THRESHOLD=3000;
	private int treshold,currentConsumption;
	private HashMap<String,HashMap<String,Device>> priority1;
	private HashMap<String,HashMap<String,Device>> priority2;
	private HashMap<String,HashMap<String,Device>> priority3;
	private boolean execute;
	
	/**
	 * Initialize the artifact. After this call, the energy profile selected
	 * is BALANCE
	 */
	void init() {
		defineObsProperty(CURRENT_CONSUMPTION, 0);
		defineObsProperty(PROFILE, Profile.BALANCE.ordinal());
		treshold=BALANCE_THRESHOLD;
		currentConsumption=0;
		priority1=new HashMap<>();
		priority2=new HashMap<>();
		priority3=new HashMap<>();
		execute=false;
	}
	
	/**
	 * Set the energy profile and control if the current power consumption
	 * is over the threshold of selected profile. If the power consumption is 
	 * over the threshold, some devices, according to their priority, are turn off.
	 * There are three profile of energy: LOW with a threshold of 1000, BALANCE with a
	 * threshold of 2000 and HIGH with a threshold of 3000
	 * 
	 * @param profile Energy profile. The value must be 0 (LOW), 1 (BALANCE) or 2(HIGH)
	 */
	@OPERATION
	void setProfile(int profile){
		
		System.out.println("Setto profilo a: "+profile);
		switch(profile){
			case 0:{
				getObsProperty(PROFILE).updateValue(Profile.LOW.ordinal());
				treshold=LOW_THRESHOLD;
			};break;
			
			case 1:{
				getObsProperty(PROFILE).updateValue(Profile.BALANCE.ordinal());
				treshold=BALANCE_THRESHOLD;
			};break;
			
			case 2:{
				getObsProperty(PROFILE).updateValue(Profile.HIGH.ordinal());
				treshold=HIGH_THRESHOLD;
			};break;
			
			default:{
				getObsProperty(PROFILE).updateValue(Profile.BALANCE.ordinal());
				treshold=BALANCE_THRESHOLD;
			};break;
		}
		
		if(currentConsumption>treshold && !execute){
			execInternalOp("checkCurrentConsumption");
		}
		
	}
	
	/**
	 * Add a device
	 * 
	 * @param nameAgent Agent's name that control the device
	 * @param nameDevice Device's name
	 * @param powerConsumption Power consumption of the device
	 * @param priority Priority of the device. Must be between 1 (LOW) and 3 (HIGH). 
	 * If the total power consumption of the devices is over the threshold, the device will be
	 * turned off according to their priority. Devices with a low priority are turned off earlier 
	 * than those with higher priority
	 */
	@OPERATION
	void addDevice(String nameAgent,String nameDevice,int powerConsumption,int priority){
		Device device=new Device(powerConsumption);
		
		switch(priority){
			case 1: addDeviceToMap(priority1,nameAgent,nameDevice,device);break;
			case 2: addDeviceToMap(priority2,nameAgent,nameDevice,device);break;
			case 3: addDeviceToMap(priority3,nameAgent,nameDevice,device);break;
		}
	}
	
	/**
	 * Turn on or off the device
	 * 
	 * @param nameAgent Agent's name that control the device
	 * @param nameDevice Device's name
	 * @param on Boolean value that indicate if turn on or off the device
	 */
	@OPERATION
	void turnDevice(String nameAgent,String nameDevice,boolean on){
		
		Device device=findDevice(nameAgent,nameDevice);

		if(device!=null){
			
			System.out.println("Accendo/spengo il device "+nameDevice+": "+on);
			
			if(on!=device.isOn()){
				device.setOn(on);
	
				if(on){
					currentConsumption+=device.getPowerConsumption();
					if(currentConsumption>treshold && !execute){
						execInternalOp("checkCurrentConsumption");
					}
				}else{
					device.setFlag(false);
					currentConsumption-=device.getPowerConsumption();
				}
			
				System.out.println("Consumo corrente: "+currentConsumption);
				getObsProperty(CURRENT_CONSUMPTION).updateValue(currentConsumption);
			}
		}
	}
	
	/**
	 * Change power consumption of the device
	 * 
	 * @param nameAgent Agent's name that control the device
	 * @param nameDevice Device's name
	 * @param powerConsumption Power consumption of the device
	 */
	@OPERATION
	void changePowerConsumption(String nameAgent,String nameDevice,int powerConsumption){
				
		Device device=findDevice(nameAgent,nameDevice);
		
		if(device!=null){
			
			System.out.println("Cambio il consumo di corrente del device "+nameDevice+" con consumo: "+powerConsumption);
			
			if(device.isOn()){
				System.out.println("Consumo corrente: "+currentConsumption);
				
				currentConsumption-=device.getPowerConsumption();
				device.setPowerConsumption(powerConsumption);
				currentConsumption+=powerConsumption;
				
				System.out.println("Consumo corrente: "+currentConsumption);
				
				getObsProperty(CURRENT_CONSUMPTION).updateValue(currentConsumption);
				
				if(currentConsumption>treshold && !execute){
					execInternalOp("checkCurrentConsumption");
				}
			}else{
				device.setPowerConsumption(powerConsumption);
			}
		}
	}
	
	/**
	 * Add power consumption to the device
	 * 
	 * @param nameAgent Agent's name that control the device
	 * @param nameDevice Device's name
	 * @param amount The amount of power consumption to be added to the device
	 */
	@OPERATION
	void addPowerConsumption(String nameAgent,String nameDevice,int amount){
		
		Device device=findDevice(nameAgent,nameDevice);
		
		if(device!=null){
			if(device.isOn()){
				System.out.println("Aggiungo consumo di corrente del device: "+nameDevice+" con consumo: "+amount);
				device.setPowerConsumption(device.getPowerConsumption()+amount);
				currentConsumption+=amount;
				
				System.out.println("Consumo corrente: "+currentConsumption);
				
				getObsProperty(CURRENT_CONSUMPTION).updateValue(currentConsumption);
				
				if(currentConsumption>treshold && !execute){
					execInternalOp("checkCurrentConsumption");
				}
			}else{
				device.setPowerConsumption(device.getPowerConsumption()+amount);
			}
		}
	}
	
	/**
	 * Remove power consumption to the device
	 * 
	 * @param nameAgent Agent's name that control the device
	 * @param nameDevice Device's name
	 * @param amount The amount of power consumption to be removed from the device
	 */
	@OPERATION
	void removePowerConsumption(String nameAgent,String nameDevice,int amount){
		
		Device device=findDevice(nameAgent,nameDevice);
		
		if(device!=null){
			System.out.println("Rimuovo consumo di corrente del device: "+nameDevice+" con consumo: "+amount);
			
			if(device.isOn()){
				device.setPowerConsumption(device.getPowerConsumption()-amount);
				currentConsumption-=amount;
				
				System.out.println("Consumo corrente: "+currentConsumption);
				
				getObsProperty(CURRENT_CONSUMPTION).updateValue(currentConsumption);
			}else{
				device.setPowerConsumption(device.getPowerConsumption()-amount);
			}
		}
	}
	
	@INTERNAL_OPERATION
	void checkCurrentConsumption(){
		
		int theoricalCurrentConsumption=currentConsumption;
		execute=true;
		
		while(currentConsumption>treshold){
			
			theoricalCurrentConsumption=turnOffDevice(priority1,theoricalCurrentConsumption,1);
			
			if(theoricalCurrentConsumption>treshold){
				theoricalCurrentConsumption=turnOffDevice(priority2,theoricalCurrentConsumption,2);
				
				if(theoricalCurrentConsumption>treshold){
					theoricalCurrentConsumption=turnOffDevice(priority3,theoricalCurrentConsumption,3);
				}else{
					await_time(1000);
				}
			}else{
				await_time(1000);
			}
		}
		
		execute=false;
	}
	
	private int turnOffDevice(HashMap<String,HashMap<String,Device>> map,int currentConsumption,int priority){
		int theoricalCurrentConsumption=currentConsumption;
		
		Set<String> set=map.keySet();
		
		for(String keyAgent:set){
			HashMap<String,Device> tmp=map.get(keyAgent);
			Set<String> setDevice=tmp.keySet();	
			
			for(String keyDevice:setDevice){
				Device device=tmp.get(keyDevice);
				
				if(device.isOn()){
					System.out.println(keyDevice+" priorita: "+priority+" flag: "+device.getFlag()+" on: "+device.isOn());
				}
				
				if(!device.getFlag() && device.isOn()){
					device.setFlag(true);
					signal("turnOffDevice",keyAgent,keyDevice);
					theoricalCurrentConsumption-=device.getPowerConsumption();
					
					if(theoricalCurrentConsumption<=treshold){
						return theoricalCurrentConsumption;
					}
				}
				
			}
		}
		
		return theoricalCurrentConsumption;
	}
	
	private void addDeviceToMap(HashMap<String,HashMap<String,Device>> map,String nameAgent,String nameDevice,Device device){
		if(map.containsKey(nameAgent)){
			if(!map.get(nameAgent).containsKey(nameDevice)){
				map.get(nameAgent).put(nameDevice,device);
			}
		}else{
			HashMap<String,Device> tmp=new HashMap<>();
			tmp.put(nameDevice,device);
			map.put(nameAgent,tmp);
		}
	}
	
	private Device findDevice(String nameAgent,String nameDevice){
		Device device=null;
		HashMap<String,Device> tmp=null;
		
		if(priority1.containsKey(nameAgent)){
			tmp=priority1.get(nameAgent);
			
			if(tmp.containsKey(nameDevice)){
				device=tmp.get(nameDevice);
			}
		}
		
		if(device==null){
			if(priority2.containsKey(nameAgent)){
				tmp=priority2.get(nameAgent);
				
				if(tmp.containsKey(nameDevice)){
					device=tmp.get(nameDevice);
				}
			}
			
			if(device==null){
				if(priority3.containsKey(nameAgent)){
					tmp=priority3.get(nameAgent);
					
					if(tmp.containsKey(nameDevice)){
						device=tmp.get(nameDevice);
					}
				}
			}
		}
		
		return device;
	}
	
	private class Device{

		private int powerConsumption;
		private boolean flag,on;
		
		public Device(int powerConsumption){
			this.powerConsumption=powerConsumption;
			flag=false;
			on=false;
		}
		
		public int getPowerConsumption(){
			return powerConsumption;
		}
		
		public boolean getFlag(){
			return flag;
		}
		
		public void setFlag(boolean flag){
			this.flag=flag;
		}
		
		public void setPowerConsumption(int powerConsumption){
			this.powerConsumption=powerConsumption;
		}
		
		public boolean isOn(){
			return on;
		}
		
		public void setOn(boolean on){
			this.on=on;
		}
	}

}
