package smartHome;

public class NameGpioOperation {

	public final static String SET_PIN_MODE="setPinMode";
	public final static String GET_PIN_MODE="getPinMode";
	public final static String READ="read";
	public final static String WRITE="write";
	public final static String READI2C="readI2C";
	public final static String WRITEI2C="writeI2C";
	public final static String READI2CDIRECT="readI2CDirect";
	public final static String WRITEI2CDIRECT="writeI2CDirect";
	public final static String ADDI2CDEVICE="addI2CDevice";
	public final static String START_TRACK_PIN="startTrackPin";
	public final static String STOP_TRACK_PIN="stopTrackPin";
	
	private NameGpioOperation(){}
}
