package smartHome;

import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.gpio.event.PinEventType;

/**
 * 
 * GPIO servo angle change event.
 * @author Francesco Cozzolino
 *
 */
public class GpioPinServoAngleChangeEvent extends GpioPinEvent {

	private static final long serialVersionUID = 1L;
	
	public GpioPinServoAngleChangeEvent(Object obj, GpioPin pin, PinEventType type) {
		super(obj, pin, type);
	}

	public long getAngle(){
		return ((GpioPinServoImpl)pin).getAngle();
	}

}
