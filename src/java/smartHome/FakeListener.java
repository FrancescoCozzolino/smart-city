package smartHome;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import cartago.IBlockingCmd;
/**
 * The listener class for receiving events
 * @author Francesco Cozzolino
 *
 */
public class FakeListener implements IBlockingCmd{

	private BlockingQueue<Boolean> events;
	private Boolean event;
	
	public FakeListener(){
		events=new ArrayBlockingQueue<>(1);
	}
	
	@Override
	public void exec() {
		try {
			event=events.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get the current event
	 * @return
	 */
	public boolean get(){
		return event;
	}
	
	/**
	 * Add the event
	 */
	public void add(){
		add(true);
	}
	
	/**
	 * Add the event
	 * @param value
	 */
	public void add(boolean value){
		try {
			events.put(value);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
