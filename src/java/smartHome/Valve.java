package smartHome;

import cartago.*;

/**
 * Artifact that model an electrovalve.
 * This Artifact extend Actuator's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Actuator
 */
public class Valve extends Actuator {

	private final static String PROPERTY="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which electrovalve is attached.
	 * @param pin Pin of the device which the electrovalve is attached.
	 */
	void init(ArtifactId idDevice, int pin) {
		super.init(idDevice, pin, false);
		defineObsProperty(PROPERTY, 0);
		defineObsProperty(POWER_CONSUMPTION, 200);
	}

	/**
	 * Turn on the electrovalve
	 */
	@OPERATION
	public void turnOn(){
		setValue(1);
	}
	
	/**
	 * Turn off the electrovalve
	 */
	@OPERATION
	public void turnOff(){
		setValue(0);
	}
	
	@LINK
	public void changeValue(long value){
		getObsProperty(PROPERTY).updateValue(value);
	}
}
