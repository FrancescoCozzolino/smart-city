package smartHome;

import com.pi4j.io.gpio.event.GpioPinListener;
/**
 * This interface implements the callback event handler for GPIO pin state changes.
 * @author Francesco Cozzolino
 *
 */
public interface GpioPinListenerServo extends GpioPinListener{

	void handleGpioPinServoAngleChangeEvent(GpioPinServoAngleChangeEvent arg0);
}
