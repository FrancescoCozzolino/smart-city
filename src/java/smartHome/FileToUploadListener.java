package smartHome;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import cartago.IBlockingCmd;

/**
 * The listener class for receiving files to upload.
 * @author Francesco Cozzolino
 *
 */
public class FileToUploadListener implements IBlockingCmd{

	private BlockingQueue<FileInfo> files;
	private FileInfo fileInfo;
	
	public FileToUploadListener() {
		files=new ArrayBlockingQueue<>(100);
		fileInfo=null;
	}
	
	@Override
	public void exec() {
		try {
			fileInfo=files.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return The current file to upload
	 */
	public FileInfo getFileInfo(){
		return fileInfo;
	}
	
	public void addFileToUpload(String path,String fileName){
		try {
			files.put(new FileInfo(path,fileName));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}


