// CArtAgO artifact code for project smartHome

package smartHome;

import cartago.*;
/**
 * Artifact that model a moisture sensor of soil.
 * This Artifact extend Sensor's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Sensor
 */
public class MoistureSensor extends Sensor {
	
	private final static String PROPERTY="soil";
	private final static String DRY="dry";
	private final static String HUMID="humid";
	private final static String WET="wet";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private int state;
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which moisture sensor is attached.
	 * @param pin Pin of the device which the moisture sensor is attached.
	 */
	void init(ArtifactId idDevice, int pin){		
		super.init(idDevice, pin, true);
		defineObsProperty(PROPERTY, "");
		defineObsProperty(POWER_CONSUMPTION, 20);
	}
	
	@OPERATION
	@Override
	public void startTrackPin(){
		state=-1;
		changeValue(read().get());
		super.startTrackPin();
	}
	
	@OPERATION
	@Override
	public void stopTrackPin(){
		super.stopTrackPin();
		getObsProperty(PROPERTY).updateValue("");
		state=-2;
	}

	@LINK
	public void changeValue(long value){
		if(state!=-2){
			if(value>=0&&value<=200){
				if(state!=0){
					state=0;
					getObsProperty(PROPERTY).updateValue(DRY);
				}
			}else if(value>300 && value<=600){
				if(state!=1){
					state=1;
					getObsProperty(PROPERTY).updateValue(HUMID);
				}
			}else if(value>700 && value<=1023){
				if(state!=2){
					state=2;
					getObsProperty(PROPERTY).updateValue(WET);
				}
			}
		}
	}
}

