package smartHome;

import cartago.OpFeedbackParam;

public interface ArtifactSensor {

	/**
	 * Read the value perceived from the sensor from the environment 
	 * @return The value perceived from the sensor from the environment
	 */
	OpFeedbackParam<Long> read();
}
