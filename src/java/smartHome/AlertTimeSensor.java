package smartHome;

import cartago.*;
/**
 * Artifact that model a sensor can perceive the environment
 * and emit a signal when the average values read are over an established threshold
 * for an established time.
 * This artifact extend Sensor's Artifact.
 * 
 * @author Francesco
 * @see Sensor
 */
public class AlertTimeSensor extends Sensor {

	private final static String STATE="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean flag,flagContinue;
	private int threshold;
	private long time;
	
	/**
	 * Initialize the sensor
	 * 
	 * @param idDevice Artifact's identifier that control the device which the sensor is attached
	 * @param pin Pin of the device which the sensor is attached
	 * @param threshold The threshold value when the artifact will emit a signal
	 * @param second The amount of time in which the read values are above the threshold before the
	 * artifact emit the signal
	 */
	void init(ArtifactId idDevice, int pin,int threshold,int second) {
		super.init(idDevice, pin, true);
		this.threshold=threshold;
		flag=false;
		time=second<=0?500:second*100;
		defineObsProperty(STATE,0);
		defineObsProperty(POWER_CONSUMPTION,100);
	}
	
	@OPERATION
	@Override
	public void startTrackPin() {
		flagContinue=true;
		super.startTrackPin();
		getObsProperty(STATE).updateValue(1);
		
	}
	
	@OPERATION
	@Override
	public void stopTrackPin() {
		flagContinue=false;
		super.stopTrackPin();
		getObsProperty(STATE).updateValue(0);
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		if((int)getObsProperty(STATE).getValue()!=0){
			if(value>=threshold&&!flag){
				flag=true;
				execInternalOp("monitor",value);
			}
		}
	}
	
	@INTERNAL_OPERATION
	void monitor(long value){
		
		int number_read=0;
		
		for(int i=1;i<=10;i++){
			await_time(time);
			
			if(flagContinue){
				value+=read().get();
				number_read=i;
			}
		}
		
		value/=(number_read+1);
		
		if(value>=threshold){
			signal("alarmGas");
		}
		
		flag=false;
	}
	

}
