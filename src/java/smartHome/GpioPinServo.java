package smartHome;

/**
 * This is a decorator interface to describe servo pin.
 * @author Francesco Cozzolino
 *
 */
public interface GpioPinServo {
	
	/**
	 * Rotate servo to the desired angle 
	 * @param angle Angle of servo motor. The value must be between 0 and 180
	 */
	void setAngle(long angle);
	
	/**
	 * 
	 * @return The pwm value of pin that rotate the servo motor
	 */
	int getServoPulseWidth();
	
	/**
	 * 
	 * @return The angle of servo motor
	 */
	long getAngle();
}
