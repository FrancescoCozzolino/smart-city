package smartHome;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.NetworkIOException;
import com.dropbox.core.RetryException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.UploadSessionFinishErrorException;
import com.dropbox.core.v2.files.UploadSessionLookupErrorException;
import com.dropbox.core.v2.files.WriteMode;

import cartago.*;
/**
 * Artifact that permit to upload a file on Dropbox account
 * 
 * @author Francesco Cozzolino
 *
 */
public class Dropbox extends Artifact{
	
	private static final long CHUNKED_UPLOAD_CHUNK_SIZE = 8L << 20; // 8MiB
    private static final int CHUNKED_UPLOAD_MAX_ATTEMPTS = 5;
	private FileToUploadListener listener;
	private FakeListener fakeListener;
	private DbxClientV2 client;
	private long freespace;
	
	/**
	 * Initialize the artifact for upload the incoming files on a Dropbox account
	 * 
	 * @param clientIdentifier Identifier of Dropbox account
	 * @param accessToken Token for the specific app who want upload a file on a Dropbox account
	 */
	void init(String clientIdentifier,String accessToken){
		listener=new FileToUploadListener();
		fakeListener=new FakeListener();
		client = new DbxClientV2(new DbxRequestConfig(clientIdentifier),accessToken);
		try {
			freespace=client.users().getSpaceUsage().getAllocation().getIndividualValue().getAllocated()-client.users().getSpaceUsage().getUsed();
		} catch (DbxException e) {
			e.printStackTrace();
		} 

		execInternalOp("uploadFile");
	}
	
	/**
	 * Upload a file
	 * 
	 * @param path Destination path of Dropbox account
	 * @param fileName Name of file to upload
	 */
	@LINK
	void upload(String path,String fileName){
		listener.addFileToUpload(path,fileName);
	}
	
	@INTERNAL_OPERATION
	void uploadFile(){
		while(true){
			await(listener);
			
			FileInfo fileInfo=listener.getFileInfo();
			File file=new File(fileInfo.getName());
			String path=fileInfo.getPath();
			
			if(file.exists()){
				if(freespace-file.length()>0){
					freespace-=file.length();
					System.out.println(file.getName());
					
					if(file.length()<= (2 * CHUNKED_UPLOAD_CHUNK_SIZE)){
						new Thread(()->fakeListener.add(uploadSmallFile(path,file)?true:false)).start();
					}else{
						new Thread(()->fakeListener.add(uploadLargeFile(path,file)?true:false)).start();
					}
					
					await(fakeListener);
					
					if(fakeListener.get()){
						signal("completed",file.getName());
					}
				}
			}
		}
	}
	
	private boolean uploadLargeFile(String path,File file){
		long size = file.length();
        long uploaded = 0L;
        @SuppressWarnings("unused")
		DbxException thrown = null;

        // Chunked uploads have 3 phases, each of which can accept uploaded bytes:
        //
        //    (1)  Start: initiate the upload and get an upload session ID
        //    (2) Append: upload chunks of the file to append to our session
        //    (3) Finish: commit the upload and close the session
        //
        // We track how many bytes we uploaded to determine which phase we should be in.
       
        String sessionId = null;
        for (int i = 0; i < CHUNKED_UPLOAD_MAX_ATTEMPTS; ++i) {
  
            try (InputStream in = new FileInputStream(file)) {
                // if this is a retry, make sure seek to the correct offset
                in.skip(uploaded);

                // (1) Start
                if (sessionId == null) {
                    sessionId = client.files().uploadSessionStart()
                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE)
                        .getSessionId();
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                }

                UploadSessionCursor cursor = new UploadSessionCursor(sessionId, uploaded);

                // (2) Append
                while ((size - uploaded) > CHUNKED_UPLOAD_CHUNK_SIZE) {
                    client.files().uploadSessionAppendV2(cursor)
                        .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE);
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                    cursor = new UploadSessionCursor(sessionId, uploaded);
                }

                // (3) Finish
                long remaining = size - uploaded;
                CommitInfo commitInfo = CommitInfo.newBuilder(/*"/video/"*/path+"/"+file.getName())
                    .withMode(WriteMode.ADD)
                    .withClientModified(new Date(file.lastModified()))
                    .build();
                client.files().uploadSessionFinish(cursor, commitInfo).uploadAndFinish(in, remaining);

                return true;
            } catch (RetryException ex) {
                thrown = ex;
                // RetryExceptions are never automatically retried by the client for uploads. Must
                // catch this exception even if DbxRequestConfig.getMaxRetries() > 0.
                try {
                    Thread.sleep(ex.getBackoffMillis());
                } catch (InterruptedException ex2) {
                	ex2.printStackTrace();
                }
                continue;
            } catch (NetworkIOException ex) {
                thrown = ex;
                // network issue with Dropbox (maybe a timeout?) try again
                continue;
            } catch (UploadSessionLookupErrorException ex) {
                if (ex.errorValue.isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                        .getIncorrectOffsetValue()
                        .getCorrectOffset();
                    continue;
                } else {
                    ex.printStackTrace();
                    freespace+=file.length();
                    return false;
                }
            } catch (UploadSessionFinishErrorException ex) {
                if (ex.errorValue.isLookupFailed() && ex.errorValue.getLookupFailedValue().isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                        .getLookupFailedValue()
                        .getIncorrectOffsetValue()
                        .getCorrectOffset();
                    continue;
                } else {
                    ex.printStackTrace();
                    freespace+=file.length();
                    return false;
                }
            } catch (DbxException ex) {
            	ex.printStackTrace();
            	freespace+=file.length();
            	return false;
            } catch (IOException ex) {
                ex.printStackTrace();
                freespace+=file.length();
                return false;
            }
        }
        
        freespace+=file.length();
        return false;
	}
	
	private boolean uploadSmallFile(String path,File file){
		try{
			InputStream in = new FileInputStream(file);
			client.files().uploadBuilder(/*"/video/"*/path+"/"+file.getName()).uploadAndFinish(in);
			return true;
		} catch (DbxException | IOException e) {
			e.printStackTrace();
			freespace+=file.length();
			return false;
		}
	}
}
