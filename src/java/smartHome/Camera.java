package smartHome;
// CArtAgO artifact code for project smartHome

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfRect;
import org.opencv.core.Size;
import org.opencv.objdetect.HOGDescriptor;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;

import cartago.*;

/**
 * Artifact that permit to detect people with a camera, 
 * record and save the video in local and on a cloud platform.
 *  
 * @author Francesco Cozzolino
 *
 */
public class Camera extends Artifact {
	
	private final static String PROPERTY="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean stopCapture;
	private ObsProperty state;
	private ArtifactId idCloud;
	private ArrayList<Mat> frames;
	private FakeListener listener;
	
	/**
	 * initialize the artifact that control the camera
	 * 
	 * @param idDropbox Artifact's id that upload the video on cloud platform.
	 * Pass null if you not want upload the video on cloud.
	 */
	void init(ArtifactId idCloud) {
		this.idCloud=idCloud;
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		frames=new ArrayList<>();
		stopCapture=false;
		defineObsProperty(PROPERTY,"off");
		defineObsProperty(POWER_CONSUMPTION,250);
		state=getObsProperty(PROPERTY);
		listener=new FakeListener();
	}
	
	/**
	 * Start detecting people
	 * 
	 */
	@OPERATION
	void start(){
		state.updateValue("on");
		execInternalOp("capture");		
	}
	
	/**
	 * Stop detecting people
	 */
	@OPERATION
	void stop(){
		stopCapture=true;
		state.updateValue("stopping");
	}
	
	@INTERNAL_OPERATION
	void capture(){
		VideoCapture video=new VideoCapture(0);
		video.set(Videoio.CAP_PROP_FRAME_WIDTH, 640);
		video.set(Videoio.CAP_PROP_FRAME_HEIGHT, 480);
		
		Mat frame=new Mat();
		MatOfRect foundLocations=new MatOfRect();
		MatOfDouble weights=new MatOfDouble();
		HOGDescriptor hog=new HOGDescriptor();
		Size winStride=new Size(8,8);
		Size padding=new Size(0,0); 
		hog.setSVMDetector(HOGDescriptor.getDefaultPeopleDetector());
		VideoWriter writer=new VideoWriter();
		boolean flag=false,continueDetection=true;
		long startTime=0;
		String fileName="";
		
		
		while(video.read(frame)&&continueDetection){
			if(!flag){
				
				if(stopCapture){
					continueDetection=false;
					stopCapture=false;
				}
				
				if(continueDetection){
					new Thread(()->{
						hog.detectMultiScale(frame,foundLocations,weights,0,winStride,padding,1.05,2,false);
						listener.add();
					}).start();

					await(listener);
					
					if(frames.size()<270){
						frames.add(frame);
					}else{
						frames.remove(0);
						frames.add(frame);
					}
					
					if(foundLocations.rows()>0){
						getObsProperty(POWER_CONSUMPTION).updateValue(200);
						signal("alarm");
						flag=true;
						startTime=System.currentTimeMillis();
						Size frameSize= new Size((int) video.get(Videoio.CAP_PROP_FRAME_WIDTH),(int) video.get(Videoio.CAP_PROP_FRAME_HEIGHT));
						fileName="video"+new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date())+".avi";
						writer.open(fileName,VideoWriter.fourcc('M','J','P','G'),video.get(Videoio.CAP_PROP_FPS),frameSize,true);
						writer.write(frames.remove(0));
					}
				}
			}else{
				frames.add(frame);
				
				new Thread(()->{
					writer.write(frames.remove(0));
					listener.add();
				}).start();
				
				await(listener);

				if((System.currentTimeMillis()-startTime)>=60000){
					hog.detectMultiScale(frame,foundLocations,weights,0,winStride,padding,1.05,2,false);

					if(foundLocations.rows()==0){
						
						while(frames.size()!=0){
							writer.write(frames.remove(0));
						}
						
						writer.release();
						flag=false;

						getObsProperty(POWER_CONSUMPTION).updateValue(250);
						signal("startUpload");
						
						try {
							if(idCloud!=null){
								execLinkedOp(idCloud,"upload","/video",fileName);
							}
						} catch (OperationException e) {
							e.printStackTrace();
						}

					}else{
						startTime=System.currentTimeMillis();
					}
				}
			}
		}
		
		frames.clear();
		video.release();
	    foundLocations.release();
	    weights.release();
	    frame.release();

	    if(writer!=null){
			writer.release();
		}	    
	    
	    state.updateValue("off");
	    
	}
}

