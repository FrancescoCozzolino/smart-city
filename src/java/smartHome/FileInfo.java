package smartHome;

/**
 * Class that contains some info of a file
 * @author Francesco
 *
 */
public class FileInfo{
	private String path,name;
	
	/**
	 * 
	 * @param path Path of file
	 * @param name Name of file
	 */
	public FileInfo(String path,String name){
		this.path=path;
		this.name=name;
	}
	
	/**
	 * Get path of file
	 * @return Path of file
	 */
	public String getPath(){
		return path;
	}
	
	/**
	 * Get name of file
	 * @return Name of file
	 */
	public String getName(){
		return name;
	}
}
