package smartHome;

import java.io.IOException;
import java.util.HashMap;

import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.system.SystemInfo;

public class RaspberryPin {

	private static HashMap<Integer,Pin> pin=null;
	
	private RaspberryPin(){}
	
	/**
	 * 
	 * @return Return a map where the key is the number of pin (Wiring pi notation)
	 * and the value is the Pin interface of pi4j
	 */
	public synchronized static HashMap<Integer,Pin> getPins(){
		if(pin==null){
			pin=new HashMap<>();
			try {
				Pin[] raspiPin=RaspiPin.allPins(SystemInfo.getBoardType());
				
				for(Pin tmp:raspiPin){
					pin.put(tmp.getAddress(),tmp);
				}
			} catch (UnsupportedOperationException | IOException |InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		return pin;
	}
}
