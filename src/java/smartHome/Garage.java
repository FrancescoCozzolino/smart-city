// CArtAgO artifact code for project smartHome

package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.*;

/**
 * Artifact that model a garage with a servo motor.
 * This Artifact extend Transducer's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Transducer
 *
 */
public class Garage extends Transducer {
	private final static String PROPERTY="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean flag,continueOp=true,open,close;
	private int restart;
	private Object lock;
	private ObsProperty property;
	private FakeListener listener;
	
	/**
	 * Initialize the garage 
	 * 
	 * @param idDevice Artifact's identifier that control the device which garage is attached
	 * @param pin Pin of the device which the garage is attached
	 */
	void init(ArtifactId idDevice,int pin) {
		super.init(idDevice,pin,Mode.SERVO);
		defineObsProperty(PROPERTY,"close");
		defineObsProperty(POWER_CONSUMPTION, 400);
		property=getObsProperty(PROPERTY);
		lock=new Object();
		flag=false;
		listener=new FakeListener();
		execInternalOp("calibrate");
	}
	
	@INTERNAL_OPERATION
	void calibrate(){
		setValue(179);
		setValue(0);
		restart=1;
		open=true;
		close=false;
	}
	
	/**
	 * Close the garage
	 */
	@OPERATION
	void close(){
		if(close){
			close=false;
			open=true;
			flag=false;
			property.updateValue("closing");
			execInternalOp("write",restart,0,false);
		}
	}
	
	/**
	 * Open the garage
	 */
	@OPERATION
	void open(){
		if(open){
			open=false;
			close=true;
			flag=false;
			property.updateValue("opening");
			execInternalOp("write",restart,180,true);
		}
	}
	
	@GUARD
	boolean checkContinue(){
		synchronized(lock){
			return continueOp;
		}
	}
	
	/**
	 * Stop open or close the garage
	 */
	@OPERATION
	void stop(){
		flag=true;
		await("checkContinue");
		property.updateValue("stop");
		close=true;
		open=true;
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		if(value==180){
			if(((String)property.getValue()).compareTo("opening")==0){
				open=false;
				close=true;
				property.updateValue("open");
			}
		}else if(value==0){
			if(((String)property.getValue()).compareTo("closing")==0){
				open=true;
				close=false;
				property.updateValue("close");
			}
		}
	}
	
	@INTERNAL_OPERATION
	void write(int start,int end,boolean crescent){
		synchronized(lock){
			continueOp=false;
		}
		
		if(crescent){
			restart=179;
			for(int i=start;i<=end;i++){
				int angle=i;
				if(!flag){
					setValue(angle);
					new Thread(()->listener.add()).start();
					await(listener);
				}else{
					open=false;
					close=true;
					restart=i;
					i=end+1;
				}
	
				await_time(20);
			}
		}else{
			restart=1;
			
			for(int i=start;i>=end;i--){
				int angle=i;
				if(!flag){
					setValue(angle);
					new Thread(()->listener.add()).start();
					await(listener);
				}else{
					open=true;
					close=false;
					restart=i;
					i=end-1;
				}
				
				await_time(20);
			}
		}
		
		if(!flag){
			if(crescent){
				restart=179;
			}else{
				restart=1;
			}
		}else{
			flag=false;
		}
		
		synchronized(lock){
			continueOp=true;
		}
	}
	
	@INTERNAL_OPERATION
	void setValue(int value){
		try {
			execLinkedOp(idDevice,NameGpioOperation.WRITE,pin, value);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
}

