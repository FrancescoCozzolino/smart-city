package smartHome;

import java.util.Random;

import cartago.*;

/**
 * Artifact that model a washer.
 * This Artifact extend Actuator's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see Actuator
 */
public class Washer extends Actuator {

	//PROVARE A TOGLIERE LA VARIABILE BOOLEAN E USARE LA PROPRIETA
	private static final String PROPERTY="state";
	private static final String TIME="timeWait";
	private static final String REMAINING_TIME="remainingTime";
	private static final String START_TIME="startTime";
	private static final String PROGRAM="program";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean isOn=false;
	private int program;
	private long[] programs;
	private FakeListener listener;
	private Wait wait;
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which washer is attached.
	 * @param pin Pin of the device which the washer is attached.
	 */
	void init(ArtifactId idDevice,int pin) {
		super.init(idDevice, pin, false);
		defineObsProperty(TIME,0L);
		defineObsProperty(START_TIME,0L);
		defineObsProperty(REMAINING_TIME,0L);
		defineObsProperty(PROPERTY, "off");
		defineObsProperty(PROGRAM,1);
		defineObsProperty(POWER_CONSUMPTION,400);
		programs = new long[10];
		Random r=new Random();
		listener=new FakeListener();
		
		for(int i=0;i<programs.length;i++){
			programs[i]= (r.nextInt(5)+1)*10000;
		}
	}
	
	/**
	 * Start the washer with the selected program
	 * @param program Program for the washer. The value must be between 0 and 9
	 */
	@OPERATION
	void start(int program){
		if(!isOn){
			isOn=true;
			if(program<0||program>9){
				program=program<0?0:9;
			}
			this.program=program;
			setValue(1);
		}
	}
	
	/**
	 * Resume the washer for ther remaining time of the program
	 */
	@OPERATION
	void resume(){
		if(!isOn){
			isOn=true;
			setValue(1);
		}		
	}
	
	/**
	 * Stop the washer
	 */
	@OPERATION
	void stop(){
		if(isOn){
			wait.interrupt();			
		}
	}
	
	@OPERATION
	void refreshRemainingTime(){
		getObsProperty(REMAINING_TIME).updateValue((((long)getObsProperty(TIME).getValue())-(System.currentTimeMillis()-((long)getObsProperty(START_TIME).getValue()))));
	}
	
	@INTERNAL_OPERATION
	void waitTime(long time){
		wait=new Wait(listener,time);
		wait.start();
		await(listener);
		
		if(isOn){
			long timeT=(long)getObsProperty(TIME).getValue();
			long startTime=(long)getObsProperty(START_TIME).getValue();
			long tmp= timeT-(System.currentTimeMillis()-startTime);
			if(tmp>0){
				getObsProperty(PROPERTY).updateValue("pausing");
			}
			getObsProperty(TIME).updateValue(tmp);
			System.out.println("Spengo la lavatrice: "+System.currentTimeMillis());
			isOn=false;
			setValue(0);
		}
	}
	
	
	@LINK
	public void changeValue(long value){
		if(value!=0){
			
			if(((String)getObsProperty(PROPERTY).getValue()).equals("pause")){
				System.out.println("Riprendo il lavaggio");
			}else{
				getObsProperty(TIME).updateValue(programs[program]);
				getObsProperty(PROGRAM).updateValue(program);
				getObsProperty(POWER_CONSUMPTION).updateValue(400+(10*program));
				System.out.println("Selezionato programma : "+program+" per : "+programs[program]+" secondi");
			}
			
			getObsProperty(START_TIME).updateValue(System.currentTimeMillis());
			getObsProperty(PROPERTY).updateValue("on");
			execInternalOp("waitTime",(long)getObsProperty(TIME).getValue());
		}else{
			if(getObsProperty(PROPERTY).getValue().equals("pausing")){
				getObsProperty(PROPERTY).updateValue("pause");
				getObsProperty(REMAINING_TIME).updateValue((long)getObsProperty(TIME).getValue());
			}else{
				getObsProperty(PROPERTY).updateValue("off");
				getObsProperty(REMAINING_TIME).updateValue(0);
			}
		}
	}

}
