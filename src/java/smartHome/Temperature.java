// CArtAgO artifact code for project smartHome

package smartHome;

import cartago.*;
/**
 * Artifact that model the I2C sensor temperature mcp9808.
 * This artifact extend I2CDevice's Artifact.
 * 
 * @author Francesco Cozzolino
 * @see I2CDevice
 */
public class Temperature extends I2CDevice{
	private final static String PROPERTY="temperature";
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which temperature sensor is attached
	 * @param address Address of I2C device 
	 */
	void init(ArtifactId idDevice,byte address) {
		super.init(idDevice,address);
	}
	
	/**
	 * Read the temperature (Celsius degrees) from the sensor
	 */
	@OPERATION
	void readTemperature(){
		/*try {
			execLinkedOp(idDevice,MicroControllerOperation.READI2C,this,address,(byte)0x05,(byte)0x02);	
		} catch (OperationException e) {
			e.printStackTrace();
		}*/
		
		execInternalOp("readI2C",(byte)0x05,(byte)0x02);
	}
	
	public void changeValueI2C(byte[] value){
		int t = (value[0] << 8) + (value[1]&0xFF);
		double temp = (t & 0x0FFF)/16.0;
		
		if ((t & 0x1000)==0x1000){
			temp -= 256;
		}

		if(hasObsProperty(PROPERTY)){
			getObsProperty(PROPERTY).updateValue(temp);
		}else{
			defineObsProperty(PROPERTY,temp);
		}
	}
}

