package smartHome;

import cartago.ArtifactId;

/**
 * The ArtifactTransducer interface defines the necessary methods for an artifact tha model a transducer
 * 
 * @author Francesco Cozzolino
 *
 */
public interface ArtifactTransducer{
	final static String METHOD="changeValue";
	
	/**
	 * Set the working mode of transducer
	 */
	void setPinMode();
	
	/**
	 *  Start track the state of the pin in which the transducer is attached 
	 */
	void startTrackPin();
	
	/**
	 * Stop track the state of the pin in which the transducer is attached
	 */
	void stopTrackPin();
	
	
	/**
	 * After call startTrackPin operation, the device in which the tranducer is attacched
	 * invoke this operation at every change of pin in which the transducer is attached
	 * 
	 * @param value Value of pin in which the transducer is attached
	 */
	void changeValue(long value);
	
	/**
	 * Return the Id of artifact
	 * @return Id of artifact
	 */
	ArtifactId getArtifactId();
}
