package smartHome;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import cartago.IBlockingCmd;

/**
 * The listener class for receiving events from Raspberry pi.
 * @author Francesco Cozzolino
 *
 */
public class RaspEventListener implements IBlockingCmd, GpioPinListenerDigital, GpioPinListenerAnalog, GpioPinListenerServo{

	private BlockingQueue<GpioPinEvent> events;
	private GpioPinEvent event;
	
	public RaspEventListener(){
		events=new ArrayBlockingQueue<>(100);
		event=null;
	}

	@Override
	public void handleGpioPinAnalogValueChangeEvent(GpioPinAnalogValueChangeEvent arg0) {
		try {
			events.put(arg0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent arg0) {
		try {
			events.put(arg0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleGpioPinServoAngleChangeEvent(GpioPinServoAngleChangeEvent arg0) {
		try {
			events.put(arg0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	@Override
	public void exec() {
		try {
			event=events.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @return the current event
	 */
	public GpioPinEvent getEvent(){
		return event;
	}
}
