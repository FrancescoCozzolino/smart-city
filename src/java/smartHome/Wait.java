package smartHome;

/**
 * Class that extends Thread class.
 * Wait amount of time specified in the constructor and adds an event
 * to a FakeListener object after waited this amount of time
 * @author Francesco
 *
 */
public class Wait extends Thread{

	private FakeListener listener;
	private long time;
	
	public Wait(FakeListener listener,long time){
		this.listener=listener;
		this.time=time;
	}
	
	
	@Override
	public void run() {
		try {
			Thread.sleep(time);
			listener.add(true);
		} catch (InterruptedException e) {
			listener.add(false);
		}
	}
}
