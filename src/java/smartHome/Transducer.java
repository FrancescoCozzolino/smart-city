// CArtAgO artifact code for project smartHome

package smartHome;

import org.firmata4j.Pin.Mode;

import cartago.*;

/**
 * Artifact that model a transducer. It can be an actuator or a sensor.
 * This artifact implement ArtifactTransducer's interface.
 * 
 * @author Francesco
 * @see ArtifactTransducer
 */
public abstract class Transducer extends Artifact implements ArtifactTransducer{

	private final static String TRACK_PIN="trackPin";
	private Mode mode;
	protected ArtifactId idDevice;
	protected int pin;
	
	/**
	 * Initialize the artifact
	 * 
	 * @param idDevice Artifact's identifier that control the device which the transducer is attached
	 * @param pin Pin of the device which the transducer is attached.
	 * @param mode Working mode of the transducer
	 */
	void init(ArtifactId idDevice, int pin, Mode mode) {
		this.idDevice=idDevice;
		this.pin=pin;
		this.mode=mode;
		defineObsProperty(TRACK_PIN, false);
	}
	
	/**
	 * Set the working mode of the pin of the device in which the transducer is attached
	 */
	@OPERATION
	public void setPinMode(){
		try {
			execLinkedOp(idDevice,NameGpioOperation.SET_PIN_MODE,pin,mode);
		} catch (OperationException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * Start track the state of the pin in which the transducer is attached 
	 * 
	 */
	@OPERATION
	public void startTrackPin(){
		try {
			execLinkedOp(idDevice,NameGpioOperation.START_TRACK_PIN,pin, this);
			getObsProperty(TRACK_PIN).updateValue(true);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Stop track the state of the pin in which the transducer is attached
	 * 
	 */
	@OPERATION
	public void stopTrackPin(){
		try {
			execLinkedOp(idDevice,NameGpioOperation.STOP_TRACK_PIN,pin, this);
			getObsProperty(TRACK_PIN).updateValue(false);
		} catch (OperationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * Return the Id of artifact
	 * 
	 */
	public ArtifactId getArtifactId(){
		return getId();
	}
	
	public abstract void changeValue(long value);
	
}

