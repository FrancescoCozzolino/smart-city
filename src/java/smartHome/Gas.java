package smartHome;

import cartago.*;

public class Gas extends Sensor {

	private final static String STATE="state";
	private final static String POWER_CONSUMPTION="powerConsumption";
	private boolean flag,flagContinue;
	private int treshold;
	
	void init(ArtifactId idDevice, int pin,int treshold) {
		super.init(idDevice, pin, true);
		this.treshold=treshold;
		flag=false;
		defineObsProperty(STATE,0);
		defineObsProperty(POWER_CONSUMPTION,100);
	}
	
	@OPERATION
	@Override
	public void startTrackPin() {
		flagContinue=true;
		super.startTrackPin();
		getObsProperty(STATE).updateValue(1);
		
	}
	
	@OPERATION
	@Override
	public void stopTrackPin() {
		flagContinue=false;
		super.stopTrackPin();
		getObsProperty(STATE).updateValue(0);
	}
	
	@LINK
	@Override
	public void changeValue(long value) {
		if((int)getObsProperty(STATE).getValue()!=0){
			if(value>=treshold&&!flag){
				flag=true;
				execInternalOp("monitor",value);
			}
		}
	}
	
	@INTERNAL_OPERATION
	void monitor(long value){
		
		int number_read=0;
		
		for(int i=1;i<=10;i++){
			await_time(500);
			
			if(flagContinue){
				value+=read().get();
				number_read=i;
			}
		}
		
		value/=(number_read+1);
		
		if(value>=treshold){
			signal("alarmGas");
		}
		
		flag=false;
	}
	

}
