package smartHome;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.pi4j.component.servo.ServoDriver;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinShutdown;
import com.pi4j.io.gpio.GpioProvider;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.PinEventType;

public class GpioPinServoImpl implements GpioPin,GpioPinServo{

	private ServoDriver servo;
	private Set<GpioPinListener> listeners;
	private long angle;
	private Object lock;
	private int min,max;
	private String name;
	private Object tag;
	private final Map<String, String> properties = new ConcurrentHashMap<String, String>();
	
	public GpioPinServoImpl(ServoDriver servo){
		this.servo=servo;
		readFile();
		listeners=Collections.synchronizedSet(new HashSet<GpioPinListener>());
		lock=new Object();
	}
	
	@Override
	public void setAngle(long angle){
		synchronized(lock){
			this.angle=angle;
			servo.setServoPulseWidth((int) (((angle/180.0)*(max-min))+min));
		}
		
		for(GpioPinListener tmp:listeners){
			if(tmp instanceof GpioPinListenerServo){
				((GpioPinListenerServo)tmp).handleGpioPinServoAngleChangeEvent(new GpioPinServoAngleChangeEvent(this,this,PinEventType.ANALOG_VALUE_CHANGE));
			}
		}
	}
	
	@Override
	public int getServoPulseWidth(){
		synchronized(lock){
			return servo.getServoPulseWidth();
		}
	}
	
	@Override
	public long getAngle(){
		synchronized(lock){
			return angle;
		}
	}

	@Override
	public void addListener(List<? extends GpioPinListener> arg0) {
		for(GpioPinListener tmp:arg0){
			listeners.add(tmp);
		}
	}

	@Override
	public void clearProperties() {
		properties.clear();
	}

	@Override
	public void export(PinMode arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void export(PinMode arg0, PinState arg1) {
		// TODO Auto-generated method stub	
	}

	@Override
	public PinMode getMode() {
		return PinMode.ANALOG_OUTPUT;
	}

	@Override
	public String getName() {
		if(name==null){
			return servo.getPin().getName();
		}else if(name.length()==0){
			return servo.getPin().getName();
		}
		
		return name;
	}

	@Override
	public Pin getPin() {
		return servo.getPin();
	}

	@Override
	public Map<String, String> getProperties() {
		return properties;
	}

	@Override
	public String getProperty(String arg0) {
		return getProperty(arg0, null);
	}

	@Override
	public String getProperty(String arg0, String arg1) {
		if (properties.containsKey(arg0)) {
			if(properties.get(arg0) == null || properties.get(arg0).isEmpty())
				return arg1;
			else
				return properties.get(arg0);
		}
		
		return arg1;
	}

	@Override
	public GpioProvider getProvider() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PinPullResistance getPullResistance() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GpioPinShutdown getShutdownOptions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getTag() {
		return tag;
	}

	@Override
	public boolean hasListener(GpioPinListener... arg0) {
		return listeners.contains(arg0);
	}

	@Override
	public boolean hasProperty(String arg0) {
		return properties.containsKey(arg0);
	}

	@Override
	public boolean isExported() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isMode(PinMode arg0) {
		return PinMode.ANALOG_OUTPUT==arg0;
	}

	@Override
	public boolean isPullResistance(PinPullResistance arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeListener(GpioPinListener... arg0) {
		for(GpioPinListener tmp:arg0){
			listeners.remove(tmp);
		}
	}

	@Override
	public void removeListener(List<? extends GpioPinListener> arg0) {
		for(GpioPinListener tmp:arg0){
			listeners.remove(tmp);
		}
	}

	@Override
	public void removeProperty(String arg0) {
		if (properties.containsKey(arg0)) {
			properties.remove(arg0);
		}
	}

	@Override
	public void setMode(PinMode arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setName(String arg0) {
		name=arg0;
	}

	@Override
	public void setProperty(String arg0, String arg1) {
		properties.put(arg0, arg1);
	}

	@Override
	public void setPullResistance(PinPullResistance arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setShutdownOptions(GpioPinShutdown arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setShutdownOptions(Boolean arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setShutdownOptions(Boolean arg0, PinState arg1) {
		// TODO Auto-generated method stub	
	}

	@Override
	public void setShutdownOptions(Boolean arg0, PinState arg1, PinPullResistance arg2) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setShutdownOptions(Boolean arg0, PinState arg1, PinPullResistance arg2, PinMode arg3) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setTag(Object arg0) {
		tag=arg0;	
	}

	@Override
	public void unexport() {
		// TODO Auto-generated method stub		
	}

	@Override
	public void addListener(GpioPinListener... arg0) {
		for(GpioPinListener tmp:arg0){
			listeners.add(tmp);
		}
	}

	@Override
	public Collection<GpioPinListener> getListeners() {
		return listeners;
	}

	@Override
	public void removeAllListeners() {
		listeners.clear();
	}
	
	private void readFile(){
		try {
			String fileName="servo"+servo.getPin().getAddress()+".txt";
			String minString=null;
			String maxString=null;
			File file=new File(fileName);

			if(file.exists()){
				BufferedReader br = new BufferedReader(new FileReader(fileName));
				minString = br.readLine();
		    
				if(minString!=null){
					maxString=br.readLine();
					
		    	
					if(maxString!=null){
						try{
							min=Integer.parseInt(minString);
						}catch(NumberFormatException e){
							min=50;
						}
			    	
						try{
							max=Integer.parseInt(maxString);
						}catch(NumberFormatException e){
							max=250;
						}
			    	
						if(min<0){
							min=50;
						}
					
						if(max<0){
							max=250;
						}
					
						if(min>max){
							int tmp=min;
							min=max;
							max=tmp;
						}

					}else{
						min=50;
						max=250;
					}
				}else{
					min=50;
					max=250;
				}	
				
				br.close();
			}else{
				min=50;
				max=250;
			}
		} catch (IOException e) {
			min=50;
			max=250;
		}
	}
}
