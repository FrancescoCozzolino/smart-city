// Agent camera in project smartHome

/* Initial beliefs and rules */
timeStamp(0).
/* Initial goals */

!start.

/* Plans */

+!start : true <- .print("camera is ready");
jadedf.register("camera","camera");
makeArtifact("dropboxArtifact","smartHome.Dropbox",[],IdDropbox);
focus(IdDropbox);
initUpload;
makeArtifact("cameraArtifact","smartHome.Camera",[IdDropbox],IdCamera);
focus(IdCamera);
start;
.wait(120000);
stop./*;
start.*/

//QUESTA E' LA GET
+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?state(V);
//stato della camera
.send(Sender,tell,[V],MsgId).

//QUESTA E' LA PUT
+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus)
<-.nth(1,Content,V);
?state(S);
!doJob(S,V);
.send(Sender,tell,"done",MsgId).

+!doJob(S,V): S=="on" & V==0
<- stop.

+!doJob(S,V): S=="off" & V==1
<- start.

+completed(V)
<-//println("Completato: ",V);
.send(server,achieve,V,"alarmCamera").

+alarm
<-//println("Alarm from camera");
?timeStamp(T);
?((system.time-T)>60000);
-+timeStamp(system.time);
.send(server,achieve,system.time,"alarmCamera").