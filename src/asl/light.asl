// Agent light in project smartHome

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true .

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("light","smartHome.Light",[IdArduinoBoard,13,false],IdLight);
setPinMode [artifact_name("light")];
startTrackPin [artifact_name("light")];
focus(IdLight);
?powerConsumption(PowerLight)[artifact_name(_,"light")];
.send(main,achieve,["addDevice","light","light",PowerLight,1]);
jadedf.register("light","light");
makeArtifact("externalLight","smartHome.Light",[IdArduinoBoard,3,true],IdLightExternal);
setPinMode [artifact_name("externalLight")];
startTrackPin [artifact_name("externalLight")];
focus(IdLightExternal);
?powerConsumption(PowerExternalLight)[artifact_name(_,"externalLight")];
.send(main,achieve,["addDevice","light","externalLight",PowerExternalLight,1]);
jadedf.register("externalLight","light").

+ready(V): V=="raspberryArtifact"
<-lookupArtifact(V,IdRaspberryBoard);
makeArtifact("lightSensorArtifact","smartHome.LightSensor",[IdRaspberryBoard,50],IdLightSensor);
setPinMode [artifact_name("lightSensorArtifact")];
focus(IdLightSensor);
?powerConsumption(PowerSensorLight)[artifact_name(_,"lightSensorArtifact")];
.send(main,achieve,["addDevice","light","lightSensorArtifact",PowerSensorLight,1]).
//startTrackPin [artifact_name("lightSensorArtifact")].

//PER LA LUCE ESTERNA V3 INDICA SE ATTIVO O MENO LA RILEVAZIONE DELL'INTENSITA' DELLA LUCE
+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?value(V1)[artifact_name(IdLight,"light")];
?value(V2)[artifact_name(IdLightExternal,"externalLight")];
?state(V3);
//V1 e' il valore della luce
//V3 e V2 se il sensore della luce esterna � attivo e il valore della luce esterna
.send(Sender,tell,[V1,V3,V2],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(0,Content,Id);
.term2string(Id,IdString);
!doJob(IdString,Content);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-!checkDeviceName(Content).

+!checkDeviceName(DeviceName): not(/*DeviceName=="externalLight" | */DeviceName=="lightSensorArtifact")
<-setBrightness(0) [artifact_name(DeviceName)].//!doJob(DeviceName,0).

+!checkDeviceName(DeviceName): DeviceName=="lightSensorArtifact"
<-stopTrackPin [artifact_name("lightSensorArtifact")].//!setParameters("externalLight",0,0).

+!doJob(IdString,Content): not(IdString=="externalLight")
<- .nth(1,Content,V);
setBrightness(V) [artifact_name(IdString)].

+!doJob(IdString,Content): IdString=="externalLight"
<- .nth(1,Content,State);
.nth(2,Content,Brightness);
!setParameters(IdString,State,Brightness).

//CON STATE=1 ATTIVO IL SENSORE DI LUCE
+!setParameters(IdString,State,Brightness): State==1 & state(0)
<-startTrackPin [artifact_name("lightSensorArtifact")].

//CON STATE=0 E SE IL SENSORE E' GIA' SPENTO, IMPOSTO LA LUMINOSITA' DEL LED MANUALMENTE
+!setParameters(IdString,State,Brightness): State==0 & state(0)
<- setBrightness(Brightness) [artifact_name(IdString)].

//CON STATE=0 E SE IL SESNORE E' ATTIVO, LO SPENGO E IMPOSTO LA LUMINOSITA' DEL LED MANUALMENTE
+!setParameters(IdString,State,Brightness): State==0 & state(1)
<-stopTrackPin [artifact_name("lightSensorArtifact")];
setBrightness(Brightness) [artifact_name(IdString)].

-!setParameters(IdString,State,Brightness).

+state(S): S==0
<-.send(main,achieve,["turnDevice","light","lightSensorArtifact",false]).

+state(S): S==1
<-.send(main,achieve,["turnDevice","light","lightSensorArtifact",true]).

+value(V)[artifact_name(_,Name)]: V==0
<-.send(main,achieve,["turnDevice","light",Name,false]).

+value(V)[artifact_name(_,Name)]: not(V==0)
<-!checkName(Name);
.send(main,achieve,["turnDevice","light",Name,true]).

+!checkName(Name): Name=="externalLight"
<-?powerConsumption(Power)[artifact_name(_,Name)];
.send(main,achieve,["changePowerConsumption","light","externalLight",Power]).

-!checkName(Name).

+lux(V): V<=100 & not(value(100)[artifact_name(IdLightExternal,"externalLight")])
<-setBrightness(100) [artifact_name("externalLight")].

+lux(V): V>100 & V<1000 & not(value(50)[artifact_name(IdLightExternal,"externalLight")])
<-setBrightness(50) [artifact_name("externalLight")].

+lux(V): V>=1000 & not(value(0)[artifact_name(IdLightExternal,"externalLight")])
<-setBrightness(0) [artifact_name("externalLight")].