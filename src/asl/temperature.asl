// Agent temperature in project smartHome

/* Initial beliefs and rules */
minTemperature(18).
maxTemperature(24).
time("2","10","48","60000").
time("2","11","55","480000").
duration(60000)[artifact_name(_,"21048")].
duration(480000)[artifact_name(_,"21155")].
/* Initial goals */

!start.

/* Plans */

+!start : true
<-jadedf.register("setMinTemp","temperature");
jadedf.register("setMaxTemp","temperature");
jadedf.register("setWaterTemp","temperature");
jadedf.register("setHeater","temperature");
jadedf.register("addTime","temperature");
jadedf.register("removeTime","temperature").

+!checkStartHeater
<-utils.dayHourMinuteOfWeek(D,H,M);
?temperature(Temperature);
?maxTemperature(Max);
!startHeater(D,H,M,Temperature,Max);
!!checkStartHeater.

+!startHeater(D,H,M,Temperature,Max): time(D,H,M,_) & Temperature<Max & on(false)
<-.concat(D,H,M,IdName);
?duration(Duration)[artifact_name(_,IdName)];
turnOn;
.wait(Duration);
turnOff.

-!startHeater(Day,H,M,Temperature,Max)
<-.wait(60000).

+!read
<-readTemperature;
.wait(1000);
!!read.

+!checkTemperature(T,Min,Max): T<Min
<-?on(false);
turnOn.

+!checkTemperature(T,Min,Max): T>Max
<-?on(true);
turnOff.

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?temperature(T);
?on(S);
?waterTemperature(W);
?minTemperature(Min);
?maxTemperature(Max);
.findall(t(Day,H,M,Duration),time(Day,H,M,Duration),ListTime);
.length(ListTime,Size);
!buildString("",ListTime,0,Size);
?stringTime(StringTime);
-stringTime(StringTime);
.send(Sender,tell,[T,Min,Max,S,W,StringTime],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(0,Content,Id);
.nth(1,Content,Value);
.term2string(Id,IdString);
!doJob(IdString,Value);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==main
<-!doJob("setHeater",0).

+!buildString(String,List,Index,Size): not(Size==0) & Index < (Size-1)
<-.nth(Index,List,Element);
.term2string(Element,StringElement);
utils.replace(StringElement,"t","",Tmp);
utils.replace(Tmp,"(","",Tmp2);
utils.replace(Tmp2,")","",Tmp3);
utils.replace(Tmp3,"\""," ",Tmp4);
utils.replace(Tmp4,","," ",Tmp5);
.concat(String,Tmp5,"\n",NewString);
!buildString(NewString,List,Index+1,Size).

+!buildString(String,List,Index,Size): not(Size==0) & Index < Size
<-.nth(Index,List,Element);
.term2string(Element,StringElement);
utils.replace(StringElement,"t","",Tmp);
utils.replace(Tmp,"(","",Tmp2);
utils.replace(Tmp2,")","",Tmp3);
utils.replace(Tmp3,"\""," ",Tmp4);
utils.replace(Tmp4,","," ",Tmp5);
.concat(String,Tmp5,NewString);
+stringTime(NewString).

-!buildString(String,List,Index,Size)
<-+stringTime("").

+!doJob(IdString,Value): IdString=="setMinTemp"
<-!changeMinTemperature(Value).

+!doJob(IdString,Value): IdString=="setMaxTemp"
<-!changeMaxTemperature(Value).

+!doJob(IdString,Value): IdString=="setWaterTemp"
<-!changeWater(Value).

+!doJob(IdString,Value): IdString=="setHeater"
<-?temperature(T);
?minTemperature(MinT);
?maxTemperature(MaxT);
!turnHeater(Value,T,MinT,MaxT).

+!doJob(IdString,Value): IdString=="addTime"
<-utils.split(Value,"-",List);
.length(List,Size);
!addTime(List,0,Size).

+!doJob(IdString,Value): IdString=="removeTime"
<-utils.split(Value,"-",List);
.length(List,Size);
!removeTime(List,0,Size).

-!doJob(IdString,Value).

+!addTime(List,Index,Size): Index<Size
<-.nth(Index,List,String);
utils.split(String," ",TimeList);
.nth(0,TimeList,Day);
.nth(1,TimeList,Hour);
.nth(2,TimeList,Minute);
.nth(3,TimeList,DurationString);
utils.stringToNumber(DurationString,Duration);
.concat(Day,Hour,Minute,IdName);
+time(Day,Hour,Minute,DurationString);
+duration(Duration)[artifact_name(_,IdName)];
!addTime(List,Index+1,Size).

-!addTime(List,Index,Size).

+!removeTime(List,Index,Size): Index<Size
<-.nth(Index,List,String);
utils.split(String," ",TimeList);
.nth(0,TimeList,Day);
.nth(1,TimeList,Hour);
.nth(2,TimeList,Minute);
.concat(Day,Hour,Minute,IdName);
-time(Day,Hour,Minute,_);
-duration(_)[artifact_name(_,IdName)];
!removeTime(List,Index+1,Size).

-!removeTime(List,Index,Size).

+!turnHeater(State,T,MinT,MaxT): State==1 & on(false) & T<=MaxT
<-turnOn.

+!turnHeater(State,T,MinT,MaxT): State==0 & on(true) & T>=MinT
<-turnOff;
.drop_intention(startHeater(_,_,_,_,_)).

-!turnHeater(State,T,MinT,MaxT).

+!changeWater(Water): not(Water==0)
<-setWaterTemperature(Water).

-!changeWater(Water).

+!changeMinTemperature(Min): not(Min==0)
<--+minTemperature(Min).

-!changeMinTemperature(Min).

+!changeMaxTemperature(Max): not(Max==0)
<--+maxTemperature(Max).

-!changeMaxTemperature(Max).

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("heaterArtifact","smartHome.Heater",[IdArduinoBoard,6],IdHeater);
setPinMode [artifact_name("heaterArtifact")];
focus(IdHeater);
?powerConsumption(Power);
.send(main,achieve,["addDevice","temperature","heater",Power,3]);
startTrackPin [artifact_name("heaterArtifact")].

+ready(V): V=="raspberryArtifact"
<-lookupArtifact(V,IdRaspberryBoard);
makeArtifact("temperatureArtifact","smartHome.Temperature",[IdRaspberryBoard,24],IdTemperature);
addI2CDevice [artifact_name("temperatureArtifact")];
focus(IdTemperature);
!read;
!!checkStartHeater.

+on(S): S==false
<-.send(main,achieve,["turnDevice","temperature","heater",false]).

+on(S): S==true
<-.send(main,achieve,["turnDevice","temperature","heater",true]).

+waterTemperature(T)
<-?powerConsumption(Power);
.send(main,achieve,["changePowerConsumption","temperature","heater",Power]).

+temperature(V)
<-?minTemperature(Min);
?maxTemperature(Max);
!checkTemperature(V,Min,Max).