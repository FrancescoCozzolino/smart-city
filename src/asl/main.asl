// Agent sample_agent in project smartHome

/* Initial beliefs and rules */
/* Initial goals */

!start.

/* Plans */

+!start : true 
<-.wait(5000);
makeArtifact("arduinoArtifact","smartHome.Arduino",["/dev/ttyACM0"],IdArduinoBoard);
makeArtifact("raspberryArtifact","smartHome.Raspberry",[],IdRaspberryBoard);
makeArtifact("electricMeterArtifact","smartHome.ElectricMeter",[],IdElectricMeter);
jadedf.register("energy","main");
focus(IdElectricMeter);
.broadcast(tell,ready("arduinoArtifact"));
.broadcast(tell,ready("raspberryArtifact")).

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?currentConsumption(C);
?profile(P);
.send(Sender,tell,[C,P],MsgId).

//QUESTO OLTRE A RICEVERE LA PUT DAL SERVER RICEVE ANCHE GLI ACHIEVE DAGLI ALTRI AGENTI
//QUINDI AL POSTO DI METTERE IL NOME DEL METODO, PASSARE UNA LISTA CON PRIMO PARAMETRO QUALCOSA INERENTE
// ALL'AGGIUNTA/RIMOZIONE DEL DEVICE, POI GLI ALTRI PARAMETRI
+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(1,Content,Value);
setProfile(Value);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & not(Sender==server)
<-.nth(0,Content,String);
!doJob(String,Content).

+!doJob(String,Content): String=="addDevice"
<-.nth(1,Content,NameAgent);
.nth(2,Content,NameDevice);
.nth(3,Content,PowerConsumption);
.nth(4,Content,Priority);
addDevice(NameAgent,NameDevice,PowerConsumption,Priority).

+!doJob(String,Content): String=="turnDevice"
<-.nth(1,Content,NameAgent);
.nth(2,Content,NameDevice);
.nth(3,Content,On);
turnDevice(NameAgent,NameDevice,On).

+!doJob(String,Content): not(String=="turnDevice"|String=="addDevice")
<-.nth(1,Content,NameAgent);
.nth(2,Content,NameDevice);
.nth(3,Content,PowerConsumption);
!callPowerConsumption(String,NameAgent,NameDevice,PowerConsumption).

-!doJob(String,Content).

+!callPowerConsumption(String,NameAgent,NameDevice,PowerConsumption): String=="changePowerConsumption"
<-changePowerConsumption(NameAgent,NameDevice,PowerConsumption).

+!callPowerConsumption(String,NameAgent,NameDevice,PowerConsumption): String=="addPowerConsumption"
<-addPowerConsumption(NameAgent,NameDevice,PowerConsumption).

+!callPowerConsumption(String,NameAgent,NameDevice,PowerConsumption): String=="removePowerConsumption"
<-removePowerConsumption(NameAgent,NameDevice,PowerConsumption).

+turnOffDevice(NameAgent,NameDevice)
<-.send(NameAgent,achieve,NameDevice).