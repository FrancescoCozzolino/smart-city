// Agent garage in project smartHome

/* Initial beliefs and rules */

/* Initial goals */
!start.

/* Plans */
+!open
<-open;
.wait(10000);
close.

+!start : true <-jadedf.register("garage","garage").

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("garage","smartHome.Garage",[IdArduinoBoard,11],IdGarage);
setPinMode [artifact_name("garage")];
focus(IdGarage);
startTrackPin [artifact_name("garage")];
//calibrate;
makeArtifact("irArtifact","smartHome.IRDistance",[IdArduinoBoard,15],IdIr);
setPinMode [artifact_name("irArtifact")];
focus(IdIr);
?powerConsumption(PowerIr)[artifact_name(_,"irArtifact")];
?powerConsumption(PowerGarage)[artifact_name(_,"garage")];
.send(main,achieve,["addDevice","garage","garage",PowerIr+PowerGarage,2]).//;
//startTrackPin [artifact_name("irArtifact")].

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?state(V1);
.send(Sender,tell,[V1],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(1,Content,V);
?state(S);
!doJob(S,V);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-?state(S);
!doJobGarage(S).

+!doJobGarage(S): S=="opening" | S=="closing"
<-stop.

-!doJobGarage(S).

+!doJob(S,V): (S=="open"| S=="stop") & V==0
<-.drop_intention(open);
close.

+!doJob(S,V): S=="opening" & V==0
<- stop;
.drop_intention(open);
close.

+!doJob(S,V): (S=="close" | S=="stop")& V==1
<-!!open.

+!doJob(S,V): S=="closing" & V==1
<- stop;
!!open.

-!doJob(S,V).

+state(S): S=="open"
<-.send(main,achieve,["turnDevice","garage","garage",false]).

+state(S): S=="close"
<-stopTrackPin [artifact_name("irArtifact")];
.send(main,achieve,["turnDevice","garage","garage",false]).

+state(S): S=="opening"
<-?powerConsumption(PowerConsumption)[artifact_name(_,"garage")];
.send(main,achieve,["changePowerConsumption","garage","garage",PowerConsumption]);
.send(main,achieve,["turnDevice","garage","garage",true]).

+state(S): S=="closing"
<-startTrackPin [artifact_name("irArtifact")];
?powerConsumption(PowerGarage)[artifact_name(_,"garage")];
?powerConsumption(PowerIr)[artifact_name(_,"irArtifact")];
.send(main,achieve,["changePowerConsumption","garage","garage",PowerGarage+PowerIr]);
.send(main,achieve,["turnDevice","garage","garage",true]).

+state(S): S=="stop"
<-.send(main,achieve,["turnDevice","garage","garage",false]).

+distance(V): V<=5
<-?state("closing");
stop;
!!open.