// Agent alarm in project smartHome

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true.

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("alarmSmoke","smartHome.AlertTimeSensor",[IdArduinoBoard,16,600,5],IdSmoke);
setPinMode [artifact_name("alarmSmoke")];
focus(IdSmoke);
?powerConsumption(PowerSmoke)[artifact_name(_,"alarmSmoke")];
.send(main,achieve,["addDevice","alarm","alarmSmoke",PowerSmoke,3]);
jadedf.register("alarmSmoke","alarm");
makeArtifact("alarmGas","smartHome.AlertTimeSensor",[IdArduinoBoard,17,600,5],IdGas);
setPinMode [artifact_name("alarmGas")];
focus(IdGas);
?powerConsumption(PowerGas)[artifact_name(_,"alarmGas")];
.send(main,achieve,["addDevice","alarm","alarmGas",PowerGas,3]);
jadedf.register("alarmGas","alarm");
makeArtifact("dropboxArtifact","smartHome.Dropbox",["smartCityHome","YCJ57IH015AAAAAAAAAFwrOqSs39QV4PEV_dJbR3Vn7WWTcoQ0oGel0MFq5gg9R5"],IdDropbox);
focus(IdDropbox);
makeArtifact("alarmCamera","smartHome.Camera",[IdDropbox],IdCamera);
focus(IdCamera);
?powerConsumption(PowerCamera)[artifact_name(_,"alarmCamera")];
.send(main,achieve,["addDevice","alarm","alarmCamera",PowerCamera,3]);
jadedf.register("alarmCamera","alarm").
      
+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus & Sender==server
<-?state(V1)[artifact_name(_,"alarmSmoke")];
?state(V2)[artifact_name(_,"alarmGas")];
?state(V3)[artifact_name(_,"alarmCamera")];
.send(Sender,tell,[V1,V2,V3],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(0,Content,Id);
.nth(1,Content,V);
.term2string(Id,IdString);
!doJob(IdString,V);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-!doJob(Content,0).

+!doJob(IdString,V): not(IdString=="alarmCamera")
<-!doJobGas(IdString,V).

+!doJob(IdString,V): IdString=="alarmCamera"
<-?state(S)[artifact_name(_,"alarmCamera")];
!doJobCamera(S,V).

+!doJobCamera(S,V): S=="on" & V==0
<-stop.

+!doJobCamera(S,V): S=="off" & V==1
<-start.

-!doJobCamera(S,V).

+!doJobGas(IdString,V): V==1 & state(0)[artifact_name(_,IdString)]
<-startTrackPin [artifact_name(IdString)].

+!doJobGas(IdString,V): V==0 & state(1)[artifact_name(_,IdString)]
<-stopTrackPin [artifact_name(IdString)].

-!doJobGas(IdString,V).

+state(V)[artifact_name(_,Name)]: V==0 | V=="off"
<-print("Nome Artefatto: ",Name);
println("Stato: ",V);//;
.send(main,achieve,["turnDevice","alarm",Name,false]).

+state(V)[artifact_name(_,Name)]: V==1 | V=="on"
<-.send(main,achieve,["turnDevice","alarm",Name,true]).

+alarmGas [artifact_name(_,A)]
<-println("Alarm from ",A);
.send(server,achieve,[A,system.time],"alarmGas").

+completed(V)
<-println("Completato: ",V);
.send(server,achieve,V,"alarmCamera").

+startUpload
<-?powerConsumption(PowerCamera)[artifact_name(_,"alarmCamera")];
.send(main,achieve,["changePowerConsumption","alarm","alarmCamera",PowerCamera]).

+alarm
<-println("Alarm from camera");
?powerConsumption(PowerCamera)[artifact_name(_,"alarmCamera")];
.send(main,achieve,["changePowerConsumption","alarm","alarmCamera",PowerCamera]);
.send(server,achieve,["alarmCamera",system.time],"alarmCamera").