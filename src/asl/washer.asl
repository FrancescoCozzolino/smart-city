// Agent oven in project smartHome

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true <-jadedf.register("washer","washer").

+!startWasher(Program)
<-start(Program);
?startTime(Start); //da togliere
println("Inizio a ",Start); //da togliere
?timeWait(T); //da togliere
println("Aspetto per ",T).//; //da togliere
//!!waitTime(T).

+!stopWasher
<-?timeWait(Time);
?startTime(Start);
!stopWasher2((Time-(system.time-Start))).

+!stopWasher2(T): T>0
<-//.drop_intention(waitTime(_));
stop.

-!stopWasher2(T).

+!resumeWasher
<-//?timeWait(Time);
println("Riprendo a: ",system.time); //da togliere
resume.//;
//!!waitTime(Time).

/*+!waitTime(T)
<-.wait(T);
stop.*/

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?state(V);
?program(P);
!calculateTime;
?remainingTime(T);
//-remainingTime(T);
.send(Sender,tell,[V,P,T],MsgId).

+!calculateTime: state("on")
<-/*?timeWait(Time);
?startTime(Start);
+remainingTime((Time-(system.time-Start)))*/refreshRemainingTime.

-!calculateTime: not(state("on")).
/*+!calculateTime: state("pause")
<-?timeWait(Time);
+remainingTime(Time).

+!calculateTime: state("off")
<-+remainingTime(0).*/

//QUESTA E' LA PUT
+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(1,Content,State);
.nth(2,Content,Program);
!doJob(State,Program);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-!doJob(0,0).

+!doJob(State,Program): State==0 & state("on")
<-!stopWasher.

+!doJob(State,Program): State==1 & state("pause")
<-!resumeWasher.

+!doJob(State,Program): State==1 & state("off")
<-!startWasher(Program).

-!doJob(State,Program).

+state(V): V=="on"
<-?powerConsumption(PowerConsumption);
.send(main,achieve,["changePowerConsumption","washer","washerArtifact",PowerConsumption]);
.send(main,achieve,["turnDevice","washer","washerArtifact",true]).

+state(V): not(V=="on")
<-.send(main,achieve,["turnDevice","washer","washerArtifact",false]).

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("washerArtifact","smartHome.Washer",[IdArduinoBoard,4],IdWasher);
setPinMode;
startTrackPin;
focus(IdWasher);
?powerConsumption(PowerConsumption);
.send(main,achieve,["addDevice","washer","washerArtifact",PowerConsumption,1]).