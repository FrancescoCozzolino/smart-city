// Agent oven in project smartHome

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true
<-jadedf.register("turnOnOven","oven");
jadedf.register("turnOffOven","oven");
jadedf.register("changeTime","oven");
jadedf.register("changeDegrees","oven").

/*+!waitTime(T)
<-.wait(T);
turnOff.*/

+!turnOffOven
<-//.drop_intention(waitTime(_));
turnOff.

+!turnOnOven(Degrees,Time)
<-//-+endTime(Time+system.time);  
turnOn(Degrees,Time);
?endTime(T);  //da togliere
println("EndTime : ",T).//; //da togliere
//!!waitTime(Time).

+!changeTimeOven(Time): not(Time==0)
<-println("Cambio il tempo di: ",Time); //da togliere
?endTime(T); 
+timeWait((T-system.time+Time));
?timeWait(V);
-timeWait(V);
!continueChangeTime(V).

-!changeTimeOven(Time).

+!continueChangeTime(V):V>0
<-//-+endTime(system.time+V);
//.drop_intention(waitTime(_));
//!!waitTime(V)
changeTime(V);
?endTime(V2); //da togliere
println("nuovo endTime: ",V2). //da togliere

-!continueChangeTime(V).

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?on(V);
?temperature(T);
?endTime(E);
.send(Sender,tell,[V,T,(E-system.time)],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-.nth(0,Content,Id);
.term2string(Id,IdString);
!doJob(IdString,Content);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-!doJob("turnOffOven",0).

+!doJob(IdString,Content): IdString=="turnOnOven" & on(false)
<-.nth(1,Content,Temperature);
.nth(2,Content,Time);
!turnOnOven(Temperature,Time).

+!doJob(IdString,Content): IdString=="turnOffOven" & on(true)
<-turnOff.//!turnOffOven.//;
//-+endTime(system.time).

+!doJob(IdString,Content): IdString=="changeTime" & on(true)
<-.nth(1,Content,Time);
!changeTimeOven(Time).

+!doJob(IdString,Content): IdString=="changeDegrees" & on(true)
<-.nth(1,Content,Temperature);
!changeDegrees(Temperature).

-!doJob(IdString,Content).

+!changeDegrees(Temperature): not(Temperature==0)
<-changeDegrees(Temperature).

-!changeDegrees(Temperature).

+ready(V): V=="arduinoArtifact"
<-//+endTime(system.time);
lookupArtifact(V,IdArduinoBoard);
makeArtifact("oven","smartHome.Oven",[IdArduinoBoard,5],IdOven);
setPinMode;
startTrackPin;
focus(IdOven);
?powerConsumption(PowerConsumption);
.send(main,achieve,["addDevice","oven","oven",PowerConsumption,1]).

+on(V): V==false
<-.send(main,achieve,["turnDevice","oven","oven",false]).

+on(V): V==true
<-?powerConsumption(PowerConsumption);
.send(main,achieve,["changePowerConsumption","oven","oven",PowerConsumption]);
.send(main,achieve,["turnDevice","oven","oven",true]).

+temperature(T): on(true)
<-?powerConsumption(PowerConsumption);
.send(main,achieve,["changePowerConsumption","oven","oven",PowerConsumption]).