//CASO PIU' SENSORI DI UMIDITA'
//quando il serbatoio cambia
//fare una read (al posto del test goal)per il sensore di umidit� 
//(in modo da forzare il cambio della propriet� osservabile (read per ogni sensore presente)
//e aggiornare le belief. in modo da aprire le valvole corrispondenti

// Agent irrigation in project smartHome

/* Initial beliefs and rules */

/* Initial goals */

!start.

/* Plans */

+!start : true <-jadedf.register("moistureSensor","irrigation").

+!open(Name): empty(false)
<- turnOn [artifact_name(Name)].

-!open(Name).

+!close(Name) 
<- turnOff [artifact_name(Name)].

+!kqml_received(Sender, achieve, Content, MsgId): Content==getStatus
<-?soil(V1)[artifact_name(IdMoistureSensor,"moistureSensor")];
?empty(V2);
.send(Sender,tell,[V1,V2],MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): not(Content==getStatus) & Sender==server
<-/*.nth(0,Content,Id);
.term2string(Id,IdString);*/
.nth(1,Content,V);
//?soil(S)/*[artifact_name(_,IdString)]*/;
!doJob(/*IdString,S,*/V);
.send(Sender,tell,"done",MsgId).

+!kqml_received(Sender, achieve, Content, MsgId): Sender==main
<-!doJob(0).

//con piu sensori di umidita', fare tanti start quanti i sensori
+!doJob(/*Id,S,*/V): /*S==""*/soil("") & V==1
<- startTrackPin [artifact_name(/*Id*/"moistureSensor")];
startTrackPin [artifact_name("waterSensor")];
?powerConsumption(PowerSoil)[artifact_name(_,"moistureSensor")];
?powerConsumption(PowerWater)[artifact_name(_,"waterSensor")];
.send(main,achieve,["changePowerConsumption","irrigation","irrigation",PowerSoil+PowerWater]);
.send(main,achieve,["turnDevice","irrigation","irrigation",true]).

+!doJob(/*Id,S,*/V): /*not(S=="")*/not(soil("")) & V==0
<- stopTrackPin [artifact_name(/*Id*/"moistureSensor")];
stopTrackPin [artifact_name("waterSensor")];
//?map(Name)[artifact_name(_,Id)];
?state(Value)[artifact_name(_,"valve")];
!stopValve(/*Name*/"valve",Value);
.send(main,achieve,["turnDevice","irrigation","irrigation",false]).

-!doJob(/*Id,S,*/V).

+!stopValve(Name,Value): Value==1
<-!close(Name).

-!stopValve(Name,Value).

+ready(V): V=="arduinoArtifact"
<-lookupArtifact(V,IdArduinoBoard);
makeArtifact("valve","smartHome.Valve",[IdArduinoBoard,8],IdValve);
setPinMode [artifact_name("valve")];
focus(IdValve);
startTrackPin [artifact_name("valve")];
makeArtifact("moistureSensor","smartHome.MoistureSensor",[IdArduinoBoard,14],IdMoistureSensor);
setPinMode [artifact_name("moistureSensor")];
focus(IdMoistureSensor);
+map("valve") [artifact_name(_,"moistureSensor")];
makeArtifact("waterSensor","smartHome.WaterSensor",[IdArduinoBoard,18],IdWaterSensor);
setPinMode [artifact_name("waterSensor")];
focus(IdWaterSensor);
.send(main,achieve,["addDevice","irrigation","irrigation",0,1]).
//startTrackPin [artifact_name("waterSensor")].

//se il terreno � asciutto, attivo la pompa
+soil(V)[artifact_name(_,MoistureSensor)]: V=="dry"
<-println("Terreno secco");
?map(Name)[artifact_name(_,MoistureSensor)];
!open(Name).

//se il terreno � umido e la pompa � attiva, la spengo
+soil(V)[artifact_name(_,MoistureSensor)]: V=="humid"
<-println("Terreno umido");
?map(Name)[artifact_name(_,MoistureSensor)];
?state(Value)[artifact_name(_,Name)];
!stopValve(Name,Value).

// se il terreno � bagnato, mandare notifica
+soil(V)[artifact_name(_,MoistureSensor)]: V=="wet"
<-println("Terreno bagnato");
?map(Name)[artifact_name(_,MoistureSensor)];
?state(Value)[artifact_name(_,Name)];
!stopValve(Name,Value).

//se il serbatoio � vuoto e la pompa � attiva, la spengo
+empty(V): V==true 
<-println("The tank is empty");
//?map(Name)[artifact_name(_,"moistureSensor")];
?state(Value)[artifact_name(_,"valve")];
!stopValve("valve",Value).

//se il serbatoio � stato riempito e il terreno � asciutto, attivo la pompa
+empty(V): V==false
<-println("The tank is not empty");
?soil("dry")[artifact_name(_,"moistureSensor")];
?map(Name)[artifact_name(_,"moistureSensor")];
!open(Name).

+state(V) [artifact_name(_,Name)]: V==0
<-?powerConsumption(PowerValve)[artifact_name(_,Name)];
.send(main,achieve,["removePowerConsumption","irrigation","irrigation",PowerValve]).

+state(V) [artifact_name(_,Name)]: V==1
<-?powerConsumption(PowerValve)[artifact_name(_,Name)];
.send(main,achieve,["addPowerConsumption","irrigation","irrigation",PowerValve]).